﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using EasyFrame.BLL;
using EasyFrame.NewCommon.Model;
using EasyFrame.Redis;
using ServiceStack.Redis;
using System.Web.Services.Protocols;
using System.Data;
using EasyFrame.NewCommon;
/// <summary>
///ReportWebService 的摘要说明
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
//若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消对下行的注释。 
// [System.Web.Script.Services.ScriptService]
public class ReportWebService : System.Web.Services.WebService
{
    /// <summary>
    /// Redis操作帮助类
    /// </summary>
    private RedisHelper redisHelper = new RedisHelper();
    public ReportWebService()
    {

        //如果使用设计的组件，请取消注释以下行 
        //InitializeComponent(); 
    }
    public EncryptionSoapHeader encryptionSoapHeader = new EncryptionSoapHeader();
    //[WebMethod]
    //public string HelloWorld() {
    //    return "Hello World";
    //}
    /// <summary>
    /// 验证权限
    /// </summary>
    /// <returns></returns>
    private string ValidateAuthority()
    {
        ResultInfoT<dt_report_inoutaccount> resultInfo = new ResultInfoT<dt_report_inoutaccount>();
        string strMsg = string.Empty;
        if (!encryptionSoapHeader.IsValid(out  strMsg))
        {
            resultInfo.Code = -1;
            resultInfo.StrCode = strMsg;
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
        return strMsg;
    }
    /// <summary>
    /// 前台获取该会员在这一时间段内的盈利总额、投注总额、中奖总额、活动总额、返点总额、充值总额、提现总额。－－－－前台[今日盈亏  BeforeProfitReport]的功能
    /// </summary>
    /// <param name="iUserId">会员ID</param>
    /// <param name="flog">是否测试</param>
    /// <param name="dtStartTime">起始日期</param>
    /// <param name="dtEndTime">结束日期</param>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string BeforeProfitReport(int iUserId, int flog, DateTime dtStartTime, DateTime dtEndTime)
    {
        ResultInfoT<dt_report_inoutaccount> resultInfo = new ResultInfoT<dt_report_inoutaccount>();
        string strResult = string.Empty;
        try
        {
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            if (!string.IsNullOrWhiteSpace(strMsg))
            {
                return strMsg;
            }
            //会员
            if (flog == 0)
            {
                //获取前台盈利报表数据
                strResult = GetProfitReport(iUserId, dtStartTime, dtEndTime);
                return strResult;
            }
            //测试账号
            strResult = GetTestProfitReport(iUserId, dtStartTime, dtEndTime);
            return strResult;
        }
        catch (Exception ex)
        {
            resultInfo.Code = -1;
            resultInfo.StrCode = ex.Message;
            EasyFrame.NewCommon.Log.Error(ex);
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
    }
    /// <summary>
    /// 获取前台盈利报表数据
    /// </summary>
    /// <param name="iUserId">会员ID</param>
    /// <param name="dtStartTime">起始日期</param>
    /// <param name="dtEndTime">截止日期</param>
    /// <param name="context"></param>
    private string GetProfitReport(int iUserId, DateTime dtStartTime, DateTime dtEndTime)
    {
        ResultInfoT<dt_report_inoutaccount> resultInfo = new ResultInfoT<dt_report_inoutaccount>();
        //RedisClientConfig redisClientConfig = new RedisClientConfig();
        //redisClientConfig.ServerIP = DBcon.DBcontring.redisServerIP;
        var redisClient = GlobalVariable.redisClient;
        var redisClientKey = GlobalVariable.redisClientKey;
        //var reportAgentList = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.user_id == iUserId && m.AddTime >= dtStartTime && m.AddTime <= dtEndTime).ToList();
        //Log.Info("获取出入账数据开始时间：" + DateTime.Now.ToString());
        List<dt_report_inoutaccount> reportAgentList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, dtStartTime, dtEndTime, iUserId.ToString(), "").ToList();
        //Log.Info("获取出入账数据结束时间：" + DateTime.Now.ToString());
        redisClient.Quit();
        redisClient.Dispose();
        redisClientKey.Quit();
        redisClientKey.Dispose();
        dt_report_inoutaccount inOutAccount = new dt_report_inoutaccount();
        for (var i = 0; i < reportAgentList.Count; i++)
        {
            //投注总额
            inOutAccount.Out_BettingAccount += reportAgentList[i].Out_BettingAccount;
            //中奖总额
            inOutAccount.Out_WinningAccount += reportAgentList[i].Out_WinningAccount;
            //活动总额
            inOutAccount.Out_ActivityDiscountAccount += reportAgentList[i].Out_ActivityDiscountAccount;
            //返点总额
            inOutAccount.Out_RebateAccount += reportAgentList[i].Out_RebateAccount;
            //充值总额
            inOutAccount.In_FastPrepaid += reportAgentList[i].In_FastPrepaid + reportAgentList[i].In_BankPrepaid
                                         + reportAgentList[i].In_AlipayPrepaid + reportAgentList[i].In_WeChatPrepaid + reportAgentList[i].In_ArtificialDeposit;
            //提现总额
            inOutAccount.Out_Account += reportAgentList[i].Out_Account;
            //盈利总额=中奖总额－投注总额+活动总额+返点总额
            inOutAccount.ProfitLoss = inOutAccount.Out_WinningAccount;
            inOutAccount.ProfitLoss -= inOutAccount.Out_BettingAccount;
            inOutAccount.ProfitLoss += inOutAccount.Out_ActivityDiscountAccount;
            inOutAccount.ProfitLoss += inOutAccount.Out_RebateAccount;
        }
        // Log.Info("统计今日盈亏结束时间：" + DateTime.Now.ToString());
        inOutAccount.AddTime = DateTime.Now;
        resultInfo.Code = 1;
        resultInfo.BackData = inOutAccount;
        return LitJson.JsonMapper.ToJson(resultInfo);
    }
    /// <summary>
    /// 获取测试前台盈利报表数据
    /// </summary>
    /// <param name="iUserId">会员ID</param>
    /// <param name="dtStartTime">起始日期</param>
    /// <param name="dtEndTime">截止日期</param>
    /// <param name="context"></param>
    private string GetTestProfitReport(int iUserId, DateTime dtStartTime, DateTime dtEndTime)
    {
        ResultInfoT<dt_report_test_inoutaccount> resultInfo = new ResultInfoT<dt_report_test_inoutaccount>();
        //RedisClientConfig redisClientConfig = new RedisClientConfig();
        //redisClientConfig.ServerIP = DBcon.DBcontring.redisServerIP;
        var redisClient = GlobalVariable.redisClient;
        var redisClientKey = GlobalVariable.redisClientKey;
        //var reportAgentList = redisClient.GetAll<dt_report_test_inoutaccount>().Where(m => m.user_id == iUserId && m.AddTime >= dtStartTime && m.AddTime <= dtEndTime).ToList();
        List<int> listUserId = new List<int>();
        listUserId.Add(iUserId);
        List<dt_report_test_inoutaccount> reportAgentList = redisHelper.Search<dt_report_test_inoutaccount>(redisClient, redisClientKey, dtStartTime, dtEndTime, iUserId.ToString(), "").ToList();
        redisClient.Quit();
        redisClient.Dispose();
        redisClientKey.Quit();
        redisClientKey.Dispose();
        dt_report_test_inoutaccount inOutAccount = new dt_report_test_inoutaccount();
        for (var i = 0; i < reportAgentList.Count; i++)
        {
            //投注总额
            inOutAccount.Out_BettingAccount += reportAgentList[i].Out_BettingAccount;
            //中奖总额
            inOutAccount.Out_WinningAccount += reportAgentList[i].Out_WinningAccount;
            //活动总额
            inOutAccount.Out_ActivityDiscountAccount += reportAgentList[i].Out_ActivityDiscountAccount;
            //返点总额
            inOutAccount.Out_RebateAccount += reportAgentList[i].Out_RebateAccount;
            //充值总额
            inOutAccount.In_FastPrepaid += reportAgentList[i].In_FastPrepaid + reportAgentList[i].In_BankPrepaid
                                         + reportAgentList[i].In_AlipayPrepaid + reportAgentList[i].In_WeChatPrepaid + reportAgentList[i].In_ArtificialDeposit;
            //提现总额
            inOutAccount.Out_Account += reportAgentList[i].Out_Account;
            //盈利总额=中奖总额－投注总额+活动总额+返点总额
            inOutAccount.ProfitLoss = inOutAccount.Out_WinningAccount;
            inOutAccount.ProfitLoss -= inOutAccount.Out_BettingAccount;
            inOutAccount.ProfitLoss += inOutAccount.Out_ActivityDiscountAccount;
            inOutAccount.ProfitLoss += inOutAccount.Out_RebateAccount;
        }
        inOutAccount.AddTime = DateTime.Now;
        resultInfo.Code = 1;
        resultInfo.BackData = inOutAccount;
        return LitJson.JsonMapper.ToJson(resultInfo);
    }
    /// <summary>
    /// 获取投注金额－－－前台的[获取用户的投注总额   GetBetMoney]的功能
    /// </summary>
    /// <param name="context"></param>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetBetMoney(int iUserId, int flog)
    {
        ResultInfoT<dt_report_inoutaccount> resultInfo = new ResultInfoT<dt_report_inoutaccount>();
        try
        {
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            if (!string.IsNullOrWhiteSpace(strMsg))
            {
                return strMsg;
            }
            //RedisClientConfig redisClientConfig = new RedisClientConfig();
            //redisClientConfig.ServerIP = DBcon.DBcontring.redisServerIP;
            var redisClient = GlobalVariable.redisClient;
            var redisClientKey = GlobalVariable.redisClientKey;
            dt_report_inoutaccount inOutAccount = new dt_report_inoutaccount();
            //会员账号
            if (flog == 0)
            {
                GetBetData(iUserId, redisClient,redisClientKey, inOutAccount);
            }
            else  //测试账号
            {
                GetTestBetData(iUserId, redisClient,redisClientKey, inOutAccount);
            }
            redisClient.Quit();
            redisClient.Dispose();
            redisClientKey.Quit();
            redisClientKey.Dispose();
            inOutAccount.AddTime = DateTime.Now;
            resultInfo.Code = 1;
            resultInfo.BackData = inOutAccount;
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
        catch (Exception ex)
        {
            resultInfo.Code = -1;
            resultInfo.StrCode = ex.Message;
            EasyFrame.NewCommon.Log.Error(ex);
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
    }
    /// <summary>
    /// 获取会员投注金额
    /// </summary>
    /// <param name="iUserId">用户ID</param>
    /// <param name="redisClient">Redis操作类</param>
    /// <param name="inOutAccount"></param>
    private void GetBetData(int iUserId, RedisClient redisClient,RedisClient redisClientKey, dt_report_inoutaccount inOutAccount)
    {
        //var reportAgentList = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.user_id == iUserId).ToList();
        List<dt_report_inoutaccount> reportAgentList = redisHelper.SearchByUserId<dt_report_inoutaccount>(redisClient, redisClientKey, iUserId.ToString()).ToList();
        for (var i = 0; i < reportAgentList.Count; i++)
        {
            //投注总额
            inOutAccount.Out_BettingAccount += reportAgentList[i].Out_BettingAccount;
        }
    }
    /// <summary>
    /// 获取测试账号投注金额
    /// </summary>
    /// <param name="iUserId">用户ID</param>
    /// <param name="redisClient">Redis操作类</param>
    /// <param name="inOutAccount"></param>
    private void GetTestBetData(int iUserId, RedisClient redisClient,RedisClient redisClientKey, dt_report_inoutaccount inOutAccount)
    {
        //var reportAgentList = redisClient.GetAll<dt_report_test_inoutaccount>().Where(m => m.user_id == iUserId).ToList();
        List<dt_report_test_inoutaccount> reportAgentList = redisHelper.SearchByUserId<dt_report_test_inoutaccount>(redisClient, redisClientKey, iUserId.ToString()).ToList();
        for (var i = 0; i < reportAgentList.Count; i++)
        {
            //投注总额
            inOutAccount.Out_BettingAccount += reportAgentList[i].Out_BettingAccount;
        }
    }
    /// <summary>
    /// 获取中奖金额－－－－－－前台的[获取用户的中奖总额 GetAwardMoneyData]的功能
    /// </summary>
    /// <param name="context"></param>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetAwardMoney(int iUserId, int flog)
    {
        ResultInfoT<AwardMoneyData> resultInfo = new ResultInfoT<AwardMoneyData>();
        try
        {
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            if (!string.IsNullOrWhiteSpace(strMsg))
            {
                return strMsg;
            }
            //RedisClientConfig redisClientConfig = new RedisClientConfig();
            //redisClientConfig.ServerIP = DBcon.DBcontring.redisServerIP;
            var redisClient = GlobalVariable.redisClient;
            var redisClientKey = GlobalVariable.redisClientKey;
            AwardMoneyData awardMoneyData = new AwardMoneyData();
            //会员
            if (flog == 0)
            {
                GetAwardData(iUserId, redisClient,redisClientKey, awardMoneyData);
            }
            else //测试账号
            {
                GetTestAwardData(iUserId, redisClient,redisClientKey, awardMoneyData);
            }
            redisClient.Quit();
            redisClient.Dispose();
            redisClientKey.Quit();
            redisClientKey.Dispose();
            resultInfo.Code = 1;
            resultInfo.BackData = awardMoneyData;
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
        catch (Exception ex)
        {
            resultInfo.Code = -1;
            resultInfo.StrCode = ex.Message;
            EasyFrame.NewCommon.Log.Error(ex);
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
    }
    /// <summary>
    /// 获取会员中奖总额数据
    /// </summary>
    /// <param name="bjUserId">用户ID</param>
    /// <param name="context"></param>
    private void GetAwardData(int iUserId, RedisClient redisClient,RedisClient redisClientKey, AwardMoneyData awardMoneyData)
    {
        //var reportAgentList = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.user_id == iUserId).ToList();
        List<dt_report_inoutaccount> reportAgentList = redisHelper.SearchByUserId<dt_report_inoutaccount>(redisClient, redisClientKey,iUserId.ToString()).ToList();
        for (var i = 0; i < reportAgentList.Count; i++)
        {
            //中奖总额
            awardMoneyData.AwardMoney += reportAgentList[i].Out_WinningAccount;
        }
    }
    /// <summary>
    /// 获取测试账号中奖总额数据
    /// </summary>
    /// <param name="bjUserId">用户ID</param>
    /// <param name="context"></param>
    private void GetTestAwardData(int iUserId, RedisClient redisClient, RedisClient redisClientKey, AwardMoneyData awardMoneyData)
    {
        //var reportAgentList = redisClient.GetAll<dt_report_test_inoutaccount>().Where(m => m.user_id == iUserId).ToList();
        List<dt_report_test_inoutaccount> reportAgentList = redisHelper.SearchByUserId<dt_report_test_inoutaccount>(redisClient, redisClientKey, iUserId.ToString()).ToList();
        for (var i = 0; i < reportAgentList.Count; i++)
        {
            //中奖总额
            awardMoneyData.AwardMoney += reportAgentList[i].Out_WinningAccount;
        }
    }
    /// <summary>
    /// 获取代理报表－代理的查询条件
    /// </summary>
    /// <param name="reportAgent">会员资金异动报表</param>
    /// <param name="strIssue">期号</param>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strUserId">会员ID</param>
    private bool GetAgentCondition(dt_report_agent reportAgent, string strStartDate, string strEndDate, string strIdentityId, string strUserId)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= reportAgent.identityid == strIdentityId;
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= reportAgent.AddTime >= DateTime.Parse(strStartDate) && reportAgent.AddTime <= DateTime.Parse(strEndDate);
        }
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            boolResult &= reportAgent.user_id == int.Parse(strUserId);
        }
        return boolResult;
    }
    /// <summary>
    /// 获取代理报表－会员的查询条件
    /// </summary>
    /// <param name="reportInOutAccount">会员资金异动报表</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">结束日期</param>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strUserId">会员ID</param>
    private bool GetUserCondition(dt_report_inoutaccount reportInOutAccount, string strStartDate, string strEndDate, string strIdentityId, string strUserId)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= reportInOutAccount.identityid == strIdentityId;
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= int.Parse(reportInOutAccount.AddTime.ToString("yyyyMMdd")) >= int.Parse(DateTime.Parse(strStartDate).ToString("yyyyMMdd")) &&
                int.Parse(reportInOutAccount.AddTime.ToString("yyyyMMdd")) <= int.Parse(DateTime.Parse(strEndDate).ToString("yyyyMMdd"));
        }
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            boolResult &= reportInOutAccount.user_id == int.Parse(strUserId);
        }
        return boolResult;
    }
    /// <summary>
    /// 代理报表查询－－－－－－前台的[下级报表 GetAgentReport]的功能
    /// </summary>
    /// <param name="context"></param>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetAgentReport(string IdentityId, int UserId, string UserName, int IsAgent, DateTime StartTime, DateTime EndTime, int ClassNum, int LoginId, int flog, int Index, int DataNum)
    {
        ResultInfoT<List<AgentReport>> ResultInfo = new ResultInfoT<List<AgentReport>>();
        try
        {
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            if (!string.IsNullOrWhiteSpace(strMsg))
            {
                return strMsg;
            }
            //RedisClientConfig redisClientConfig = new RedisClientConfig();
            //redisClientConfig.ServerIP = DBcon.DBcontring.redisServerIP;
            var redisClient = GlobalVariable.redisClient;
            var redisClientKey = GlobalVariable.redisClientKey;
            List<AgentReport> agentReportList = new List<AgentReport>();
            int Count = 1;
            if (LoginId == UserId)
            {
                ClassNum = 1;
            }
            if (IsAgent > 0)
            {
                string strSql = "select id,user_name,is_agent from dt_users where identityid='" + IdentityId + "' and agent_id=" + UserId;
                DataTable userDataTable = DBcon.DbHelperSQL.Query(strSql).Tables[0];
                Dictionary<int, int> AgentClassList = new Dictionary<int, int>();
                List<int> MemberClassList = new List<int>();
                Dictionary<int, int> TempClass = new Dictionary<int, int>();
                AgentClassList.Add(UserId,UserId);
                for (int i = 0; i < userDataTable.Rows.Count; i++)
                {
                    string strIsAgent = userDataTable.Rows[i]["is_agent"].ToString();
                    string strId = userDataTable.Rows[i]["id"].ToString();
                    int iId = int.Parse(strId);
                    if (strIsAgent.ToLower() == "true")
                    {
                        AgentClassList.Add(iId,iId);
                    }
                    else
                    {
                        MemberClassList.Add(iId);
                    }
                }
                //正式账号
                if (flog == 0)
                {
                    //获取正式代理下级数据
                    string strResult = GetAgentData(MemberClassList, redisClient,redisClientKey, IdentityId, UserId, StartTime, EndTime, UserName, ClassNum, LoginId, userDataTable, agentReportList, ResultInfo, AgentClassList);
                    if (!string.IsNullOrWhiteSpace(strResult))
                    {
                        return strResult;
                    }
                }
                else //测试账号
                {
                    //获取测试代理下级数据
                    string strResult = GetTestAgentData(MemberClassList, redisClient,redisClientKey, IdentityId, UserId, StartTime, EndTime, UserName, ClassNum, LoginId, userDataTable, agentReportList, ResultInfo, AgentClassList);
                    if (!string.IsNullOrWhiteSpace(strResult))
                    {
                        return strResult;
                    }
                }
                agentReportList = agentReportList.OrderByDescending(m => m.BetMoney).ToList();
                Count = agentReportList.Count;
                agentReportList = agentReportList.Skip(Index * DataNum).Take(DataNum).ToList();
            }
            else
            {
                //正式账号
                if (flog == 0)
                {
                    //获取正式会员本身数据
                    GetUserData(redisClient,redisClientKey, UserId, IdentityId, StartTime, EndTime, UserName, ClassNum, agentReportList);
                }
                else //测试账号
                {
                    //获取测试会员本身数据
                    GetTestUserData(redisClient,redisClientKey, UserId, IdentityId, StartTime, EndTime, UserName, ClassNum, agentReportList);
                }
            }
            redisClient.Quit();
            redisClient.Dispose();
            redisClientKey.Quit();
            redisClientKey.Dispose();
            ResultInfo.Code = 1;
            ResultInfo.DataCount = Count;
            ResultInfo.BackData = agentReportList;
            return LitJson.JsonMapper.ToJson(ResultInfo);
        }
        catch (Exception ex)
        {
            ResultInfo.Code = -1;
            ResultInfo.DataCount = 0;
            ResultInfo.StrCode = ex.Message;
            EasyFrame.NewCommon.Log.Error(ex);
            return LitJson.JsonMapper.ToJson(ResultInfo);
        }
    }
    /// <summary>
    /// 获取正式代理数据
    /// </summary>
    /// <param name="MemberClassList">会员所有层级用户ID</param>
    /// <param name="redisClient">Redis操作类</param>
    /// <param name="IdentityId">站长ID</param>
    /// <param name="UserId">用户ID</param>
    /// <param name="StartTime">开始时间</param>
    /// <param name="EndTime">结束时间</param>
    /// <param name="UserName">会员名称</param>
    /// <param name="ClassNum">代理层级</param>
    /// <param name="LoginId">登录账号ID</param>
    /// <param name="UsersList">用户数据</param>
    /// <param name="agentReportList">代理报表汇总数据</param>
    private string GetAgentData(List<int> MemberClassList, RedisClient redisClient,RedisClient redisClientKey, string IdentityId, int UserId, DateTime StartTime, DateTime EndTime, string UserName, int ClassNum,
         int LoginId, DataTable UsersList, List<AgentReport> agentReportList, ResultInfoT<List<AgentReport>> ResultInfo, Dictionary<int,int> AgentClassList)
    {
        //List<dt_report_agent> reportAgentList = redisClient.GetAll<dt_report_agent>().Where(m => m.identityid == IdentityId && AgentClassList.ToArray().Contains(m.user_id) && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
        List<dt_report_agent> reportAgentList = redisHelper.Search<dt_report_agent>(redisClient, redisClientKey, StartTime, EndTime, "", "").Where(m => m.identityid == IdentityId && AgentClassList.ContainsKey(m.user_id)).ToList();
        //判断是否有数据。
        if (reportAgentList.Count < 1)
        {
            ResultInfo.Code = 1;
            ResultInfo.BackData = agentReportList;
            return LitJson.JsonMapper.ToJson(ResultInfo);
        }
        List<dt_report_inoutaccount> reportMemberList = new List<dt_report_inoutaccount>();
        if (MemberClassList.Count > 0)
        {
            //reportMemberList = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.identityid == IdentityId && m.agent_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            reportMemberList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, StartTime, EndTime, UserId.ToString(), "").Where(m => m.identityid == IdentityId).ToList();
        }
        dt_report_agent reportAgent = new dt_report_agent();
        Dictionary<int, int> dicAgent = new Dictionary<int, int>();
        #region 代理数据汇总
        //var reportNumList = redisClient.GetAll<dt_report_num>().Where(m => m.identityid == IdentityId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
        List<dt_report_num> reportNumList = redisHelper.Search<dt_report_num>(redisClient, redisClientKey, StartTime, EndTime, "", "").Where(m => m.identityid == IdentityId).ToList();
        decimal isLower = 0m;
        for (int i = 0; i < reportAgentList.Count; i++)
        {
            if (dicAgent.ContainsKey(reportAgentList[i].user_id))
            {
                continue;
            }
            dicAgent.Add(reportAgentList[i].user_id, 1);
            AgentReport agentReport = new AgentReport();
            agentReport.UserId = reportAgentList[i].user_id;
            agentReport.UserName = UserName;
            agentReport.UserType = ClassNum + "级代理";
            agentReport.IsAgent = false;
            List<dt_report_agent> Items = reportAgentList.Where(x => x.user_id == reportAgentList[i].user_id).ToList();
            for (int k = 0; k < Items.Count; k++)
            {
                agentReport.BetMoney += Items[k].Out_BettingAccount;
                agentReport.BetMoney += Items[k].Lower_Out_BettingAccount;
                isLower += Items[k].Lower_Out_BettingAccount;
                agentReport.WinMoney += Items[k].Out_WinningAccount;
                agentReport.WinMoney += Items[k].Lower_Out_WinningAccount;
                agentReport.RebateMoney += Items[k].Out_RebateAccount;
                agentReport.RebateMoney += Items[k].Lower_Out_RebateAccount;
                agentReport.DiscountMoney += Items[k].ActivityDiscountMoney;
                agentReport.DiscountMoney += Items[k].Lower_ActivityDiscountMoney;
                agentReport.DiscountMoney += Items[k].OtherDiscountMoney;
                agentReport.DiscountMoney += Items[k].Lower_OtherDiscountMoney;
            }
            //下级投注汇总大于0时,表示有下级在投注
            agentReport.BetNum = 0;
            if (isLower > 0 && reportAgentList[i].user_id != UserId)
            {
                agentReport.IsAgent = true;
                //agentReport.BetNum = 1;
            }
            if (reportAgentList[i].user_id != UserId)
            {
                if (UserId != LoginId)
                {
                    agentReport.UserType = ClassNum + 1 + "级代理";
                }
                DataRow[] userDataRow = UsersList.Select("id=" + reportAgentList[i].user_id);
                if (userDataRow != null && userDataRow.Length > 0)
                {
                    agentReport.UserName = userDataRow[0]["user_name"].ToString();
                }
            }
            else
            {
                if (LoginId == reportAgentList[i].user_id)
                {
                    agentReport.UserType = "总代";
                    continue;
                }
                agentReport.BetMoney = 0;
                agentReport.WinMoney = 0;
                agentReport.RebateMoney = 0;
                agentReport.DiscountMoney = 0;
                for (int k = 0; k < Items.Count; k++)
                {
                    agentReport.BetMoney += Items[k].Out_BettingAccount;
                    agentReport.WinMoney += Items[k].Out_WinningAccount;
                    agentReport.RebateMoney += Items[k].Out_RebateAccount;
                    agentReport.DiscountMoney += Items[k].ActivityDiscountMoney;
                    agentReport.DiscountMoney += Items[k].OtherDiscountMoney;
                }
                if (isLower != agentReport.BetMoney)
                {
                    agentReport.BetNum = 1;
                }
            }
            isLower = 0;
            //盈利＝中奖总额－投注总额+活动优惠+返点
            agentReport.ProfitMoney = agentReport.WinMoney - agentReport.BetMoney + agentReport.DiscountMoney + agentReport.RebateMoney;

            if (agentReport.ProfitMoney > 0 || agentReport.WinMoney > 0 || agentReport.BetMoney > 0 || agentReport.DiscountMoney > 0 || agentReport.RebateMoney > 0)
            {
                if (agentReport.BetNum == 0)
                {
                    //查询投注人数
                    string strSql = "select user_id from dt_users_class where identityid='" + IdentityId + "' and father_id=" + agentReport.UserId;
                    DataTable usersClassDataTable = DBcon.DbHelperSQL.Query(strSql).Tables[0];
                    Dictionary<int, int> ClassListss = new Dictionary<int, int>();
                    ClassListss.Add(agentReport.UserId, agentReport.UserId);
                    for (int k = 0; k < usersClassDataTable.Rows.Count; k++)
                    {
                        string strUserId = usersClassDataTable.Rows[k]["user_id"].ToString();
                        ClassListss.Add(int.Parse(strUserId), int.Parse(strUserId));
                    }
                    //投注人数
                    List<dt_report_num> tempreportAgentList = reportNumList.Where(m => m.identityid == IdentityId && ClassListss.ContainsKey(m.user_id) && m.Out_BettingAccount_Num > 0 && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
                    agentReport.BetNum = tempreportAgentList.GroupBy(m => m.user_id).Count();
                }
                if (agentReport.BetMoney == 0)
                {
                    agentReport.BetNum = 0;
                }
                agentReportList.Add(agentReport);
            }
        }
        #endregion
        #region  会员数据汇总
        for (int i = 0; i < reportMemberList.Count; i++)
        {
            if (dicAgent.ContainsKey(reportMemberList[i].user_id))
            {
                continue;
            }
            dicAgent.Add(reportMemberList[i].user_id, 1);
            AgentReport agentReport = new AgentReport();
            agentReport.UserId = reportMemberList[i].user_id;
            agentReport.UserName = UserName;
            if (reportMemberList[i].user_id != UserId)
            {
                DataRow[] userDataRow = UsersList.Select("id=" + reportMemberList[i].user_id);
                if (userDataRow != null && userDataRow.Length > 0)
                {
                    agentReport.UserName = userDataRow[0]["user_name"].ToString();
                }
            }
            agentReport.UserType = ClassNum + "级玩家";
            agentReport.IsAgent = false;
            List<dt_report_inoutaccount> Items = reportMemberList.Where(x => x.user_id == reportMemberList[i].user_id).ToList();
            for (int k = 0; k < Items.Count; k++)
            {
                agentReport.BetMoney += Items[k].Out_BettingAccount;
                agentReport.WinMoney += Items[k].Out_WinningAccount;
                agentReport.RebateMoney += Items[k].Out_RebateAccount;
                agentReport.DiscountMoney += Items[k].Out_ActivityDiscountAccount;
                agentReport.DiscountMoney += Items[k].Out_OtherDiscountAccount;
            }
            //盈利＝中奖总额－投注总额+活动优惠+返点
            agentReport.ProfitMoney = agentReport.WinMoney - agentReport.BetMoney + agentReport.DiscountMoney + agentReport.RebateMoney;
            agentReport.BetNum = 1;
            agentReportList.Add(agentReport);
        }
        #endregion
        return string.Empty;
    }
    /// <summary>
    /// 获取正式会员数据
    /// </summary>
    /// <param name="redisClient">Redis操作类</param>
    /// <param name="UserId">用户ID</param>
    /// <param name="IdentityId">站长ID</param>
    /// <param name="StartTime">起始日期</param>
    /// <param name="EndTime">结束日期</param>
    /// <param name="UserName">用户名称</param>
    /// <param name="ClassNum">用户层级</param>
    /// <param name="agentReportList">代理报表汇总数据</param>
    private void GetUserData(RedisClient redisClient,RedisClient redisClientKey, int UserId, string IdentityId, DateTime StartTime, DateTime EndTime, string UserName, int ClassNum, List<AgentReport> agentReportList)
    {
        //List<dt_report_inoutaccount> reportMemberList = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.identityid == IdentityId && m.user_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
        List<dt_report_inoutaccount> reportMemberList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, StartTime, EndTime, UserId.ToString(), "").Where(m => m.identityid == IdentityId).ToList();
        AgentReport agentReport = new AgentReport();
        agentReport.UserId = UserId;
        agentReport.UserName = UserName;
        agentReport.UserType = ClassNum + "级玩家";
        agentReport.IsAgent = false;
        for (int k = 0; k < reportMemberList.Count; k++)
        {
            agentReport.BetMoney += reportMemberList[k].Out_BettingAccount;
            agentReport.WinMoney += reportMemberList[k].Out_WinningAccount;
            agentReport.RebateMoney += reportMemberList[k].Out_RebateAccount;
            agentReport.DiscountMoney += reportMemberList[k].Out_ActivityDiscountAccount;
            agentReport.DiscountMoney += reportMemberList[k].Out_OtherDiscountAccount;
        }
        //盈利＝中奖总额－投注总额+活动优惠+返点
        agentReport.ProfitMoney = agentReport.WinMoney - agentReport.BetMoney + agentReport.DiscountMoney + agentReport.RebateMoney;
        agentReport.BetNum = 1;
        agentReportList.Add(agentReport);
    }
    /// <summary>
    /// 获取测试代理数据
    /// </summary>
    /// <param name="MemberClassList">会员所有层级用户ID</param>
    /// <param name="redisClient">Redis操作类</param>
    /// <param name="IdentityId">站长ID</param>
    /// <param name="UserId">用户ID</param>
    /// <param name="StartTime">开始时间</param>
    /// <param name="EndTime">结束时间</param>
    /// <param name="UserName">会员名称</param>
    /// <param name="ClassNum">代理层级</param>
    /// <param name="LoginId">登录账号ID</param>
    /// <param name="UsersList">用户数据</param>
    /// <param name="agentReportList">代理报表汇总数据</param>
    private string GetTestAgentData(List<int> MemberClassList, RedisClient redisClient, RedisClient redisClientKey, string IdentityId, int UserId, DateTime StartTime, DateTime EndTime, string UserName, int ClassNum,
           int LoginId, DataTable UsersList, List<AgentReport> agentReportList, ResultInfoT<List<AgentReport>> ResultInfo,  Dictionary<int,int> AgentClassList)
    {
       // List<dt_report_test_agent> reportTestAgentList = redisClient.GetAll<dt_report_test_agent>().Where(m => m.identityid == IdentityId && AgentClassList.ToArray().Contains(m.user_id) && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
        List<dt_report_test_agent> reportTestAgentList = redisHelper.Search<dt_report_test_agent>(redisClient, redisClientKey, StartTime, EndTime, "", "").Where(m => m.identityid == IdentityId && AgentClassList.ContainsKey(m.user_id)).ToList();
        //判断是否有数据。
        if (reportTestAgentList.Count < 1)
        {
            ResultInfo.Code = 1;
            ResultInfo.BackData = agentReportList;
            return LitJson.JsonMapper.ToJson(ResultInfo);
        }
        List<dt_report_test_inoutaccount> reportMemberList = new List<dt_report_test_inoutaccount>();
        if (MemberClassList.Count > 0)
        {
            //reportMemberList = redisClient.GetAll<dt_report_test_inoutaccount>().Where(m => m.identityid == IdentityId && m.agent_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            reportMemberList = redisHelper.Search<dt_report_test_inoutaccount>(redisClient, redisClientKey, StartTime, EndTime, UserId.ToString(), "").Where(m => m.identityid == IdentityId).ToList();
        }
        dt_report_test_agent reportTestAgent = new dt_report_test_agent();
        Dictionary<int, int> dicAgent = new Dictionary<int, int>();
        #region 代理数据汇总
        //var reportNumList = redisClient.GetAll<dt_report_test_num>().Where(m => m.identityid == IdentityId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
        List<dt_report_test_num> reportNumList = redisHelper.Search<dt_report_test_num>(redisClient, redisClientKey, StartTime, EndTime, "", "").Where(m => m.identityid == IdentityId).ToList();
        decimal isLower = 0m;
        for (int i = 0; i < reportTestAgentList.Count; i++)
        {
            if (dicAgent.ContainsKey(reportTestAgentList[i].user_id))
            {
                continue;
            }
            //Log.Info(reportTestAgentList[i].user_id.ToString());
            dicAgent.Add(reportTestAgentList[i].user_id, 1);
            //Log.Info("OK");
            AgentReport agentReport = new AgentReport();
            agentReport.UserId = reportTestAgentList[i].user_id;
            agentReport.UserName = UserName;
            agentReport.UserType = ClassNum + "级代理";
            agentReport.IsAgent = false;
            List<dt_report_test_agent> Items = reportTestAgentList.Where(x => x.user_id == reportTestAgentList[i].user_id).ToList();
            for (int k = 0; k < Items.Count; k++)
            {
                agentReport.BetMoney += Items[k].Out_BettingAccount;
                agentReport.BetMoney += Items[k].Lower_Out_BettingAccount;
                isLower += Items[k].Lower_Out_BettingAccount;
                agentReport.WinMoney += Items[k].Out_WinningAccount;
                agentReport.WinMoney += Items[k].Lower_Out_WinningAccount;
                agentReport.RebateMoney += Items[k].Out_RebateAccount;
                agentReport.RebateMoney += Items[k].Lower_Out_RebateAccount;
                agentReport.DiscountMoney += Items[k].ActivityDiscountMoney;
                agentReport.DiscountMoney += Items[k].Lower_ActivityDiscountMoney;
                agentReport.DiscountMoney += Items[k].OtherDiscountMoney;
                agentReport.DiscountMoney += Items[k].Lower_OtherDiscountMoney;
            }
            //下级投注汇总大于0时,表示有下级在投注
            agentReport.BetNum = 0;
            if (isLower > 0 && reportTestAgentList[i].user_id != UserId)
            {
                agentReport.IsAgent = true;
                //agentReport.BetNum = 1;
            }
            if (reportTestAgentList[i].user_id != UserId)
            {
                if (UserId != LoginId)
                {
                    agentReport.UserType = ClassNum + 1 + "级代理";
                }
                DataRow[] userDataRows = UsersList.Select("id=" + reportTestAgentList[i].user_id);
                if (userDataRows != null && userDataRows.Length>0)
                {
                    agentReport.UserName = userDataRows[0]["user_name"].ToString();
                }
            }
            else
            {
                if (LoginId == reportTestAgentList[i].user_id)
                {
                    agentReport.UserType = "总代";
                    continue;
                }
                agentReport.BetMoney = 0;
                agentReport.WinMoney = 0;
                agentReport.RebateMoney = 0;
                agentReport.DiscountMoney = 0;
                for (int k = 0; k < Items.Count; k++)
                {
                    agentReport.BetMoney += Items[k].Out_BettingAccount;
                    agentReport.WinMoney += Items[k].Out_WinningAccount;
                    agentReport.RebateMoney += Items[k].Out_RebateAccount;
                    agentReport.DiscountMoney += Items[k].ActivityDiscountMoney;
                    agentReport.DiscountMoney += Items[k].OtherDiscountMoney;
                }
                if (isLower != agentReport.BetMoney)
                {
                    agentReport.BetNum = 1;
                }
            }
            isLower = 0;
            //盈利＝中奖总额－投注总额+活动优惠+返点
            agentReport.ProfitMoney = agentReport.WinMoney - agentReport.BetMoney + agentReport.DiscountMoney + agentReport.RebateMoney;

            if (agentReport.ProfitMoney > 0 || agentReport.WinMoney > 0 || agentReport.BetMoney > 0 || agentReport.DiscountMoney > 0 || agentReport.RebateMoney > 0)
            {
                if (agentReport.BetNum == 0)
                {
                    //查询投注人数
                    string strSql = "select user_id from dt_users_class where identityid='" + IdentityId + "' and father_id=" + agentReport.UserId;
                    DataTable userClassDataTable = DBcon.DbHelperSQL.Query(strSql).Tables[0];
                    Dictionary<int, int> ClassListss = new Dictionary<int, int>();
                    if (!ClassListss.ContainsKey(agentReport.UserId))
                    {
                        ClassListss.Add(agentReport.UserId, agentReport.UserId);
                    }
                    for (int k = 0; k < userClassDataTable.Rows.Count; k++)
                    {
                        int strUserId = Convert.ToInt32(userClassDataTable.Rows[i]["user_id"]);
                        if (ClassListss.ContainsKey(strUserId))
                        {
                            continue;
                        }
                        ClassListss.Add(strUserId, strUserId);
                    }
                    //投注人数
                    List<dt_report_test_num> tempreportAgentList = reportNumList.Where(m => m.identityid == IdentityId && ClassListss.ContainsKey(m.user_id) && m.Out_BettingAccount_Num > 0 && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
                    agentReport.BetNum = tempreportAgentList.GroupBy(m => m.user_id).Count();
                }
                if (agentReport.BetMoney == 0)
                {
                    agentReport.BetNum = 0;
                }
                agentReportList.Add(agentReport);
            }
        }
        #endregion
        #region  会员数据汇总
        for (int i = 0; i < reportMemberList.Count; i++)
        {
            if (dicAgent.ContainsKey(reportMemberList[i].user_id))
            {
                continue;
            }
            //Log.Info(reportTestAgentList[i].user_id.ToString());
            dicAgent.Add(reportMemberList[i].user_id, 1);
            //Log.Info("OK");
            AgentReport agentReport = new AgentReport();
            agentReport.UserId = reportMemberList[i].user_id;
            agentReport.UserName = UserName;
            if (reportMemberList[i].user_id != UserId)
            {
                DataRow[] userDataRows = UsersList.Select("id=" + reportMemberList[i].user_id);
                if (userDataRows != null && userDataRows.Length>0)
                {
                    agentReport.UserName = userDataRows[0]["user_name"].ToString();
                }
            }
            agentReport.UserType = ClassNum + "级玩家";
            agentReport.IsAgent = false;
            List<dt_report_test_inoutaccount> Items = reportMemberList.Where(x => x.user_id == reportMemberList[i].user_id).ToList();
            for (int k = 0; k < Items.Count; k++)
            {
                agentReport.BetMoney += Items[k].Out_BettingAccount;
                agentReport.WinMoney += Items[k].Out_WinningAccount;
                agentReport.RebateMoney += Items[k].Out_RebateAccount;
                agentReport.DiscountMoney += Items[k].Out_ActivityDiscountAccount;
                agentReport.DiscountMoney += Items[k].Out_OtherDiscountAccount;
            }
            //盈利＝中奖总额－投注总额+活动优惠+返点
            agentReport.ProfitMoney = agentReport.WinMoney - agentReport.BetMoney + agentReport.DiscountMoney + agentReport.RebateMoney;
            agentReport.BetNum = 1;
            agentReportList.Add(agentReport);
        }
        #endregion
        return string.Empty;
    }
    /// <summary>
    /// 获取测试会员数据
    /// </summary>
    /// <param name="redisClient">Redis操作类</param>
    /// <param name="UserId">用户ID</param>
    /// <param name="IdentityId">站长ID</param>
    /// <param name="StartTime">起始日期</param>
    /// <param name="EndTime">结束日期</param>
    /// <param name="UserName">用户名称</param>
    /// <param name="ClassNum">用户层级</param>
    /// <param name="agentReportList">代理报表汇总数据</param>
    private void GetTestUserData(RedisClient redisClient,RedisClient redisClientKey, int UserId, string IdentityId, DateTime StartTime, DateTime EndTime, string UserName, int ClassNum, List<AgentReport> agentReportList)
    {
        //List<dt_report_test_inoutaccount> reportTestMemberList = redisClient.GetAll<dt_report_test_inoutaccount>().Where(m => m.identityid == IdentityId && m.user_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
        List<dt_report_test_inoutaccount> reportTestMemberList = redisHelper.Search<dt_report_test_inoutaccount>(redisClient, redisClientKey, StartTime, EndTime, UserId.ToString(), "").Where(m => m.identityid == IdentityId).ToList();
        AgentReport agentReport = new AgentReport();
        agentReport.UserId = UserId;
        agentReport.UserName = UserName;
        agentReport.UserType = ClassNum + "级玩家";
        agentReport.IsAgent = false;
        for (int k = 0; k < reportTestMemberList.Count; k++)
        {
            agentReport.BetMoney += reportTestMemberList[k].Out_BettingAccount;
            agentReport.WinMoney += reportTestMemberList[k].Out_WinningAccount;
            agentReport.RebateMoney += reportTestMemberList[k].Out_RebateAccount;
            agentReport.DiscountMoney += reportTestMemberList[k].Out_ActivityDiscountAccount;
            agentReport.DiscountMoney += reportTestMemberList[k].Out_OtherDiscountAccount;
        }
        //盈利＝中奖总额－投注总额+活动优惠+返点
        agentReport.ProfitMoney = agentReport.WinMoney - agentReport.BetMoney + agentReport.DiscountMoney + agentReport.RebateMoney;
        agentReport.BetNum = 1;
        agentReportList.Add(agentReport);
    }
    /// <summary>
    /// 前台获取中奖总额前10名排行榜－－－－－－前台的[获取昨日中奖总额前十__昨日奖金榜   GetWinMoneyInTop]的功能
    /// </summary>
    /// <param name="context"></param>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetWinMoneyInTop(string strDay)
    {
        ResultInfoT<List<WinMoneyInTop>> resultInfo = new ResultInfoT<List<WinMoneyInTop>>();
        try
        {
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            if (!string.IsNullOrWhiteSpace(strMsg))
            {
                return strMsg;
            }
            //1、获取会员资金异动表数据
            //RedisClientConfig redisClientConfig = new RedisClientConfig();
            //redisClientConfig.ServerIP = DBcon.DBcontring.redisServerIP;
            var redisClient = GlobalVariable.redisClient;
            var redisClientKey = GlobalVariable.redisClientKey;
           // List<dt_report_inoutaccount> reportreportAgentList = redisClient.GetAll<dt_report_inoutaccount>().Where(m => int.Parse(m.AddTime.ToString("yyyyMMdd")) == int.Parse(DateTime.Parse(strDay).ToString("yyyyMMdd"))).ToList();
            List<dt_report_inoutaccount> reportreportAgentList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, DateTime.Parse(strDay), DateTime.Parse(strDay), "", "").ToList();
            List<WinMoneyInTop> betMoneyInTopList = new List<WinMoneyInTop>();
            Dictionary<string, WinMoneyInTop> dicBetMoneyInTop = new Dictionary<string, WinMoneyInTop>();
            //2、统计会员投注额
            for (int i = 0; i < reportreportAgentList.Count; i++)
            {
                string strUserId = reportreportAgentList[i].user_id.ToString();
                WinMoneyInTop betMoneyInTop = new WinMoneyInTop();
                if (!dicBetMoneyInTop.ContainsKey(strUserId))
                {
                    dicBetMoneyInTop.Add(strUserId, betMoneyInTop);
                    betMoneyInTopList.Add(betMoneyInTop);
                }
                betMoneyInTop = dicBetMoneyInTop[strUserId];
                betMoneyInTop.UserId = int.Parse(strUserId);
                betMoneyInTop.IdentityId = reportreportAgentList[i].identityid;
                betMoneyInTop.BetMoney += reportreportAgentList[i].Out_BettingAccount;
                betMoneyInTop.WinMoney += reportreportAgentList[i].Out_WinningAccount;
            }
            betMoneyInTopList = betMoneyInTopList.OrderByDescending(m => m.WinMoney).Take(10).ToList();
            redisClient.Quit();
            redisClient.Dispose();
            redisClientKey.Quit();
            redisClientKey.Dispose();
            resultInfo.Code = 1;
            resultInfo.DataCount = betMoneyInTopList.Count;
            resultInfo.BackData = betMoneyInTopList;
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
        catch (Exception ex)
        {
            resultInfo.Code = -1;
            resultInfo.StrCode = ex.Message;
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
    }
    /// <summary>
    /// 前台获取出入款平均时间－－－－－－前台的[获取服务体验时间充值时间和提现时间  GetWithdrawTime]的功能
    /// </summary>
    /// <param name="context"></param>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetWithdrawTime(string strDay)
    {
        ResultInfoT<List<WithdrawTimeEntity>> resultInfo = new ResultInfoT<List<WithdrawTimeEntity>>();
        try
        {
            //验证是否有权访问  
            //string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return strMsg;
            //}
            //1、获取会员资金异动表数据
            //RedisClientConfig redisClientConfig = new RedisClientConfig();
            //redisClientConfig.ServerIP = DBcon.DBcontring.redisServerIP;
            var redisClient = GlobalVariable.redisClient;
            var redisClientKey = GlobalVariable.redisClientKey;
            //提现时间合计
            double fTotalWithdrawTime = 0;
            //提现次数
            double fWithdrawNum = 0;
            //充值时间合计
            double fTotalRechargeTime = 0;
            //充值次数
            double fRechargeTimeNum = 0;
            DataTable tenantDataTable = DBcon.DbHelperSQL.Query("select identityid from dt_tenant").Tables[0];
            List<WithdrawTimeEntity> BTimeList = new List<WithdrawTimeEntity>();
            int wTime = 0;
            int rTime = 0;
            List<dt_report_inoutaccount> PerList = new List<dt_report_inoutaccount>();
            PerList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, DateTime.Parse(strDay), DateTime.Parse(strDay), "", "").ToList();
            for (int k = 0; k < tenantDataTable.Rows.Count; k++)
            {
                string strIdentityId = tenantDataTable.Rows[k]["identityid"].ToString();
                var tempList = PerList.Where(m => m.identityid == strIdentityId).ToList();
                //2、统计会员投注额
                WithdrawTimeEntity withdrawTime = new WithdrawTimeEntity();
                for (int i = 0; i < tempList.Count; i++)
                {
                    //提现
                    fTotalWithdrawTime += tempList[i].OutMoneyTime;
                    fWithdrawNum += tempList[i].OutMoneyNum;
                    //充值
                    fTotalRechargeTime += tempList[i].InMoneyTime;
                    fRechargeTimeNum += tempList[i].InMoneyNum;
                }
                //超过1小时，只取分钟并且换算成秒
                FormatHelper formatHelper = new FormatHelper();
                //提现平均时间
                double fAverageWithdrawTime = 0;
                if (fWithdrawNum != 0)
                {
                    fAverageWithdrawTime = fTotalWithdrawTime / fWithdrawNum;
                    wTime = formatHelper.GetSecond(fAverageWithdrawTime);
                }
                //充值平均时间
                double fAverageRechargeTime = 0;
                if (fRechargeTimeNum != 0)
                {
                    fAverageRechargeTime = fTotalRechargeTime / fRechargeTimeNum;
                    rTime = formatHelper.GetSecond(fAverageRechargeTime);
                }
                fTotalWithdrawTime = 0;
                fWithdrawNum = 0;
                fTotalRechargeTime = 0;
                fRechargeTimeNum = 0;
                withdrawTime.IdentityId = strIdentityId;
                withdrawTime.RechargeTime = rTime;
                withdrawTime.WithdrawTime = wTime;
                BTimeList.Add(withdrawTime);
                wTime = 0;
                rTime = 0;
            }
            redisClient.Quit();
            redisClient.Dispose();
            redisClientKey.Quit();
            redisClientKey.Dispose();
            resultInfo.Code = 1;
            resultInfo.BackData = BTimeList;
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
        catch (Exception ex)
        {
            resultInfo.Code = -1;
            resultInfo.StrCode = ex.Message;
            Log.Error(ex);
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
    }
    /// <summary>
    /// 前台代理报表数据xt－－－－－－前台的[代理报表  GetAgencyHender]的功能
    /// </summary>
    /// <param name="context"></param>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetAgencyHender(string IdentityId, int UserId, DateTime StartTime, DateTime EndTime, int IsAgent, int flog)
    {
        ResultInfoT<AgentHender> ResultInfo = new ResultInfoT<AgentHender>();
        try
        {
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            if (!string.IsNullOrWhiteSpace(strMsg))
            {
                return strMsg;
            }
            //会员
            string strResult = string.Empty;
            if (flog == 0)
            {
                //获取会员数据
                strResult=GetAgencyData(IsAgent, IdentityId, UserId, StartTime, EndTime, ResultInfo);
                return strResult;
            }
            //获取测试账号数据
            strResult=GetTestAgencyData(IsAgent, IdentityId, UserId, StartTime, EndTime, ResultInfo);
            return strResult;
        }
        catch (Exception ex)
        {
            ResultInfo.Code = -1;
            ResultInfo.DataCount = 0;
            ResultInfo.StrCode = "发生错误";
            Log.Error(ex);
            return LitJson.JsonMapper.ToJson(ResultInfo);
        }
    }
    /// <summary>
    /// 获取代理数据
    /// </summary>
    /// <param name="IsAgent">是否代理</param>
    /// <param name="IdentityId">站长ID</param>
    /// <param name="UserId">会员ID</param>
    /// <param name="StartTime">开始时间</param>
    /// <param name="EndTime">结束时间</param>
    /// <param name="ResultInfo">返加给前端数据</param>
    private string GetAgencyData(int IsAgent, string IdentityId, int UserId, DateTime StartTime, DateTime EndTime, ResultInfoT<AgentHender> ResultInfo)
    {
        var redisClient = GlobalVariable.redisClient;
        var redisClientKey = GlobalVariable.redisClientKey;
        #region 初始化
        decimal BetMoney = 0m;
        decimal Bonus = 0m;
        decimal RebateMoney = 0m;
        decimal RechargeMoney = 0m;
        decimal WithdrawMoney = 0m;
        decimal ActivityMoney = 0m;
        int BetNum = 0;
        int FirstChargeNum = 0;
        int RegisterNum = 0;
        int TeamNum = 0;
        decimal TeamBalance = 0m;
        decimal ProfitMoney = 0m;
        decimal AgentRebate = 0m;
        decimal AgentWages = 0m;
        decimal AgentDividends = 0m;
        #endregion
        AgentHender AgencyHenders = new AgentHender();
        if (IsAgent > 0)//大于0表示是代理
        {
            #region 代理数据获取
            //Log.Info("获取代理数据开始时间：" + DateTime.Now.ToString());
            //List<dt_report_agent> reportAgentList = redisClient.GetAll<dt_report_agent>().Where(m => m.identityid == IdentityId && m.user_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            List<dt_report_agent> reportAgentList = redisHelper.Search<dt_report_agent>(redisClient, redisClientKey, StartTime, EndTime, UserId.ToString(), "").Where(m => m.identityid == IdentityId).ToList();
            //Log.Info("获取代理数据结束时间：" + DateTime.Now.ToString());
            //Log.Info("统计代理数据开始时间：" + DateTime.Now.ToString());
            for (var i = 0; i < reportAgentList.Count; i++)
            {
                //投注总额
                BetMoney += reportAgentList[i].Out_BettingAccount;
                BetMoney += reportAgentList[i].Lower_Out_BettingAccount;
                //中奖总额
                Bonus += reportAgentList[i].Out_WinningAccount;
                Bonus += reportAgentList[i].Lower_Out_WinningAccount;
                //团队返点总额
                RebateMoney += reportAgentList[i].Out_RebateAccount;
                RebateMoney += reportAgentList[i].Lower_Out_RebateAccount;
                //充值总额
                RechargeMoney += reportAgentList[i].InMoney;
                RechargeMoney += reportAgentList[i].Lower_InMoney;
                //提现总额
                WithdrawMoney += reportAgentList[i].Out_Account;
                WithdrawMoney += reportAgentList[i].Lower_Out_Account;
                //活动总额
                ActivityMoney += reportAgentList[i].ActivityDiscountMoney;
                ActivityMoney += reportAgentList[i].Lower_ActivityDiscountMoney;
                ActivityMoney += reportAgentList[i].OtherDiscountMoney;
                ActivityMoney += reportAgentList[i].Lower_OtherDiscountMoney;
                //代理返点总额
                AgentRebate += reportAgentList[i].Out_RebateAccount;
            }
            //Log.Info("统计代理数据结束时间：" + DateTime.Now.ToString());
            //盈利金额
            ProfitMoney = Bonus - BetMoney + ActivityMoney + RebateMoney;
            //查询首充人数
            //Log.Info("获取下级代理数据开始时间：" + DateTime.Now.ToString());
            string strSql = "select user_id from dt_users_class where identityid='" + IdentityId + "' and father_id=" + UserId;
            DataTable userClassDataTable = DBcon.DbHelperSQL.Query(strSql).Tables[0];
            Dictionary<int, int> ClassListss = new Dictionary<int, int>();
            ClassListss.Add(UserId, UserId);
            for (int i = 0; i < userClassDataTable.Rows.Count; i++)
            {
                string strUserId = userClassDataTable.Rows[i]["user_id"].ToString();
                ClassListss.Add(int.Parse(strUserId), int.Parse(strUserId));
            }
            //Log.Info("获取下级代理数据结束时间：" + DateTime.Now.ToString());
            //Log.Info("获取首充数据开始时间：" + DateTime.Now.ToString());
            //List<dt_report_newprepaid> reportNewprepaid = redisClient.GetAll<dt_report_newprepaid>().Where(m => m.identityid == IdentityId && ClassListss.ToArray().Contains(m.user_id) && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            List<dt_report_newprepaid> reportNewprepaid = redisHelper.SearchByUserId<dt_report_newprepaid>(redisClient, redisClientKey, "").Where(m => m.identityid == IdentityId && ClassListss.ContainsKey(m.user_id) && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            FirstChargeNum = reportNewprepaid.Count;
            //Log.Info("获取首充数据结束时间：" + DateTime.Now.ToString());
            //Log.Info("获取投注人数开始时间：" + DateTime.Now.ToString());
            //List<dt_report_num> tempreportAgentList = redisClient.GetAll<dt_report_num>().Where(m => m.identityid == IdentityId && ClassListss.ToArray().Contains(m.user_id) && m.Out_BettingAccount_Num > 0 && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            List<dt_report_num> tempreportAgentList = redisHelper.Search<dt_report_num>(redisClient, redisClientKey, StartTime, EndTime, "", "").Where(m => m.identityid == IdentityId && m.Out_BettingAccount_Num > 0 && ClassListss.ContainsKey(m.user_id)).ToList();
            //投注人数
            BetNum = tempreportAgentList.GroupBy(m => m.user_id).Count();
            //Log.Info("获取投注人数结束时间：" + DateTime.Now.ToString());
            //Log.Info("获取代理工资分红数据开始时间：" + DateTime.Now.ToString());
            //List<dt_report_inoutaccount> reportInAccount = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.identityid == IdentityId && m.user_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            List<dt_report_inoutaccount> reportInAccount = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, StartTime, EndTime, UserId.ToString(), "").Where(m => m.identityid == IdentityId).ToList();
            for (int t = 0; t < reportInAccount.Count; t++)
            {
                //代理工资
                AgentWages += reportInAccount[t].Wages;
                //代理分红
                AgentDividends += reportInAccount[t].Bonus;
            }
            //Log.Info("获取代理工资分红数据结束时间：" + DateTime.Now.ToString());
            //注册人数
            //Log.Info("获取注册人数开始时间：" + DateTime.Now.ToString());
            DataSet Register = DBcon.DbHelperSQL.Query(string.Format("select count(id) from dt_users_class where identityid='{0}' and father_id={1} and addtime>='{2}' and addtime<='{3}'", IdentityId, UserId, StartTime, EndTime));
            if (Register.Tables.Count > 0)
            {
                if (Register.Tables[0].Rows[0][0] != DBNull.Value)
                {
                    RegisterNum = Convert.ToInt32(Register.Tables[0].Rows[0][0]);
                }
            }
            //Log.Info("获取注册人数结束时间：" + DateTime.Now.ToString());
            //Log.Info("获取团队人数开始时间：" + DateTime.Now.ToString());
            //团队人数
            DataSet ClassNum = DBcon.DbHelperSQL.Query(string.Format("select count(id) from dt_users_class where identityid='{0}' and father_id={1} and addtime<='{2}'", IdentityId, UserId, EndTime));
            if (ClassNum.Tables.Count > 0)
            {
                if (ClassNum.Tables[0].Rows[0][0] != DBNull.Value)
                {
                    TeamNum = Convert.ToInt32(ClassNum.Tables[0].Rows[0][0]);
                }
            }
            //Log.Info("获取团队人数结束时间：" + DateTime.Now.ToString());
            //团队余额
            //Log.Info("获取团队余额开始时间：" + DateTime.Now.ToString());
            DataSet TMoney = DBcon.DbHelperSQL.Query(string.Format("select SUM(use_money) from dt_user_capital where user_id in(select user_id from dt_users_class where identityid='{0}' and father_id={1} or user_id={1})", IdentityId, UserId));
            if (TMoney.Tables.Count > 0)
            {
                if (TMoney.Tables[0].Rows[0][0] != DBNull.Value)
                {
                    TeamBalance = Convert.ToDecimal(TMoney.Tables[0].Rows[0][0]);
                }
            }
            //Log.Info("获取团队余额结束时间：" + DateTime.Now.ToString());
            #region 赋值
            AgencyHenders.BetMoney = BetMoney.ToString("#0.00");
            AgencyHenders.Bonus = Bonus.ToString("#0.00");
            AgencyHenders.RebateMoney = RebateMoney.ToString("#0.00");
            AgencyHenders.ActivityMoney = ActivityMoney.ToString("#0.00");
            AgencyHenders.RechargeMoney = RechargeMoney.ToString("#0.00");
            AgencyHenders.WithdrawMoney = WithdrawMoney.ToString("#0.00");
            AgencyHenders.BetNum = BetNum.ToString();
            AgencyHenders.FirstChargeNum = FirstChargeNum.ToString();
            AgencyHenders.TeamNum = TeamNum.ToString();
            AgencyHenders.RegisterNum = RegisterNum.ToString();
            AgencyHenders.TeamBalance = TeamBalance.ToString("#0.00");
            AgencyHenders.ProfitMoney = ProfitMoney.ToString("#0.00");
            AgencyHenders.AgentRebate = AgentRebate.ToString("#0.00");
            AgencyHenders.AgentWages = AgentWages.ToString("#0.00");
            AgencyHenders.AgentDividends = AgentDividends.ToString("#0.00");
            #endregion
            //是否有分红。没有传null
            //Log.Info("是否有分红。没有传null开始时间：" + DateTime.Now.ToString());
            DataTable BounsDataTable = DBcon.DbHelperSQL.Query(string.Format("select top 1 id from dt_agent_bonus where identityid='{0}' and UserId={1}", IdentityId, UserId)).Tables[0];
            if (BounsDataTable.Rows.Count < 1)
            {
                AgencyHenders.AgentDividends = null;
            }
            //Log.Info("是否有分红。没有传null结束时间：" + DateTime.Now.ToString());
            //是否有工资。没有传null
            //Log.Info("是否有工资。没有传null开始时间：" + DateTime.Now.ToString());
            DataTable WageDataTable = DBcon.DbHelperSQL.Query(string.Format("select top 1 id from dt_agent_wageday where identityid='{0}' and UserId={1}", IdentityId, UserId)).Tables[0];
            if (WageDataTable.Rows.Count < 1)
            {
                AgencyHenders.AgentWages = null;
            }
            //Log.Info("是否有工资。没有传null结束时间：" + DateTime.Now.ToString());
            #endregion
        }
        else
        {
            #region 会员数据获取
            //Log.Info("统计会员数据开始时间：" + DateTime.Now.ToString());
            //List<dt_report_inoutaccount> tempreportAgentList = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.identityid == IdentityId && m.user_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            List<dt_report_inoutaccount> tempreportAgentList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, StartTime, EndTime, UserId.ToString(), "").Where(m => m.identityid == IdentityId).ToList();
            for (int k = 0; k < tempreportAgentList.Count; k++)
            {
                //投注总额
                BetMoney += tempreportAgentList[k].Out_BettingAccount;
                //中奖总额
                Bonus += tempreportAgentList[k].Out_WinningAccount;
                //返点总额
                RebateMoney += tempreportAgentList[k].Out_RebateAccount;
                //充值总额
                RechargeMoney += tempreportAgentList[k].In_AlipayPrepaid;
                RechargeMoney += tempreportAgentList[k].In_ArtificialDeposit;
                RechargeMoney += tempreportAgentList[k].In_BankPrepaid;
                RechargeMoney += tempreportAgentList[k].In_FastPrepaid;
                RechargeMoney += tempreportAgentList[k].In_WeChatPrepaid;
                //提现总额
                WithdrawMoney += tempreportAgentList[k].Out_Account;
                //活动总额
                ActivityMoney += tempreportAgentList[k].Out_OtherDiscountAccount;
                ActivityMoney += tempreportAgentList[k].Out_ActivityDiscountAccount;
            }
            //Log.Info("统计会员数据结束时间：" + DateTime.Now.ToString());
            //盈利金额
            ProfitMoney = Bonus - BetMoney + ActivityMoney + RebateMoney;
            //团队余额
            //Log.Info("获取团队余额数据开始时间：" + DateTime.Now.ToString());
            DataSet TMoney = DBcon.DbHelperSQL.Query(string.Format("select use_money from dt_user_capital where identityid='{0}' and user_id={1}", IdentityId, UserId));
            if (TMoney.Tables.Count > 0)
            {
                if (TMoney.Tables[0].Rows[0][0] != DBNull.Value)
                {
                    TeamBalance = Convert.ToDecimal(TMoney.Tables[0].Rows[0][0]);
                }
            }
            //Log.Info("获取团队余额数据结束时间：" + DateTime.Now.ToString());
            #region 赋值
            AgencyHenders.BetMoney = BetMoney.ToString("#0.00");
            AgencyHenders.Bonus = Bonus.ToString("#0.00");
            AgencyHenders.RebateMoney = RebateMoney.ToString("#0.00");
            AgencyHenders.ActivityMoney = ActivityMoney.ToString("#0.00");
            AgencyHenders.RechargeMoney = RechargeMoney.ToString("#0.00");
            AgencyHenders.WithdrawMoney = WithdrawMoney.ToString("#0.00");
            AgencyHenders.ProfitMoney = ProfitMoney.ToString("#0.00");
            AgencyHenders.TeamBalance = TeamBalance.ToString("#0.00");
            AgencyHenders.BetNum = null;
            AgencyHenders.FirstChargeNum = null;
            AgencyHenders.TeamNum = null;
            AgencyHenders.RegisterNum = null;
            AgencyHenders.AgentRebate = null;
            AgencyHenders.AgentWages = null;
            AgencyHenders.AgentDividends = null;
            #endregion
            #endregion
        }
        redisClient.Quit();
        redisClient.Dispose();
        redisClientKey.Quit();
        redisClientKey.Dispose();
        ResultInfo.Code = 1;
        ResultInfo.DataCount = 1;
        ResultInfo.BackData = AgencyHenders;
        string sss = LitJson.JsonMapper.ToJson(ResultInfo);
        //Log.Info("结束：" + sss);
        return sss;
    }
    /// <summary>
    /// 获取代理数据
    /// </summary>
    /// <param name="IsAgent">是否代理</param>
    /// <param name="IdentityId">站长ID</param>
    /// <param name="UserId">会员ID</param>
    /// <param name="StartTime">开始时间</param>
    /// <param name="EndTime">结束时间</param>
    /// <param name="ResultInfo">返加给前端数据</param>
    private string GetTestAgencyData(int IsAgent, string IdentityId, int UserId, DateTime StartTime, DateTime EndTime, ResultInfoT<AgentHender> ResultInfo)
    {
        var redisClient = GlobalVariable.redisClient ;
        var redisClientKey = GlobalVariable.redisClientKey;
        #region 初始化
        decimal BetMoney = 0m;
        decimal Bonus = 0m;
        decimal RebateMoney = 0m;
        decimal RechargeMoney = 0m;
        decimal WithdrawMoney = 0m;
        decimal ActivityMoney = 0m;
        int BetNum = 0;
        int FirstChargeNum = 0;
        int RegisterNum = 0;
        int TeamNum = 0;
        decimal TeamBalance = 0m;
        decimal ProfitMoney = 0m;
        decimal AgentRebate = 0m;
        decimal AgentWages = 0m;
        decimal AgentDividends = 0m;
        #endregion
        AgentHender AgencyHenders = new AgentHender();
        if (IsAgent > 0)//大于0表示是代理
        {
            #region 代理数据获取
            //List<dt_report_test_agent> reportAgentList = redisClient.GetAll<dt_report_test_agent>().Where(m => m.identityid == IdentityId && m.user_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            List<dt_report_test_agent> reportAgentList = redisHelper.Search<dt_report_test_agent>(redisClient, redisClientKey, StartTime, EndTime, UserId.ToString(), "").Where(m => m.identityid == IdentityId).ToList();
            for (var i = 0; i < reportAgentList.Count; i++)
            {
                //投注总额
                BetMoney += reportAgentList[i].Out_BettingAccount;
                BetMoney += reportAgentList[i].Lower_Out_BettingAccount;
                //中奖总额
                Bonus += reportAgentList[i].Out_WinningAccount;
                Bonus += reportAgentList[i].Lower_Out_WinningAccount;
                //团队返点总额
                RebateMoney += reportAgentList[i].Out_RebateAccount;
                RebateMoney += reportAgentList[i].Lower_Out_RebateAccount;
                //充值总额
                RechargeMoney += reportAgentList[i].InMoney;
                RechargeMoney += reportAgentList[i].Lower_InMoney;
                //提现总额
                WithdrawMoney += reportAgentList[i].Out_Account;
                WithdrawMoney += reportAgentList[i].Lower_Out_Account;
                //活动总额
                ActivityMoney += reportAgentList[i].ActivityDiscountMoney;
                ActivityMoney += reportAgentList[i].Lower_ActivityDiscountMoney;
                ActivityMoney += reportAgentList[i].OtherDiscountMoney;
                ActivityMoney += reportAgentList[i].Lower_OtherDiscountMoney;
                //代理返点总额
                AgentRebate += reportAgentList[i].Out_RebateAccount;
            }
            //盈利金额
            ProfitMoney = Bonus - BetMoney + ActivityMoney + RebateMoney;
            //查询首充人数
            string strSql = "select user_id from dt_users_class where identityid='" + IdentityId + "' and father_id=" + UserId;
            DataTable userClassDataTable = DBcon.DbHelperSQL.Query(strSql).Tables[0];
            Dictionary<int, int> ClassListss = new Dictionary<int, int>();
            ClassListss.Add(UserId, UserId);
            for (int i = 0; i < userClassDataTable.Rows.Count; i++)
            {
                string strUserId = userClassDataTable.Rows[i]["user_id"].ToString();
                ClassListss.Add(int.Parse(strUserId), int.Parse(strUserId));
            }
            //List<dt_report_test_newprepaid> reportNewprepaid = redisClient.GetAll<dt_report_test_newprepaid>().Where(m => m.identityid == IdentityId && ClassListss.ToArray().Contains(m.user_id) && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            List<dt_report_test_newprepaid> reportNewprepaid = redisHelper.SearchByUserId<dt_report_test_newprepaid>(redisClient, redisClientKey, "").Where(m => m.identityid == IdentityId && ClassListss.ContainsKey(m.user_id) && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            FirstChargeNum = reportNewprepaid.Count;
            //List<dt_report_test_num> tempreportAgentList = redisClient.GetAll<dt_report_test_num>().Where(m => m.identityid == IdentityId && ClassListss.ToArray().Contains(m.user_id) && m.Out_BettingAccount_Num > 0 && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            List<dt_report_test_num> tempreportAgentList = redisHelper.Search<dt_report_test_num>(redisClient, redisClientKey, StartTime, EndTime, "", "").Where(m => m.identityid == IdentityId && ClassListss.ContainsKey(m.user_id)).ToList();
            //投注人数
            BetNum = tempreportAgentList.GroupBy(m => m.user_id).Count();
            //List<dt_report_test_inoutaccount> reportInAccount = redisClient.GetAll<dt_report_test_inoutaccount>().Where(m => m.identityid == IdentityId && m.user_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            List<dt_report_test_inoutaccount> reportInAccount = redisHelper.Search<dt_report_test_inoutaccount>(redisClient, redisClientKey, StartTime, EndTime,UserId.ToString(), "").Where(m => m.identityid == IdentityId).ToList();
            for (int t = 0; t < reportInAccount.Count; t++)
            {
                //代理工资
                AgentWages += reportInAccount[t].Wages;
                //代理分红
                AgentDividends += reportInAccount[t].Bonus;
            }
            //注册人数
            DataSet Register = DBcon.DbHelperSQL.Query(string.Format("select count(id) from dt_users_class where identityid='{0}' and father_id={1} and addtime>='{2}' and addtime<='{3}'", IdentityId, UserId, StartTime, EndTime));
            if (Register.Tables.Count > 0)
            {
                if (Register.Tables[0].Rows[0][0] != DBNull.Value)
                {
                    RegisterNum = Convert.ToInt32(Register.Tables[0].Rows[0][0]);
                }
            }
            //团队人数
            DataSet ClassNum = DBcon.DbHelperSQL.Query(string.Format("select count(id) from dt_users_class where identityid='{0}' and father_id={1} and addtime<='{2}'", IdentityId, UserId, EndTime));
            if (ClassNum.Tables.Count > 0)
            {
                if (ClassNum.Tables[0].Rows[0][0] != DBNull.Value)
                {
                    TeamNum = Convert.ToInt32(ClassNum.Tables[0].Rows[0][0]);
                }
            }
            //团队余额
            DataSet TMoney = DBcon.DbHelperSQL.Query(string.Format("select SUM(use_money) from dt_user_capital where user_id in(select user_id from dt_users_class where identityid='{0}' and father_id={1} or user_id={1})", IdentityId, UserId));
            if (TMoney.Tables.Count > 0)
            {
                if (TMoney.Tables[0].Rows[0][0] != DBNull.Value)
                {
                    TeamBalance = Convert.ToDecimal(TMoney.Tables[0].Rows[0][0]);
                }
            }
            #region 赋值
            AgencyHenders.BetMoney = BetMoney.ToString("#0.00");
            AgencyHenders.Bonus = Bonus.ToString("#0.00");
            AgencyHenders.RebateMoney = RebateMoney.ToString("#0.00");
            AgencyHenders.ActivityMoney = ActivityMoney.ToString("#0.00");
            AgencyHenders.RechargeMoney = RechargeMoney.ToString("#0.00");
            AgencyHenders.WithdrawMoney = WithdrawMoney.ToString("#0.00");
            AgencyHenders.BetNum = BetNum.ToString();
            AgencyHenders.FirstChargeNum = FirstChargeNum.ToString();
            AgencyHenders.TeamNum = TeamNum.ToString();
            AgencyHenders.RegisterNum = RegisterNum.ToString();
            AgencyHenders.TeamBalance = TeamBalance.ToString("#0.00");
            AgencyHenders.ProfitMoney = ProfitMoney.ToString("#0.00");
            AgencyHenders.AgentRebate = AgentRebate.ToString("#0.00");
            AgencyHenders.AgentWages = AgentWages.ToString("#0.00");
            AgencyHenders.AgentDividends = AgentDividends.ToString("#0.00");
            #endregion
            //是否有分红。没有传null
            DataTable BounsDataTable = DBcon.DbHelperSQL.Query(string.Format("select top 1 id from dt_agent_bonus where identityid='{0}' and UserId={1}", IdentityId, UserId)).Tables[0];
            if (BounsDataTable.Rows.Count < 1)
            {
                AgencyHenders.AgentDividends = null;
            }
            //是否有工资。没有传null
            DataTable WageDataTable = DBcon.DbHelperSQL.Query(string.Format("select top 1 id from dt_agent_wageday where identityid='{0}' and UserId={1}", IdentityId, UserId)).Tables[0];
            if (WageDataTable.Rows.Count < 1)
            {
                AgencyHenders.AgentWages = null;
            }
            #endregion
        }
        else
        {
            #region 会员数据获取
            //List<dt_report_test_inoutaccount> tempreportAgentList = redisClient.GetAll<dt_report_test_inoutaccount>().Where(m => m.identityid == IdentityId && m.user_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            List<dt_report_test_inoutaccount> tempreportAgentList = redisHelper.Search<dt_report_test_inoutaccount>(redisClient, redisClientKey, StartTime, EndTime,UserId.ToString(), "").Where(m => m.identityid == IdentityId).ToList();
            for (int k = 0; k < tempreportAgentList.Count; k++)
            {
                //投注总额
                BetMoney += tempreportAgentList[k].Out_BettingAccount;
                //中奖总额
                Bonus += tempreportAgentList[k].Out_WinningAccount;
                //返点总额
                RebateMoney += tempreportAgentList[k].Out_RebateAccount;
                //充值总额
                RechargeMoney += tempreportAgentList[k].In_AlipayPrepaid;
                RechargeMoney += tempreportAgentList[k].In_ArtificialDeposit;
                RechargeMoney += tempreportAgentList[k].In_BankPrepaid;
                RechargeMoney += tempreportAgentList[k].In_FastPrepaid;
                RechargeMoney += tempreportAgentList[k].In_WeChatPrepaid;
                //提现总额
                WithdrawMoney += tempreportAgentList[k].Out_Account;
                //活动总额
                ActivityMoney += tempreportAgentList[k].Out_OtherDiscountAccount;
                ActivityMoney += tempreportAgentList[k].Out_ActivityDiscountAccount;
            }
            //盈利金额
            ProfitMoney = Bonus - BetMoney + ActivityMoney + RebateMoney;
            //团队余额
            DataSet TMoney = DBcon.DbHelperSQL.Query(string.Format("select use_money from dt_user_capital where identityid='{0}' and user_id={1}", IdentityId, UserId));
            if (TMoney.Tables.Count > 0)
            {
                if (TMoney.Tables[0].Rows[0][0] != DBNull.Value)
                {
                    TeamBalance = Convert.ToDecimal(TMoney.Tables[0].Rows[0][0]);
                }
            }
            #region 赋值
            AgencyHenders.BetMoney = BetMoney.ToString("#0.00");
            AgencyHenders.Bonus = Bonus.ToString("#0.00");
            AgencyHenders.RebateMoney = RebateMoney.ToString("#0.00");
            AgencyHenders.ActivityMoney = ActivityMoney.ToString("#0.00");
            AgencyHenders.RechargeMoney = RechargeMoney.ToString("#0.00");
            AgencyHenders.WithdrawMoney = WithdrawMoney.ToString("#0.00");
            AgencyHenders.ProfitMoney = ProfitMoney.ToString("#0.00");
            AgencyHenders.TeamBalance = TeamBalance.ToString("#0.00");
            AgencyHenders.BetNum = null;
            AgencyHenders.FirstChargeNum = null;
            AgencyHenders.TeamNum = null;
            AgencyHenders.RegisterNum = null;
            AgencyHenders.AgentRebate = null;
            AgencyHenders.AgentWages = null;
            AgencyHenders.AgentDividends = null;
            #endregion
            #endregion
        }
        redisClient.Quit();
        redisClient.Dispose();
        redisClientKey.Quit();
        redisClientKey.Dispose();
        ResultInfo.Code = 1;
        ResultInfo.DataCount = 1;
        ResultInfo.BackData = AgencyHenders;
        return LitJson.JsonMapper.ToJson(ResultInfo);
    }
    /// <summary>
    /// 获取会员的充值总额－－－－－－前台的[获取个人成长值（注册以来的充值总额）   GetRechargeMoney]的功能
    /// </summary>
    /// <param name="UserId"></param>
    /// <param name="IdentityId"></param>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetRechargeMoney(int UserId, string IdentityId)
    {
        string RechargeMoney = "0";
        try
        {
            //RedisClientConfig redisClientConfig = new RedisClientConfig();
            //redisClientConfig.ServerIP = DBcon.DBcontring.redisServerIP;
            var redisClient = GlobalVariable.redisClient;
            var redisClientKey = GlobalVariable.redisClientKey;
            //List<dt_user_summary> UserSummary = redisClient.GetAll<dt_user_summary>().Where(m => m.user_id == UserId && m.identityid == IdentityId).ToList();
            List<dt_user_summary> UserSummary = redisHelper.SearchByUserId<dt_user_summary>(redisClient, redisClientKey,UserId.ToString()).Where(m=>m.identityid==IdentityId).ToList();
            if (UserSummary != null && UserSummary.Count > 0)
            {
                decimal fInMoney = UserSummary[0].In_AlipayPrepaid + UserSummary[0].In_ArtificialDeposit + UserSummary[0].In_BankPrepaid +
                             UserSummary[0].In_FastPrepaid + UserSummary[0].In_WeChatPrepaid;
                RechargeMoney = fInMoney.ToString("#0");
            }
            redisClient.Quit();
            redisClient.Dispose();
            redisClientKey.Quit();
            redisClientKey.Dispose();
        }
        catch (Exception ex)
        {
            RechargeMoney = "0";
            EasyFrame.NewCommon.Log.Error(ex);
        }
        return RechargeMoney;
    }
}

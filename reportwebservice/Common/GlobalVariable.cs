﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SQLite;
using System.IO;
using System.Data;
using ServiceStack.Redis;
using EasyFrame.Redis;

/// <summary>
///GlobalVariable 的摘要说明
/// </summary>
public static class GlobalVariable
{
    /// <summary>
    /// SQLite数据库连接对象
    /// </summary>
    private static SQLiteConnection _mSQLiteConnection = null;
    /// <summary>
    /// SQLite数据库连接对象
    /// </summary>
    public static SQLiteConnection mSQLiteConnection
    {
        get
        {
            if (_mSQLiteConnection == null)
            {
                if (!File.Exists(Config.SQLiteDatabaseFile))
                {
                    File.Create(Config.SQLiteDatabaseFile);
                }
                _mSQLiteConnection = new SQLiteConnection(Config.SQLiteDataSource);
                _mSQLiteConnection.Open();
            }
            if (_mSQLiteConnection.State == ConnectionState.Closed)
            {
                _mSQLiteConnection.Open();
            }
            return _mSQLiteConnection;
        }
        set
        {
            mSQLiteConnection = value;
        }
    }
    /// <summary>
    /// SQLite数据库执行命令对象
    /// </summary>
    private static SQLiteCommand _mSQLiteCommand = null;
    /// <summary>
    /// SQLite数据库执行命令对象
    /// </summary>
    public static SQLiteCommand mSQLiteCommand
    {
        get
        {
            if (_mSQLiteCommand == null)
            {
                _mSQLiteCommand = new SQLiteCommand(mSQLiteConnection);
            }
            return  _mSQLiteCommand;
        }
    }
    /// <summary>
    /// 在线服务redis操作类
    /// </summary>
    public static RedisClient onlineRedisClient
    {
        get
        {
            RedisClientConfig redisClientConfigOnline = new RedisClientConfig();
            string strServerIP = DBcon.DBcontring.redisBeforeServerIP;
            redisClientConfigOnline.ServerIP = strServerIP;
            redisClientConfigOnline.Password = DBcon.DBcontring.redisBeforePassword;
            var redisOnlineClientConfig = redisClientConfigOnline.GetRedisClient();
            return redisOnlineClientConfig;
        }
    }
    /// <summary>
    /// 后台报表redis操作类
    /// </summary>
    public static RedisClient redisClient
    {
        get
        {
            RedisClientConfig redisClientConfig = new RedisClientConfig();
            redisClientConfig.Password = DBcon.DBcontring.redisPassword;
            var _redisClient = redisClientConfig.GetRedisClient(DBcon.DBcontring.redisServerIP, DBcon.DBcontring.redisPort);
            _redisClient.Db = 0;
            return _redisClient;
        }
    }
    /// <summary>
    /// 缓存－redis操作类
    /// </summary>
    public static RedisClient cacheRedisClient
    {
        get
        {
            RedisClientConfig redisClientConfig = new RedisClientConfig();
            string strServerIP = DBcon.DBcontring.redisServerIP;
            redisClientConfig.ServerIP = strServerIP;
            //redisClientConfigOnline.Password = DBcon.DBcontring.redisPassword;
            var _redisClient = redisClientConfig.GetRedisClient(DBcon.DBcontring.redisServerIP, DBcon.DBcontring.redisPort);
            _redisClient.Db = 4;
            return _redisClient;
        }
    }
    /// <summary>
    /// 缓存－redis操作类
    /// </summary>
    public static RedisClient cacheRedisClientKey
    {
        get
        {
            RedisClientConfig redisClientConfig = new RedisClientConfig();
            string strServerIP = DBcon.DBcontring.redisServerIP;
            redisClientConfig.ServerIP = strServerIP;
            //redisClientConfigOnline.Password = DBcon.DBcontring.redisPassword;
            var _redisClient = redisClientConfig.GetRedisClient(DBcon.DBcontring.redisServerIP, DBcon.DBcontring.redisPort);
            _redisClient.Db = 5;
            return _redisClient;
        }
    }
    /// <summary>
    /// 后台报表redis操作类
    /// </summary>
    public static RedisClient redisClientKey
    {
        get
        {
            RedisClientConfig redisClientConfigKey = new RedisClientConfig();
            string strServerIP = DBcon.DBcontring.redisServerIP;
            redisClientConfigKey.ServerIP = strServerIP;
            //redisClientConfigOnline.Password = DBcon.DBcontring.redisPassword;
            var _redisClientKey = redisClientConfigKey.GetRedisClient(DBcon.DBcontring.redisServerIP, DBcon.DBcontring.redisPort);
            _redisClientKey.Db = 1;
            return _redisClientKey;
        }
    }
}
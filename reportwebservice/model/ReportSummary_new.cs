﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// 报表总览实体类
/// </summary>
public class ReportSummary_new
{
    /// <summary>
    /// 存款总额
    /// </summary>
    public Decimal TotalInMoney { get; set; }
    /// <summary>
    /// 投注总额
    /// </summary>
    public Decimal Out_BettingAccount { get; set; }
    /// <summary>
    /// 当日盈利
    /// </summary>
    public Decimal NowProfit { get; set; }
    /// <summary>
    /// 快捷支付
    /// </summary>
    public Decimal In_FastPrepaid { get; set; }
    /// <summary>
    /// 中奖总额
    /// </summary>
    public Decimal Out_WinningAccount { get; set; }
    /// <summary>
    /// 当日盈率
    /// </summary>
    public Decimal NowProfitRate { get; set; }
    /// <summary>
    /// 网银汇款
    /// </summary>
    public Decimal In_BankPrepaid { get; set; }
    /// <summary>
    /// 月投注额
    /// </summary>
    public Decimal MonthOut_BettingAccount { get; set; }
    /// <summary>
    /// 本月盈利
    /// </summary>
    public Decimal MonthProfitLoss { get; set; }
    /// <summary>
    /// 支付宝
    /// </summary>
    public Decimal In_AlipayPrepaid { get; set; }
    /// <summary>
    /// 月中奖额
    /// </summary>
    public Decimal MonthOut_WinningAccount { get; set; }
    /// <summary>
    /// 本月盈率
    /// </summary>
    public Decimal MonthProfitRate { get; set; }
    /// <summary>
    /// 微信支付
    /// </summary>
    public Decimal In_WeChatPrepaid { get; set; }
    /// <summary>
    /// 投注单量
    /// </summary>
    public Decimal Out_BettingAccount_Num { get; set; }
    /// <summary>
    /// 上月盈利
    /// </summary>
    public Decimal LastMonthProfitLoss { get; set; }
    /// <summary>
    /// 人工存款
    /// </summary>
    public Decimal In_ArtificialDeposit { get; set; }
    /// <summary>
    /// 撤单总额
    /// </summary>
    public Decimal Out_CancelAccount { get; set; }
    /// <summary>
    /// 上月投注
    /// </summary>
    public Decimal LastMonthOut_BettingAccount { get; set; }
    /// <summary>
    /// 上月中奖额
    /// </summary>
    public Decimal LastMonthOut_WinningAccount { get; set; }
    /// <summary>
    /// 上月盈率
    /// </summary>
    public Decimal LastMonthProfitRate { get; set; }
    /// <summary>
    /// 取款总额
    /// </summary>
    public Decimal Out_Account { get; set; }
    /// <summary>
    /// 会员余额
    /// </summary>
    public Decimal TotlaBalance { get; set; }
    /// <summary>
    /// 入款笔数
    /// </summary>
    public Decimal InMoneyNum { get; set; }
    /// <summary>
    /// 人工提出＝误存提出+行政提出
    /// </summary>
    public Decimal TotalArtificialOut { get; set; }
    /// <summary>
    /// 转账总额
    /// </summary>
    public Decimal TotalTransferAccount { get; set; }
    /// <summary>
    /// 出款笔数
    /// </summary>
    public Decimal Out_Account_Num { get; set; }
    /// <summary>
    /// 误存提出
    /// </summary>
    public Decimal Out_MistakeAccount { get; set; }
    /// <summary>
    /// 拒绝总额
    /// </summary>
    public Decimal Out_RefuseAccount { get; set; }
    /// <summary>
    /// 入款时间
    /// </summary>
    public Decimal InMoneyTime { get; set; }
    /// <summary>
    /// 行政提出
    /// </summary>
    public Decimal Out_AdministrationAccount { get; set; }
    /// <summary>
    /// 代理总额
    /// </summary>
    public Decimal TotalAgent { get; set; }
    /// <summary>
    /// 出款时间
    /// </summary>
    public Decimal OutMoneyTime { get; set; }
    /// <summary>
    /// 返点总额
    /// </summary>
    public Decimal Out_RebateAccount { get; set; }
    /// <summary>
    /// 代理工资
    /// </summary>
    public Decimal Wages { get; set; }
    /// <summary>
    /// 新增会员
    /// </summary>
    public Decimal NewMemberNum { get; set; }
    /// <summary>
    /// 优惠总额
    /// </summary>
    public Decimal TotalDiscount { get; set; }
    /// <summary>
    /// 代理分红
    /// </summary>
    public Decimal Bonus { get; set; }
    /// <summary>
    /// 首充人数
    /// </summary>
    public Decimal NewPrepaidNum { get; set; }
    /// <summary>
    /// 活动礼金
    /// </summary>
    public Decimal Out_ActivityDiscountAccount { get; set; }
    /// <summary>
    /// 在线人数
    /// </summary>
    public Decimal OnLinessNum { get; set; }
    /// <summary>
    /// 其他优惠
    /// </summary>
    public Decimal Out_OtherDiscountAccount { get; set; }
    /// <summary>
    /// 快捷充值次数
    /// </summary>
    public Decimal In_FastPrepaid_Num { get; set; }
    /// <summary>
    /// 银行转账次数
    /// </summary>
    public Decimal In_BankPrepaid_Num { get; set; }
    /// <summary>
    /// 支付宝转账次数
    /// </summary>
    public Decimal In_AlipayPrepaid_Num { get; set; }
    /// <summary>
    /// 微信支付次数
    /// </summary>
    public Decimal In_WeChatPrepaid_Num { get; set; }
    /// <summary>
    /// 人工转账次数
    /// </summary>
    public Decimal In_ArtificialDeposit_Num { get; set; }
    /// <summary>
    /// 本月返点
    /// </summary>
    public Decimal MonthOut_RebateAccount { get; set; }
    /// <summary>
    /// 本月活动优惠
    /// </summary>
    public Decimal MonthOut_ActivityDiscountAccount { get; set; }
    /// <summary>
    /// 本月其他优惠
    /// </summary>
    public Decimal MonthOut_OtherDiscountAccount { get; set; }
    /// <summary>
    /// 本月代理分红
    /// </summary>
    public Decimal MonthBonus { get; set; }
    /// <summary>
    /// 本月代理工资
    /// </summary>
    public Decimal MonthWages { get; set; }
    /// <summary>
    /// 本月拒绝出款
    /// </summary>
    public Decimal MonthOut_RefuseAccount { get; set; }
    /// <summary>
    /// 本月行政提出
    /// </summary>
    public Decimal MonthOut_AdministrationAccount { get; set; }
    /// <summary>
    /// 当日损益
    /// </summary>
    public Decimal CurrentProfitAndLoss { get; set; }
    /// <summary>
    /// 本月损益
    /// </summary>
    public Decimal MonthProfitAndLoss { get; set; }
    /// <summary>
    /// 上月损益
    /// </summary>
    public Decimal LastMonthProfitAndLoss { get; set; }
    /// <summary>
    /// 抽成
    /// </summary>
    public Decimal TakePercentage { get; set; }
    /// <summary>
    /// 上月返点金额
    /// </summary>
    public Decimal LastMonthOut_RebateAccount { get; set; }
    /// <summary>
    /// 上月活动金额
    /// </summary>
    public Decimal LastMonthOut_ActivityDiscountAccount { get; set; }
    /// <summary>
    /// 上月其他活动金额
    /// </summary>
    public Decimal LastMonthOut_OtherDiscountAccount { get; set; }
    /// <summary>
    /// 上月分红
    /// </summary>
    public Decimal LastMonthBonus { get; set; }
    /// <summary>
    /// 上月工资
    /// </summary>
    public Decimal LastMonthWages { get; set; }
    /// <summary>
    /// 上月拒绝金额
    /// </summary>
    public Decimal LastMonthOut_RefuseAccount { get; set; }
    /// <summary>
    /// 上月行政提出
    /// </summary>
    public Decimal LastMonthOut_AdministrationAccount { get; set; }
    /// <summary>
    /// 构造函数
    /// </summary>
    public ReportSummary_new()
    {
        Decimal strDisplayValue = 0;
        //上月盈利
        this.LastMonthProfitLoss = strDisplayValue;
        //月投注额
        this.MonthOut_BettingAccount = strDisplayValue;
        //本月盈利
        this.MonthProfitLoss = strDisplayValue;
        //月中奖额
        this.MonthOut_WinningAccount = strDisplayValue;
        //代理总额
        this.TotalAgent = strDisplayValue;
        //优惠总额
        this.TotalDiscount = strDisplayValue;
    }
}
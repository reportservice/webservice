﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///代理报表
/// </summary>
public class RankList
{
    /// <summary>
    /// 会员ID
    /// </summary>
    public int UserId
    {
        get;
        set;
    }
    /// <summary>
    /// 会员账号
    /// </summary>
    public string UserName
    {
        get;
        set;
    }
    /// <summary>
    /// 会员昵称
    /// </summary>
    public string NickName
    {
        get;
        set;
    }
    /// <summary>
    /// 会员头像
    /// </summary>
    public string UserPhoto
    {
        get;
        set;
    }
    /// <summary>
    /// 奖金
    /// </summary>
    public string Bonus
    {
        get;
        set;
    }
    /// <summary>
    /// 排名
    /// </summary>
    public string Ranking
    {
        get;
        set;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///代理报表
/// </summary>
public class dt_lottery_sum
{
    public decimal tzmony { get; set; }//投注金额
    public int tznumber { get; set; } //投注人数
    public int tzcount { get; set; } //投注笔数
}
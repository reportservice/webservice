﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///代理报表
/// </summary>
public class dt_user_in_verify
{
    public string identityid { get; set; }//站
    public int user_id { get; set; }//使用者
    public decimal money { get; set; }//金額
    public int type { get; set; } //總類
    public int type2 { get; set; }//總類2
    public int state { get; set; } //狀態
   
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

/// <summary>
/// 出入款平均时间实体类
/// </summary>
class WithdrawTimeEntity
{
    /// <summary>
    /// 商户ID
    /// </summary>
    [Description("identityid")]
    public string IdentityId { get; set; }
    /// <summary>
    /// 提现时间
    /// </summary>
    [Description("WithdrawTime")]
    public int WithdrawTime { get; set; }
    /// <summary>
    /// 充值时间
    /// </summary>
    [Description("RechargeTime")]
    public int RechargeTime { get; set; }
}
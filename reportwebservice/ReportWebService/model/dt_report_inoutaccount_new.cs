﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/**************************************************************************

作者: zengzh

日期:2016-08-26

说明:会员出入款统计表

**************************************************************************/
namespace EasyFrame.NewCommon.Model
{
    /// <summary>
    /// 会员出入款统计表
    /// </summary>
    public class dt_report_inoutaccount_new
    {

        public virtual double OutMoneyTime { get; set; }
        public virtual double InMoneyNum { get; set; }
        public virtual double InMoneyTime { get; set; }
        public virtual decimal Wages { get; set; }
        public virtual decimal Bonus { get; set; }
        public virtual decimal WinRate { get; set; }
        public virtual int user_id { get; set; }
        public virtual string SourceName { get; set; }
        public virtual decimal ProfitLoss { get; set; }
        public virtual decimal Out_WinningAccount { get; set; }
        public virtual decimal Out_RefuseAccount { get; set; }
        public virtual decimal Out_RebateAccount { get; set; }
        public virtual decimal Out_OtherDiscountAccount { get; set; }
        public virtual decimal Out_MistakeAccount { get; set; }
        public virtual decimal Out_CancelAccount { get; set; }
        public virtual decimal Out_BettingAccount { get; set; }
        public virtual decimal Out_AdministrationAccount { get; set; }
        public virtual decimal Out_ActivityDiscountAccount { get; set; }
        public virtual decimal RewardMoney { get; set; }
        public virtual decimal Out_Account { get; set; }
        public virtual decimal In_WeChatPrepaid { get; set; }
        public virtual decimal In_FastPrepaid { get; set; }
        public virtual decimal In_BankPrepaid { get; set; }
        public virtual decimal In_QQ { get; set; }
        public virtual decimal In_FourthPrepaid { get; set; }
        public virtual decimal In_UnionpayPrepaid { get; set; }
        public virtual decimal In_MoneyPrepaid { get; set; }
        public virtual decimal In_ArtificialDeposit { get; set; }
        public virtual decimal In_AlipayPrepaid { get; set; }
        public virtual string identityid { get; set; }
        public virtual int id { get; set; }
        public virtual string group_name { get; set; }
        public virtual int agent_id { get; set; }
        public virtual DateTime AddTime { get; set; }
        public virtual double OutMoneyNum { get; set; }
        public DateTime UpdateTime { get; set; }
    }
}

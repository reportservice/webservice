﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///代理报表
/// </summary>
public class ServiceTing
{
    /// <summary>
    /// 提现时间
    /// </summary>
    public string WithdrawTime
    {
        get;
        set;
    }
    /// <summary>
    /// 充值时间
    /// </summary>
    public string RechargeTime
    {
        get;
        set;
    }
}
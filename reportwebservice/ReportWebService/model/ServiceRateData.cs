﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///代理报表
/// </summary>
public class ServiceRateData
{
    /// <summary>
    /// 商户ID
    /// </summary>
    public string IdentityId { get; set; }
    /// <summary>
    /// 提现时间
    /// </summary>
    public int WithdrawTime { get; set; }
    /// <summary>
    /// 充值时间
    /// </summary>
    public int RechargeTime { get; set; }
}
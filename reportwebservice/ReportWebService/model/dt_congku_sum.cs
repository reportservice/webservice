﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///註冊手充會員餘額
/// </summary>
public class dt_congku_sum
{
    public int zcnumber { get; set; }//注册人数
    public int scnumber { get; set; } //首充人数
    public decimal userMoney { get; set; }//会员余额
    public decimal InTime { get; set; }//充值操作时间
    public decimal OutTime { get; set; }//体现操作时间
}
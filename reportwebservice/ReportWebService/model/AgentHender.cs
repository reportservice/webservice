﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// 代理报表xt
/// </summary>
public class AgentHender
{
    /// <summary>
    /// 投注额
    /// </summary>
    public string BetMoney
    {
        get;
        set;
    }
    /// <summary>
    /// 奖金
    /// </summary>
    public string Bonus
    {
        get;
        set;
    }
    /// <summary>
    /// 返点金额
    /// </summary>
    public string RebateMoney
    {
        get;
        set;
    }
    /// <summary>
    /// 活动金额
    /// </summary>
    public string ActivityMoney
    {
        get;
        set;
    }
    /// <summary>
    /// 充值金额
    /// </summary>
    public string RechargeMoney
    {
        get;
        set;
    }
    /// <summary>
    /// 提现金额
    /// </summary>
    public string WithdrawMoney
    {
        get;
        set;
    }
    /// <summary>
    /// 投注人数
    /// </summary>
    public string BetNum
    {
        get;
        set;
    }
    /// <summary>
    /// 首充人数
    /// </summary>
    public string FirstChargeNum
    {
        get;
        set;
    }
    /// <summary>
    /// 注册人数
    /// </summary>
    public string RegisterNum
    {
        get;
        set;
    }
    /// <summary>
    /// 团队人数
    /// </summary>
    public string TeamNum
    {
        get;
        set;
    }
    /// <summary>
    /// 团队余额
    /// </summary>
    public string TeamBalance
    {
        get;
        set;
    }
    /// <summary>
    /// 盈利金额
    /// </summary>
    public string ProfitMoney
    {
        get;
        set;
    }
    /// <summary>
    /// 代理返点
    /// </summary>
    public string AgentRebate
    {
        get;
        set;
    }
    /// <summary>
    /// 代理工资
    /// </summary>
    public string AgentWages
    {
        get;
        set;
    }
    /// <summary>
    /// 代理工资
    /// </summary>
    public string AgentDividends
    {
        get;
        set;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.Redis;


    public class rediscon
    {
        /// <summary>
        /// 服务器Ip地址
        /// </summary>
        private string _ServerIP = "";
        /// <summary>
        /// ServiceStack注册码
        /// </summary>
        public static string ServiceStackLicenseKey = @"4228-e1JlZjo0MjI4LE5hbWU6ZGFmYSxUeXBlOkJ1c2luZXNzLEhhc2g6YkJKUXFlL1BJdmt5dUU4dTJXbVhmc3ZpTXQwU1I1V2U0bnBmSHVHVjN5UXBpMFZoSkJyQUg3OU5HTG5rU2t0NGJHUFd6RVhHaFI4QVpZMG9GVm9lV29ia1ZIRkdncGs4WlJhekZyajU0K0wrditUSnRFQkZHQ0JBWWJzMGFBSlViWkZnOXJjMmZGLzE4eUxRcWN1eFZRZ2Y4QnBVUXNEVjA5NC9RYVJOT0hZPSxFeHBpcnk6MjAxNy0wOS0zMH0 =";
        /// <summary>
        /// 构造函数
        /// </summary>

        public string ServerIP
        {
            get
            {
                return _ServerIP;
            }
            set
            {
                this._ServerIP = value;
            }
        }
        /// <summary>
        /// 服务器端口
        /// </summary>
        private int _ServerPort = 6379;
        /// <summary>
        /// 服务器端口
        /// </summary>
        public int ServerPort
        {
            get
            {
                return _ServerPort;
            }
            set
            {
                this._ServerPort = value;
            }
        }
        /// <summary>
        /// 密码
        /// </summary>
        private string _Password = "";
        /// <summary>
        /// 密码
        /// </summary>
        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                this._Password = value;
            }
        }
        /// <summary>
        /// 获取Radis客户端
        /// </summary>
        /// <returns></returns>
        public RedisClient GetRedisClient()
        {
            return new RedisClient(ServerIP, ServerPort, Password);
        }
        /// <summary>
        /// 获取Radis客户端
        /// </summary>
        /// <returns></returns>
        public RedisClient GetRedisClient(string strServerIP, int iServerPort)
        {
            return new RedisClient(strServerIP, iServerPort);
        }

    }

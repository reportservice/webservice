﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class RedisSession
    {
        private HttpContext context;

        /// <summary>
        /// RedisSession
        /// </summary>
        /// <param name="context"></param>
        /// <param name="isReadOnly">是否只读</param>
        /// <param name="sourceName">设备类型</param>
        /// <param name="timeout">单位：分钟</param>
        public RedisSession(bool isReadOnly, string sourceName, int timeout = 0)
        {
            //this.context = context;
            this.IsReadOnly = isReadOnly;
            if (!sourceName.IsNullOrEmpty() && sourceName.Contains("APP"))
            {
                if (timeout == 0)
                {
                    timeout = 720;
                }
            }
            else
            {
                if (timeout == 0)
                {
                    timeout = 720;
                }
            }
            this.Timeout = timeout;
            this.SourceName = sourceName;
        
            //更新缓存过期时间
            //RedisBase.Hash_SetExpire(SessionId, DateTime.Now.AddMinutes(Timeout));
        }
        
        public const string SessionName = "C_SessionId";

        public const string IVKeyName = "IVK";

        public string SessionId
        {
            get
            {
                string sessionKey = SessionKey;
                string ivk = IVKey;

                return "Session_" + sessionKey;
            }
        }
        public string SessionKey
        {
            get
            {
                string sessionKey = string.Empty;
                HttpCookie sessionCookie = context.Request.Cookies.Get(SessionName);
                if (sessionCookie != null)
                {
                    sessionKey = sessionCookie.Value;
                }
                if (string.IsNullOrEmpty(sessionKey))
                {
                    sessionKey = Guid.NewGuid().ToString();
                    sessionCookie = new HttpCookie(SessionName);
                    sessionCookie.Value = sessionKey;
                    sessionCookie.HttpOnly = IsReadOnly;
                    context.Response.Cookies.Add(sessionCookie);
                }
                return sessionKey;
            }
            set
            {
                HttpCookie cookie = new HttpCookie(SessionName);
                //cookie.Value = Guid.NewGuid().ToString();
                cookie.Value = value;
                cookie.HttpOnly = IsReadOnly;
                context.Response.Cookies.Add(cookie);
            }
        }

        public string IVKey
        {
            get
            {
                string ivk = string.Empty;
                HttpCookie ivKeyCookie = context.Request.Cookies.Get(IVKeyName);
                if (ivKeyCookie != null)
                {
                    ivk = ivKeyCookie.Value;
                }
                if (string.IsNullOrEmpty(ivk))
                {
                    ivk = "";//Common.Utils.GenIVKey(SessionKey);
                    ivKeyCookie = new HttpCookie(IVKeyName);
                    ivKeyCookie.Value = ivk;
                    ivKeyCookie.HttpOnly = false;
                    context.Response.Cookies.Add(ivKeyCookie);
                }
                return ivk;
            }
            set
            {
                HttpCookie cookie = new HttpCookie(IVKeyName);
                //cookie.Value = Common.Utils.GenIVKey(SessionKey);
                cookie.Value = value;
                cookie.HttpOnly = false;
                context.Response.Cookies.Add(cookie);
            }
        }

        //public int Count
        //{
        //    get
        //    {
        //        return RedisBase.Hash_GetCount(SessionId);
        //    }
        //}
        
        public bool IsReadOnly { get; set; }

        /// <summary>
        /// Device Type, ex: PC / MB / APP etc...
        /// </summary>
        public string SourceName { get; set; }
        
        public int Timeout { get; set; }
        
        public object this[string name]
        {
            get
            {
                return RedisBase.Hash_GetObj<object>(SessionId, name);
            }
            set
            {
                RedisBase.Hash_Set<object>(SessionId, name, value, DateTime.Now.AddMinutes(Timeout));
            }
        }
        
        public bool IsExistKey(string name)
        {
            return RedisBase.Hash_Exist<object>(name);
        }
        
        public void Add(string name, object value)
        {
            RedisBase.Hash_Set<object>(SessionId, name, value, DateTime.Now.AddMinutes(Timeout));
        }

        public void Clear()
        {
            RedisBase.Hash_Remove(SessionId);

            string guid = Guid.NewGuid().ToString();
            SessionKey = guid;
           // IVKey = Common.Utils.GenIVKey(guid);
        }

        public void Remove(string name)
        {
            RedisBase.Hash_Remove(SessionId, name);
        }
        public void RemoveSessuionKey(string SessionId)
        {
            RedisBase.Hash_Remove(SessionId);
        }
    }

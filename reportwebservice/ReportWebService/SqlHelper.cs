﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace EasyFrame.Web
{
    public class SqlHelper
    {
        public static string localconnnStr = "Data Source=xianggang.database.windows.net;uid=dafasys;pwd=dafayun@168.com;database=dafacloud;Connect Timeout=120000;";
       
        /// <summary>
        /// 用来查詢數據
        /// </summary>
        /// <param name="strsql"></param>
        /// <returns></returns>
        public static DataTable GetQueryFromLocalData(string strsql, SqlParameter[] cmdParms)
        {
            using (SqlConnection localconn = new SqlConnection(localconnnStr))
            {
                DataSet ds1 = new DataSet();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    PrepareCommand(cmd, localconn, null, strsql, cmdParms);
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds1);
                        cmd.Parameters.Clear();
                    }

                }
                catch
                {

                }
                finally
                {
                    if (localconn.State == ConnectionState.Open)
                    {
                        localconn.Dispose();
                        localconn.Close();
                    }
                }
                if (ds1.Tables.Count > 0)
                {
                    return ds1.Tables[0];
                }
                else
                {
                    return null;
                }

            }

        }

        /// <summary>
        /// 用来執行命令
        /// </summary>
        /// <param name="strsql"></param>
        /// <returns></returns>
        public static int QueryFromLocal(string strsql, SqlParameter[] cmdParms)
        {
            int row = 0;
            using (SqlConnection localconn = new SqlConnection(localconnnStr))
            {
                DataSet ds1 = new DataSet();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    PrepareCommand(cmd, localconn, null, strsql, cmdParms);
                    row =  cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();


                }
                catch
                {

                }
                finally
                {
                    if (localconn.State == ConnectionState.Open)
                    {
                        localconn.Dispose();
                        localconn.Close();
                    }
                }
               

            }
            return row;

        }

        public static int RunInsert(string record_code, string user_id, string identityid,string ProName)
        {
            SqlConnection conn = new SqlConnection(localconnnStr);
            int resulte = 0;
            try
            {
                //dt.Columns.Remove("lockid");
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = ProName;
                cmd.CommandTimeout = 500000;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@record_code", SqlDbType.VarChar);
                cmd.Parameters.Add("@user_id", SqlDbType.Int);
                cmd.Parameters.Add("@ruczname", SqlDbType.VarChar);
                cmd.Parameters.Add("@czname", SqlDbType.VarChar);
                cmd.Parameters.Add("@cztime", SqlDbType.DateTime);
                cmd.Parameters.Add("@result", SqlDbType.Int);
                cmd.Parameters.Add("@identityid", SqlDbType.VarChar);
                cmd.Parameters[0].Value = record_code;
                cmd.Parameters[1].Value = user_id;
                cmd.Parameters[2].Value = "0";
                cmd.Parameters[3].Value = "0";
                cmd.Parameters[4].Value = "2016-01-01";
                cmd.Parameters[5].Value = resulte;
                cmd.Parameters[6].Value = identityid;
                cmd.Parameters["@result"].Direction = ParameterDirection.ReturnValue;
                cmd.ExecuteNonQuery();
                resulte = (int)cmd.Parameters["@result"].Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Dispose();
                    conn.Close();
                }
            }
            return resulte;
        }

        private static void PrepareCommand(SqlCommand cmd, SqlConnection conn, SqlTransaction trans, string cmdText, SqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;//cmdType;
            if (cmdParms != null)
            {
                foreach (SqlParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
        }

    }
}
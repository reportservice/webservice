﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using EasyFrame.NewCommon.Model;
using System.Web.Services.Protocols;
using System.Data;
using EasyFrame.Redis;
using EasyFrame.NewCommon;
using System.Linq.Expressions;
using EasyFrame.NewCommon.Common;
using System.Reflection;
using System.Text;
using System.Data.SqlClient;
using System.Diagnostics;



/**************************************************************************

作者: zengzh

日期:2016-10-26

说明:后台报表接口

**************************************************************************/
/// <summary>
///AfterReportWebService 的摘要说明
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
//若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消对下行的注释。 
// [System.Web.Script.Services.ScriptService]
public class AfterReportWebService : System.Web.Services.WebService
{
    /// <summary>
    /// Redis操作帮助类
    /// </summary>
    private RedisHelper redisHelper = new RedisHelper();
    public AfterReportWebService()
    {

        //如果使用设计的组件，请取消注释以下行 
        //InitializeComponent(); 
    }
    public EncryptionSoapHeader encryptionSoapHeader = new EncryptionSoapHeader();
    /// <summary>
    /// 验证权限
    /// </summary>
    /// <returns></returns>
    private string ValidateAuthority()
    {
        string strMsg = string.Empty;
        encryptionSoapHeader.IsValid(out strMsg);
        string P = encryptionSoapHeader.Password;
        string U = encryptionSoapHeader.UserName;

        if (!encryptionSoapHeader.IsValid(out strMsg))
        {
            strMsg = "strMsg: " + strMsg + " Password: " + P + " UserName: " + U;
            return strMsg;
        }
        strMsg = "strMsg: " + strMsg + " Password: " + P + " UserName: " + U;

        return strMsg;
    }
    /// <summary>
    /// 资金异动入款表获取条件
    /// </summary>
    /// <param name="redisInAccount">redis_in_account表</param>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strType">类型</param>
    /// <param name="strRecordCode">流水号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="strUserId">会员ID</param>
    /// <returns></returns>
    private bool GetInAccountCondition(redis_in_account redisInAccount, string strIdentityId, string strType, string strRecordCode, string strStartDate, string strEndDate, string strUserId)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= redisInAccount.identityid == strIdentityId;
        //状态
        boolResult &= redisInAccount.state == 1;
        //类型
        if (!string.IsNullOrWhiteSpace(strType) && strType != "-1")
        {
            if (strType == "3")
            {
                boolResult &= false;
            }
            else
            {
                boolResult &= redisInAccount.type == int.Parse(strType);
            }
        }
        //会员Id
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            boolResult &= redisInAccount.user_id == int.Parse(strUserId);
        }
        //流水号
        if (!string.IsNullOrWhiteSpace(strRecordCode))
        {
            boolResult &= redisInAccount.record_code == strRecordCode;
        }
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= redisInAccount.add_time >= DateTime.Parse(strStartDate) && redisInAccount.add_time <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 资金异动出款表获取条件
    /// </summary>
    /// <param name="redisOutAccount">redis_out_account表</param>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strType">类型</param>
    /// <param name="strRecordCode">流水号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="strUserId">会员ID</param>
    /// <returns></returns>
    private bool GetOutAccountCondition(redis_out_account redisOutAccount, string strIdentityId, string strType, string strRecordCode, string strStartDate, string strEndDate, string strUserId)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= redisOutAccount.identityid == strIdentityId;
        //状态
        boolResult &= redisOutAccount.state == 1;
        boolResult |= redisOutAccount.type == 3;
        //类型
        if (!string.IsNullOrWhiteSpace(strType) && strType != "-1")
        {
            boolResult &= redisOutAccount.type == int.Parse(strType);
        }
        //会员Id
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            boolResult &= redisOutAccount.user_id == int.Parse(strUserId);
        }
        //流水号
        if (!string.IsNullOrWhiteSpace(strRecordCode))
        {
            boolResult &= redisOutAccount.record_code == strRecordCode;
        }
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= redisOutAccount.add_time >= DateTime.Parse(strStartDate) && redisOutAccount.add_time <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 获取交易记录查询条件
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strType">类型</param>
    /// <param name="strRecordCode">流水号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="strUserId">会员ID</param>
    /// <returns></returns>
    private string GetTransactionWhereSql(string strIdentityId, string strType, string strRecordCode, string strStartDate, string strEndDate, string strUserId)
    {
        string strResult = string.Empty;
        if (!string.IsNullOrWhiteSpace(strIdentityId))
        {
            strResult += " and identityid='" + strIdentityId + "'";
        }
        if (!string.IsNullOrWhiteSpace(strType) && strType != "-1")
        {
            strResult += " and type=" + strType;
        }
        if (!string.IsNullOrWhiteSpace(strRecordCode))
        {
            strResult += " and record_code='" + strRecordCode + "'";
        }
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            strResult += " and add_time>='" + DateTime.Parse(strStartDate).ToString("s") + "' and add_time<='" + DateTime.Parse(strEndDate).ToString("s") + "'";
        }
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            strResult += " and user_id=" + strUserId;
        }
        return strResult;
    }
    /// <summary>
    /// 资金异动入账表的实体类转换成DataTable字典
    /// </summary>
    /// <returns></returns>
    private Dictionary<string, string> GetInAccountPropertyName()
    {
        Dictionary<string, string> dicPropertyName = new Dictionary<string, string>();
        dicPropertyName.Add("InAccountId", "id");
        dicPropertyName.Add("user_id", "user_id");
        dicPropertyName.Add("User_name", "User_name");
        dicPropertyName.Add("record_code", "record_code");
        dicPropertyName.Add("add_time2", "add_time");
        dicPropertyName.Add("type", "type");
        dicPropertyName.Add("typeName", "typeName");
        dicPropertyName.Add("type2", "type2");
        dicPropertyName.Add("money", "in_money");
        dicPropertyName.Add("out_money", "out_money");
        dicPropertyName.Add("state", "state");
        dicPropertyName.Add("after_money", "after_money");
        dicPropertyName.Add("commt", "commt");
        dicPropertyName.Add("identityid", "identityid");
        dicPropertyName.Add("SourceName", "SourceName");
        return dicPropertyName;
    }
    /// <summary>
    /// 资金异动出账表的实体类转换成DataTable字典
    /// </summary>
    /// <returns></returns>
    private Dictionary<string, string> GetOutAccountPropertyName()
    {
        Dictionary<string, string> dicPropertyName = new Dictionary<string, string>();
        dicPropertyName.Add("OutAccountId", "id");
        dicPropertyName.Add("user_id", "user_id");
        dicPropertyName.Add("User_name", "User_name");
        dicPropertyName.Add("record_code", "record_code");
        dicPropertyName.Add("add_time2", "add_time");
        dicPropertyName.Add("type", "type");
        dicPropertyName.Add("typeName", "typeName");
        dicPropertyName.Add("type2", "type2");
        dicPropertyName.Add("in_money", "in_money");
        dicPropertyName.Add("money", "out_money");
        dicPropertyName.Add("state", "state");
        dicPropertyName.Add("after_money", "after_money");
        dicPropertyName.Add("commt", "commt");
        dicPropertyName.Add("identityid", "identityid");
        dicPropertyName.Add("SourceName", "SourceName");
        return dicPropertyName;
    }
    /// <summary>
    /// 合并资金异动入账表
    /// </summary>
    /// <param name="inAccountDataTable">sqlite资金异动入账表</param>
    /// <param name="redisInAccountDataTable">redis资金异动入账表</param>
    /// <returns></returns>
    private DataTable MergeInAccountDataTable(DataTable inAccountDataTable, DataTable redisInAccountDataTable)
    {
        EasyFrame.NewCommon.Common.DataTableHelper dataTableHelper = new EasyFrame.NewCommon.Common.DataTableHelper();
        if (redisInAccountDataTable.Rows.Count > 0)
        {
            inAccountDataTable.Merge(redisInAccountDataTable.Copy());
        }
        if (inAccountDataTable.Rows.Count > 0)
        {
            inAccountDataTable = inAccountDataTable.Select("", "id desc").CopyToDataTable();
        }
        return inAccountDataTable;
    }
    /// <summary>
    /// 投注记录获取条件
    /// </summary>
    /// <param name="redisLotteryBettingInfo">redis_lottery_betting_info实体类</param>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strLotteryCode">彩种编码</param>
    /// <param name="strIssuseNo">投注期号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="strUserId">会员ID</param>
    /// <param name="strBettingState">投注状态</param>
    /// <returns></returns>
    private bool GetLotteryBettingInfoCondition(redis_lottery_betting_info redisLotteryBettingInfo, string strIdentityId, string strLotteryCode, string strIssuseNo,
                                                string strStartDate, string strEndDate, string strUserId, string strBettingState)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= redisLotteryBettingInfo.identityid == strIdentityId;
        //彩种
        if (!string.IsNullOrWhiteSpace(strLotteryCode) && strLotteryCode != "0000")
        {
            boolResult &= redisLotteryBettingInfo.lottery_code == strLotteryCode;
        }
        //投注期号
        if (!string.IsNullOrWhiteSpace(strIssuseNo))
        {
            boolResult &= redisLotteryBettingInfo.betting_issuseNo == strIssuseNo;
        }
        //会员ID
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            boolResult &= redisLotteryBettingInfo.user_id == int.Parse(strUserId);
        }
        //投注状态
        if (!string.IsNullOrWhiteSpace(strBettingState) && strBettingState != "0")
        {
            boolResult &= redisLotteryBettingInfo.betting_state == int.Parse(strBettingState) - 1;
        }
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= redisLotteryBettingInfo.add_time >= DateTime.Parse(strStartDate) && redisLotteryBettingInfo.add_time <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 获取投注记录查询条件
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strLotteryCode">彩种编码</param>
    /// <param name="strIssuseNo">期号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="strUserId">会员ID</param>
    /// <param name="strBettingState">投注状态</param>
    /// <returns></returns>
    private string GetBetRecordWhereSql(string strIdentityId, string strLotteryCode, string strIssuseNo, string strStartDate, string strEndDate, string strUserId, string strBettingState)
    {
        string strResult = string.Empty;
        if (!string.IsNullOrWhiteSpace(strIdentityId))
        {
            strResult += " and identityid='" + strIdentityId + "'";
        }
        if (!string.IsNullOrWhiteSpace(strLotteryCode) && strLotteryCode != "0000")
        {
            strResult += " and lottery_code='" + strLotteryCode + "'";
        }
        if (!string.IsNullOrWhiteSpace(strIssuseNo))
        {
            strResult += " and betting_issuseNo='" + strIssuseNo + "'";
        }
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            strResult += " and user_id=" + strUserId;
        }
        if (!string.IsNullOrWhiteSpace(strBettingState) && strBettingState != "0")
        {
            int iBettingState = int.Parse(strBettingState) - 1;
            strResult += " and betting_state=" + iBettingState;
        }
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            strResult += " and add_time>='" + DateTime.Parse(strStartDate).ToString("s") + "' and add_time<='" + DateTime.Parse(strEndDate).ToString("s") + "'";
        }
        return strResult;
    }

    /// <summary>
    /// 投注记录的实体类转换成DataTable字典
    /// </summary>
    /// <returns></returns>
    private Dictionary<string, string> GetBetRecordPropertyName()
    {
        Dictionary<string, string> dicPropertyName = new Dictionary<string, string>();
        dicPropertyName.Add("LotteryBettingInfoId", "Id");
        dicPropertyName.Add("identityid", "IdentityId");
        dicPropertyName.Add("user_id", "UserId");
        dicPropertyName.Add("user_name", "UserName");
        dicPropertyName.Add("lottery_code", "LotteryName");
        dicPropertyName.Add("play_detail_code", "PlayName");
        dicPropertyName.Add("betting_issuseNo", "Issue");
        dicPropertyName.Add("betting_number", "BetContent");
        dicPropertyName.Add("open_num", "OpenNum");
        dicPropertyName.Add("betting_money", "BetMoney");
        dicPropertyName.Add("betting_count", "BetCount");
        dicPropertyName.Add("graduation_count", "Multiple");
        dicPropertyName.Add("betting_state", "OpenState");
        dicPropertyName.Add("betting_type", "PlayType");
        dicPropertyName.Add("add_time", "AddTime");
        dicPropertyName.Add("SourceName", "SourceName");
        return dicPropertyName;
    }
    /// <summary>
    /// 投注记录获取条件
    /// </summary>
    /// <param name="redisLotteryBettingInfo">redis_lottery_betting_info实体类</param>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strLotteryCode">彩种编码</param>
    /// <param name="strIssuseNo">投注期号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="strUserId">会员ID</param>
    /// <param name="strBettingState">投注状态</param>
    /// <returns></returns>
    private bool GetLotteryWinningRecordCondition(redis_lottery_winning_record redisLotteryWinningRecord, string strIdentityId, string strLotteryCode, string strIssuseNo,
                                                string strStartDate, string strEndDate, string strUserId, Dictionary<string, string> dicLotteryCode)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= redisLotteryWinningRecord.identityid == strIdentityId;
        //彩种编码
        boolResult &= dicLotteryCode.ContainsKey(redisLotteryWinningRecord.lottery_code);
        //彩种
        if (!string.IsNullOrWhiteSpace(strLotteryCode) && strLotteryCode != "0000")
        {
            boolResult &= redisLotteryWinningRecord.lottery_code == strLotteryCode;
        }
        //投注期号
        if (!string.IsNullOrWhiteSpace(strIssuseNo))
        {
            boolResult &= redisLotteryWinningRecord.issuse_no == strIssuseNo;
        }
        //会员ID
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            boolResult &= redisLotteryWinningRecord.user_id == int.Parse(strUserId);
        }
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= redisLotteryWinningRecord.add_time >= DateTime.Parse(strStartDate) && redisLotteryWinningRecord.add_time <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 投注记录获取条件
    /// </summary>
    /// <param name="redisLotteryOpen">redis_lottery_open实体类</param>
    /// <param name="strIssuseNo">投注期号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <returns></returns>
    private bool GetLotteryOpenCondition(redis_lottery_open redisLotteryOpen, string strStartDate, string strEndDate)
    {
        bool boolResult = true;
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= redisLotteryOpen.add_time >= DateTime.Parse(strStartDate) && redisLotteryOpen.add_time <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 在线人员获取条件
    /// </summary>
    /// <param name="reportNewPrepaid"></param>
    /// <param name="listUserId">会员Id的条件集合</param>
    /// <returns></returns>
    private bool GetReportIndexOnLineCondition(EasyFrame.NewCommon.Model.dt_sys_online sysOnline, string strIdentityId)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= sysOnline.identityid == strIdentityId;
        TimeSpan timeSpan = DateTime.Now - sysOnline.oper_time;
        boolResult &= timeSpan.TotalMinutes <= 5;
        return boolResult;
    }
    /// <summary>
    /// 获取特定字符串中的金额 格式：如0.00/0人
    /// </summary>
    /// <param name="strMoney">字符串</param>
    /// <returns></returns>
    public decimal GetMoney(object bjMoney)
    {
        if (bjMoney == null || string.IsNullOrWhiteSpace(bjMoney.ToString()))
            return 0;

        string strMoney = bjMoney.ToString();
        decimal fMoney = 0;
        int index = strMoney.IndexOf("/");
        string tempMoney = strMoney.Substring(0, index);
        fMoney = decimal.Parse(tempMoney);
        return fMoney;
    }
    /// <summary>
    /// 获取特定字符串中的人数 格式：如0.00/0人
    /// </summary>
    /// <param name="strNum">字符串</param>
    /// <returns></returns>
    public int GetNum(object bjNum)
    {
        if (bjNum == null || string.IsNullOrWhiteSpace(bjNum.ToString()))
            return 0;

        string strNum = bjNum.ToString();
        int iNum = 0;
        int index = strNum.IndexOf("/");
        string tempNum = strNum.Substring(index + 1);
        iNum = int.Parse(tempNum.Substring(0, tempNum.Length));
        return iNum;
    }



    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportIndexData(string strIdentityId, string strStartDate, string strEndDate)
    {
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "IdentityId=" + strIdentityId + " StartTime=" + strStartDate + " EndTime=" + strEndDate;
        try
        {
            st.Reset();
            st.Start();
            TimeSpan tse = new TimeSpan();
            TimeSpan tsein = new TimeSpan();
            TimeSpan tsein_s = new TimeSpan();
            TimeSpan tsein_e = new TimeSpan();
            TimeSpan tseout = new TimeSpan();
            TimeSpan tseout_s = new TimeSpan();
            TimeSpan tseout_e = new TimeSpan();
            TimeSpan tseorder = new TimeSpan();
            TimeSpan tseorder_s = new TimeSpan();
            TimeSpan tseorder_e = new TimeSpan();
            TimeSpan tselmonth = new TimeSpan();
            TimeSpan tselmonth_s = new TimeSpan();
            TimeSpan tselmonth_e = new TimeSpan();
            TimeSpan tseother = new TimeSpan();
            TimeSpan tseother_s = new TimeSpan();
            TimeSpan tseother_e = new TimeSpan();
            TimeSpan tsereg = new TimeSpan();
            TimeSpan tsereg_s = new TimeSpan();
            TimeSpan tsereg_e = new TimeSpan();
            TimeSpan tsenewpay = new TimeSpan();
            TimeSpan tsenewpay_s = new TimeSpan();
            TimeSpan tsenewpay_e = new TimeSpan();
            TimeSpan tsecapital = new TimeSpan();
            TimeSpan tsecapital_s = new TimeSpan();
            TimeSpan tsecapital_e = new TimeSpan();
            TimeSpan tse1 = new TimeSpan(DateTime.Now.Ticks);
            string strStartDate_s = Convert.ToDateTime(strStartDate).ToString("yyyy-MM-dd HH:mm:ss");
            string strEndDate_s = Convert.ToDateTime(strEndDate).ToString("yyyy-MM-dd HH:mm:ss");
            int searchType = 0;
            if (Convert.ToDateTime(strStartDate) >= DateTime.Now.AddDays(-1).Date && Convert.ToDateTime(strEndDate) >= DateTime.Now.AddDays(-1).Date)
            {
                searchType = 1;
            }
            else if (Convert.ToDateTime(strStartDate) < DateTime.Now.AddDays(-1).Date && Convert.ToDateTime(strEndDate) < DateTime.Now.AddDays(-1).Date)
            {
                searchType = 2;
            }
            else
            {
                searchType = 3;
            }
            TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
            List<ReportSummary> listReportSummary = new List<ReportSummary>();
            DataTableHelper dataTableHelper = new DataTableHelper();

            //从报表库拿数据
            string identityid = strIdentityId; //条件从查询条件获取
            string begindate = strStartDate_s;//条件从查询条件获取
            string enddate = strEndDate_s;//条件从查询条件获取

            //站長
            List<SqlParameter> sqlcmdlist_ali = new List<SqlParameter>();
            sqlcmdlist_ali.Add(new SqlParameter("@identityid", strIdentityId));
            SqlParameter[] sqlcmdpar_ali = sqlcmdlist_ali.ToArray();

            //線上
            List<SqlParameter> sqlcmdlist_online = new List<SqlParameter>();
            SqlParameter SqlParIdentityid = new SqlParameter("@identityid", SqlDbType.VarChar, 10);
            SqlParameter SqlParStarDate = new SqlParameter("@strStartDate", SqlDbType.DateTime);
            SqlParameter SqlParEndDate = new SqlParameter("@strEndDate", SqlDbType.DateTime);
            SqlParIdentityid.Value = strIdentityId;
            SqlParStarDate.Value = strStartDate;
            SqlParEndDate.Value = Convert.ToDateTime(strEndDate).AddDays(1).ToString("yyyy-MM-dd");
            sqlcmdlist_online.Add(SqlParIdentityid);
            sqlcmdlist_online.Add(SqlParStarDate);
            sqlcmdlist_online.Add(SqlParEndDate);
            SqlParameter[] sqlcmdpar_online = sqlcmdlist_online.ToArray();

            //報表
            List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
            sqlcmdlist.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist.Add(new SqlParameter("@strStartDate", strStartDate));
            sqlcmdlist.Add(new SqlParameter("@strEndDate", strEndDate));
            SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();
            List<SqlParameter> sqlcmdlist1 = new List<SqlParameter>();
            sqlcmdlist1.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist1.Add(new SqlParameter("@strStartDate", strStartDate));
            sqlcmdlist1.Add(new SqlParameter("@strEndDate", strEndDate));
            SqlParameter[] in_sqlcmdpar = sqlcmdlist1.ToArray();
            List<SqlParameter> sqlcmdlist2 = new List<SqlParameter>();
            sqlcmdlist2.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist2.Add(new SqlParameter("@strStartDate", strStartDate));
            sqlcmdlist2.Add(new SqlParameter("@strEndDate", strEndDate));
            SqlParameter[] out_sqlcmdpar = sqlcmdlist2.ToArray();
            List<SqlParameter> sqlcmdlist3 = new List<SqlParameter>();
            sqlcmdlist3.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist3.Add(new SqlParameter("@strStartDate", strStartDate));
            sqlcmdlist3.Add(new SqlParameter("@strEndDate", strEndDate));
            SqlParameter[] order_sqlcmdpar = sqlcmdlist3.ToArray();

            //上月報表
            List<SqlParameter> sqlcmdlist_lmonth = new List<SqlParameter>();
            sqlcmdlist_lmonth.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist_lmonth.Add(new SqlParameter("@strStartDate", System.DateTime.Now.AddMonths(-1).ToString("yyyy-MM") + "-01"));
            sqlcmdlist_lmonth.Add(new SqlParameter("@strEndDate", System.DateTime.Now.AddMonths(-1).ToString("yyyy-MM") + "-" + System.DateTime.DaysInMonth(System.DateTime.Now.Year, System.DateTime.Now.AddMonths(-1).Month)));
            SqlParameter[] sqlcmdpar_lmonth = sqlcmdlist_lmonth.ToArray();

            //本月報表
            List<SqlParameter> sqlcmdlist_month = new List<SqlParameter>();
            sqlcmdlist_month.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist_month.Add(new SqlParameter("@strStartDate", System.DateTime.Now.ToString("yyyy-MM") + "-01"));
            sqlcmdlist_month.Add(new SqlParameter("@strEndDate", System.DateTime.Now.ToString("yyyy-MM") + "-" + System.DateTime.DaysInMonth(System.DateTime.Now.Year, System.DateTime.Now.Month)));
            SqlParameter[] sqlcmdpar_month = sqlcmdlist_month.ToArray();

            //報表物件
            dt_month_all_record dmar = new dt_month_all_record();
            dt_month_all_record dar = new dt_month_all_record();
            dt_month_all_record dmlar = new dt_month_all_record();
            dt_month_all_record dmnar = new dt_month_all_record();

            //connectstring
            string report_in_conn = ReportHelper.Report_In_Conn;
            string report_out_conn = ReportHelper.Report_Out_Conn;
            string report_order_conn = ReportHelper.Report_Order_Conn;
            string report_share = ReportHelper.Report_Share;
            string instr = "";

            //datatable
            DataTable intable = null;
            string outstr = "";
            DataTable outtable = null;
            string orderstr = "";
            DataTable ordertable = null;
            string monthallstr = "";
            DataTable monthalltable = null;

            ReportSummary reportSummary = new ReportSummary();
            ReportHelper reporthelper = new ReportHelper();
            dt_month_all_record dmr = new dt_month_all_record();
            System.Threading.Tasks.Task online_task = new System.Threading.Tasks.Task(() =>
            {
                //在線人數
                try
                {
                    //var redisOnlineClientConfig = GlobalVariable.onlineRedisClient;
                    //Expression<Func<EasyFrame.NewCommon.Model.dt_sys_online, bool>> expressionOnLine = n => GetReportIndexOnLineCondition(n, strIdentityId);
                    //var onlineList = redisOnlineClientConfig.GetAll<EasyFrame.NewCommon.Model.dt_sys_online>().Where(expressionOnLine.Compile()).ToList();
                    int count = Flash_Dic.verdic[strIdentityId];
                    reportSummary.OnLinessNum = count.ToString();
                }
                catch
                {
                    reportSummary.OnLinessNum = "0";
                }

            }
            );
            online_task.Start();
            System.Threading.Tasks.Task lastmonth_task = new System.Threading.Tasks.Task(() =>
            {
                tselmonth_s = new TimeSpan(DateTime.Now.Ticks);
                //上月報表
                monthallstr = "select * from [srv_lnk_total_order].[dafacloud].[dbo].[dt_month_all_record] with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                monthalltable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(monthallstr, report_share, sqlcmdpar_lmonth);
                reporthelper.ConvertToMode(monthalltable, dmlar);
                tselmonth_e = new TimeSpan(DateTime.Now.Ticks);
                tselmonth = tselmonth_s.Subtract(tselmonth_e).Duration();


                //当日，本月，上月盈利，赢率和平台抽成的字段


                string monstr = "select *,rewardmoney as RewardMoney,rewardnumber as RewardNum,rewardcount as RewardCount from [srv_lnk_total_order].[dafacloud].[dbo].[dt_month_all_record] with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                var monthtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(monstr, report_share, sqlcmdpar_month);
                reporthelper.ConvertToMode(monthtable, dmr);

                monstr = "select rewardmoney as RewardMoney,rewardnumber as RewardNum,rewardcount as RewardCount from [srv_lnk_total_order].[dafacloud].[dbo].[dt_month_all_record] with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                var monthtablereward = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(monstr, report_share, sqlcmdpar_month);
                reporthelper.ConvertToMode(monthtablereward, dmr);

            }
            );
            lastmonth_task.Start();
            //sqlnewhelper DbHelperSQLNew = new sqlnewhelper();
            if (begindate.Substring(0, 10) == enddate.Substring(0, 10))//查询一天的直接获取汇总表里的数据//同一天
            {
                string in_str = "";
                string out_str = "";
                string order_str = "";

                // sqlnewhelper DbHelperSQLNew = new sqlnewhelper();
                System.Threading.Tasks.Task in_task = new System.Threading.Tasks.Task(() =>
                {
                    tsein_s = new TimeSpan(DateTime.Now.Ticks);

                    //查询in表的数据
                    //instr = "select * from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_sum]  with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";

                    instr = "select add_date,identityid,sum(czmony)as czmony,max(cznumber)as cznumber,sum(operattime)as operattime,sum(fastpaymoney)as fastpaymoney,max(fastnumber)as fastnumber,sum(bankmony)as bankmony,max(banknumber)as banknumber,sum(aliypaymony)as aliypaymony,max(aliypaynumber)as aliypaynumber,sum(wechatmony)as wechatmony,max(wechatnumber)as wechatnumber,sum(rgckmony)as rgckmony,max(rgcknumber)as rgcknumber,sum(rebatemony)as rebatemony,max(rebatenumber)as rebatenumber,sum(hdljmony)as hdljmony,max(hdljnumber)as hdljnumber,sum(xthdmony)as xthdmony,max(xthdnumber)as xthdnumber,sum(qthdmony)as qthdmony,max(qthdnumber)as qthdnumber,sum(zjmony)as zjmony,max(zjnumber)as zjnumber,sum(cdmony)as cdmony,max(cdnumber)as cdnumber,sum(dlgzmony)as dlgzmony,max(dlgznumber)as dlgznumber,sum(dlfhmony)as dlfhmony,max(dlfhnumber)as dlfhnumber,sum(czcount)as czcount,max(qqnumber)as qqnumber,sum(qqmoney)as qqmoney,sum(fourthmoney)as fourthmoney,max(fourthnumber)as fourthnumber,sum(unionpaymoney)as unionpaymoney,max(unionpaynumber)as unionpaynumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_sum]  with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate group by identityid,add_date";

                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, report_share, in_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(intable, dar);

                    //查询winning表的数据
                    instr = "select * from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_sum]  with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, report_share, in_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(intable, dar);

                    //查询get_point表的数据
                    instr = "select * from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_get_point_sum]  with(nolock) where  identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, report_share, in_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(intable, dar);

                    //非子查詢
                    in_str = "select count(distinct(case when type = 1 and state = 1 then user_id end))  as cznumber,count(distinct(case when type2 = 21 and state = 1 then user_id end)) as banknumber,count(distinct(case when type2 in (11, 20) and[state] = 1 then user_id end)) as rgcknumber,count(distinct(case when type2 in(7, 12) and[state] = 1 then user_id end)) as hdljnumber,count(distinct(case when type2 = 7 and[state] = 1 then user_id end)) as xthdnumber,count(distinct(case when type2 = 12 and[state] = 1 then user_id end)) as qthdnumber,count(distinct(case when type2 = 17 then user_id end)) as cdnumber,count(distinct(case when type2 = 14  and[state] = 1 then user_id end)) as dlgznumber,count(distinct(case when type2 = 13  and[state] = 1 then user_id end)) as dlfhnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock) where identityid =@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(intable, dar);

                    //--快捷支付/人数 ***
                    in_str = "select count(distinct(user_id)) as fastnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and type2 in (select fastpay from [dt_diction_quickpay] where fastpay>0) and [state]=1";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        dar.fastnumber = Convert.ToInt32(intable.Rows[0]["fastnumber"]);

                    //支付宝 ***
                    in_str = "select count(distinct(user_id)) as aliypaynumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and (type2 in(select Alipay from [dt_diction_quickpay] where Alipay>0) or  type2 =22) and [state]=1";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        dar.aliypaynumber = Convert.ToInt32(intable.Rows[0]["aliypaynumber"]);

                    //微信支付 ***
                    in_str = "select count(distinct(user_id)) as wechatnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and (type2 in(select Wechat from [dt_diction_quickpay] where Wechat>0) or type2 =23) and [state]=1";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        dar.wechatnumber = Convert.ToInt32(intable.Rows[0]["wechatnumber"]);

                    //QQ支付 ***
                    in_str = "select count(distinct(user_id)) as qqnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and (type2 in(select QQ from [dt_diction_quickpay] where QQ>0) or type2 =201) and [state]=1";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        dar.qqnumber = Convert.ToInt32(intable.Rows[0]["qqnumber"]);

                    //第四方支付 ***
                    in_str = "select count(distinct(user_id)) as fourthnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and type2=268 and [state]=1";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        dar.fourthnumber = Convert.ToInt32(intable.Rows[0]["fourthnumber"]);

                    //銀聯支付 ***
                    in_str = "select count(distinct(user_id)) as unionpaynumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and (type2 in (select Unionpay from [dt_diction_quickpay] where Unionpay>0) OR TYPE2=291 )and [state]=1";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        dar.unionpaynumber = Convert.ToInt32(intable.Rows[0]["unionpaynumber"]);

                    //中獎人數(同一天)

                    if (searchType == 1)
                    {
                        in_str = "select count(distinct(user_id)) as zjnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord] with(nolock) where identityid = @identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    }
                    else if (searchType == 2)
                    {
                        in_str = "select count(distinct(user_id)) as zjnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold] with(nolock) where identityid = @identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    }
                    else
                    {
                        in_str = "select count(1) as zjnumber from(select distinct(user_id) as zjnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord] with(nolock) where add_date >= @strStartDate and add_date <= @strEndDate and identityid = @identityid union select distinct(user_id) as zjnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold] with(nolock) where add_date >= @strStartDate and add_date <= @strEndDate and identityid = @identityid) as all_count ";
                    }
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(intable, dar);

                    //返點人數
                    in_str = "select count(distinct(user_id)) as rebatenumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_get_point_userrecord] with(nolock) where identityid = @identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(intable, dar);
                    tsein_e = new TimeSpan(DateTime.Now.Ticks);
                    tsein = tsein_s.Subtract(tsein_e).Duration();
                }
                );
                in_task.Start();


                System.Threading.Tasks.Task out_task = new System.Threading.Tasks.Task(() =>
                {
                    tseout_s = new TimeSpan(DateTime.Now.Ticks);
                    // sqlnewhelper DbHelperSQLNew = new sqlnewhelper();
                    //查询out表的数据
                    outstr = "select * from [srv_lnk_total_out].[dafacloud].[dbo].[dt_user_out_sum]  with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, report_share, out_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(outtable, dar);

                    //打賞總額
                    outstr = "select sum(rewardmoney) as RewardMoney from [srv_lnk_total_out].[dafacloud].[dbo].[dt_reward_sum] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, report_share, out_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(outtable, dar);
                    //打賞次數
                    outstr = "select sum(rewardcount) as RewardCount from [srv_lnk_total_out].[dafacloud].[dbo].[dt_reward_userrecord]  with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate ";
                    outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, report_share, out_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(outtable, dar);
                    //打賞人數
                    outstr = "select count(distinct(user_id)) as RewardNum from [srv_lnk_total_out].[dafacloud].[dbo].[dt_reward_userrecord]  with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, report_share, out_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(outtable, dar);

                    //out人數
                    out_str = "select count(distinct(case when type2 in(3,9,10)  and [state]=1 then user_id end))  as txnumber,count(distinct(case when type2=3  and [state]=1 then user_id end)) as rgtcnumber,count(distinct(case when type2=9  and [state]=1 then user_id end)) as wctcnumber,count(distinct(case when type2=10  and [state]=1 then user_id end)) as xztcnumber,count(distinct(case when type2=2 and state=3 then user_id end)) as jjnumber  from [srv_lnk_total_out].[dafacloud].[dbo].[dt_user_out_record] with(nolock) where identityid = @identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(out_str, report_share, out_sqlcmdpar);

                    lock (dar)
                        reporthelper.ConvertToMode(outtable, dar);
                    tseout_e = new TimeSpan(DateTime.Now.Ticks);
                    tseout = tseout_s.Subtract(tseout_e).Duration();
                }
                );
                out_task.Start();

                System.Threading.Tasks.Task order_task = new System.Threading.Tasks.Task(() =>
                {
                    tseorder_s = new TimeSpan(DateTime.Now.Ticks);
                    //sqlnewhelper DbHelperSQLNew = new sqlnewhelper();
                    //查询order表的数据
                    orderstr = "select * from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_sum]  with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    ordertable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(orderstr, report_share, order_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(ordertable, dar);

                    order_str = "select count(distinct(user_id)) as tznumber from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record] with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    ordertable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(order_str, report_share, order_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(ordertable, dar);
                    tseorder_e = new TimeSpan(DateTime.Now.Ticks);
                    tseorder = tseorder_s.Subtract(tseorder_e).Duration();
                }
                );
                order_task.Start();

                in_task.Wait();
                out_task.Wait();
                order_task.Wait();

            }
            else if (begindate.Substring(0, 10) == System.DateTime.Now.AddMonths(-1).ToString("yyyy-MM") + "-01" && enddate.Substring(0, 10) == System.DateTime.Now.AddMonths(-1).ToString("yyyy-MM") + "-" + System.DateTime.DaysInMonth(System.DateTime.Now.Year, System.DateTime.Now.AddMonths(-1).Month))
            {
                lastmonth_task.Wait();
                //上月報表
                dmar = dmlar;
            }
            else if (begindate.Substring(0, 10) == System.DateTime.Now.ToString("yyyy-MM") + "-01" && Convert.ToDateTime(enddate.Substring(0, 10)) >= Convert.ToDateTime(System.DateTime.Now.ToString("yyyy-MM-dd")) && Convert.ToDateTime(enddate.Substring(0, 10)).Day != 1)
            {
                lastmonth_task.Wait();
                //本月報表
                monthallstr = "select * from [srv_lnk_total_order].[dafacloud].[dbo].[dt_month_all_record]  with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                monthalltable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(monthallstr, report_share, sqlcmdpar_month);
                reporthelper.ConvertToMode(monthalltable, dmnar);

                monthallstr = "select rewardmoney as RewardMoney,rewardnumber as RewardNum,rewardcount as RewardCount from [srv_lnk_total_order].[dafacloud].[dbo].[dt_month_all_record] with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                var monthalltablereward = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(monthallstr, report_share, sqlcmdpar_month);
                reporthelper.ConvertToMode(monthalltablereward, dmnar);

                dmar = dmnar;

            }
            else //跨天查询的则人数需要从中间表汇总
            {
                string in_str = "";
                string out_str = "";
                string order_str = "";
                System.Threading.Tasks.Task in_task = new System.Threading.Tasks.Task(() =>
                {
                    tsein_s = new TimeSpan(DateTime.Now.Ticks);
                    //查询in表汇总
                    //instr = "select isnull(sum(czmony),0) as czmony,isnull(sum(fastpaymoney),0) as fastpaymoney,isnull(sum(bankmony),0) as bankmony,isnull(sum(aliypaymony),0) as aliypaymony,isnull(sum(wechatmony),0) as wechatmony,isnull(sum(qqmoney),0) as qqmoney,isnull(sum(fourthmoney),0) as fourthmoney,isnull(sum(unionpaymoney),0) as unionpaymoney,isnull(sum(rgckmony),0) as rgckmony,isnull(sum(rebatemony),0) as rebatemony,isnull(sum(hdljmony),0) as hdljmony,isnull(sum(xthdmony),0) as xthdmony,isnull(sum(qthdmony),0) as qthdmony,isnull(sum(zjmony),0) as zjmony,isnull(sum(cdmony),0) as cdmony,isnull(sum(dlgzmony),0) as dlgzmony,isnull(sum(dlfhmony),0) as dlfhmony,isnull(sum(czcount),0) as czcount,isnull(sum(operattime),0) as operattime from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_sum]  with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";

                    instr = "select isnull(sum(czmony),0) as czmony,isnull(sum(fastpaymoney),0) as fastpaymoney,isnull(sum(bankmony),0) as bankmony,isnull(sum(aliypaymony),0) as aliypaymony,isnull(sum(wechatmony),0) as wechatmony,isnull(sum(qqmoney),0) as qqmoney,isnull(sum(fourthmoney),0) as fourthmoney,isnull(sum(unionpaymoney),0) as unionpaymoney,isnull(sum(rgckmony),0) as rgckmony,isnull(sum(rebatemony),0) as rebatemony,isnull(sum(hdljmony),0) as hdljmony,isnull(sum(xthdmony),0) as xthdmony,isnull(sum(qthdmony),0) as qthdmony,isnull(sum(zjmony),0) as zjmony,isnull(sum(cdmony),0) as cdmony,isnull(sum(dlgzmony),0) as dlgzmony,isnull(sum(dlfhmony),0) as dlfhmony,isnull(sum(czcount),0) as czcount,isnull(sum(operattime),0) as operattime from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_sum]  with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate group by identityid";

                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, report_share, in_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(intable, dar);

                    //查询winning表的数据
                    instr = "select isnull(sum(zjmoney),0) as zjmoney,isnull(sum(zjcount),0) as zjcount from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_sum]  with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, report_share, in_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(intable, dar);

                    //查询get_point表的数据
                    instr = "select isnull(sum(rebatemoney),0) as rebatemoney,isnull(sum(rebatecount),0) as rebatecount from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_get_point_sum]  with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, report_share, in_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(intable, dar);



                    //非子查詢
                    in_str = "select count(distinct(case when type = 1 and state = 1 then user_id end))  as cznumber,count(distinct(case when type2 = 21 and state = 1 then user_id end)) as banknumber,count(distinct(case when type2 in (11, 20) and[state] = 1 then user_id end)) as rgcknumber,count(distinct(case when type2 in(7, 12) and[state] = 1 then user_id end)) as hdljnumber,count(distinct(case when type2 = 7 and[state] = 1 then user_id end)) as xthdnumber,count(distinct(case when type2 = 12 and[state] = 1 then user_id end)) as qthdnumber,count(distinct(case when type2 = 17 then user_id end)) as cdnumber,count(distinct(case when type2 = 14  and[state] = 1 then user_id end)) as dlgznumber,count(distinct(case when type2 = 13  and[state] = 1 then user_id end)) as dlfhnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock) where identityid =@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(intable, dar);

                    //--快捷支付/人数 ***
                    in_str = "select count(distinct(user_id)) as fastnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and type2 in (select fastpay from [dt_diction_quickpay] where fastpay>0) and [state]=1";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        dar.fastnumber = Convert.ToInt32(intable.Rows[0]["fastnumber"]);

                    //支付宝 ***
                    in_str = "select count(distinct(user_id)) as aliypaynumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and (type2 in(select Alipay from [dt_diction_quickpay] where Alipay>0) or  type2 =22) and [state]=1";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        dar.aliypaynumber = Convert.ToInt32(intable.Rows[0]["aliypaynumber"]);

                    //微信支付 ***
                    in_str = "select count(distinct(user_id)) as wechatnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and (type2 in(select Wechat from [dt_diction_quickpay] where Wechat>0) or type2 =23) and [state]=1";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        dar.wechatnumber = Convert.ToInt32(intable.Rows[0]["wechatnumber"]);

                    //QQ支付 ***
                    in_str = "select count(distinct(user_id)) as qqnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and (type2 in(select QQ from [dt_diction_quickpay] where QQ>0) or type2 =201) and [state]=1";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        dar.qqnumber = Convert.ToInt32(intable.Rows[0]["qqnumber"]);

                    //第四方支付 ***
                    in_str = "select count(distinct(user_id)) as fourthnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and type2 = 268 and [state]=1";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        dar.fourthnumber = Convert.ToInt32(intable.Rows[0]["fourthnumber"]);

                    //銀聯支付 ***
                    in_str = "select count(distinct(user_id)) as unionpaynumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and (type2 in(select Unionpay from [dt_diction_quickpay] where Unionpay>0) or type2 =291) and [state]=1";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        dar.unionpaynumber = Convert.ToInt32(intable.Rows[0]["unionpaynumber"]);

                    //中獎人數
                    if (searchType == 1)
                    {
                        in_str = "select count(distinct(user_id)) as zjnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord] with(nolock) where identityid = @identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    }
                    else if (searchType == 2)
                    {
                        in_str = "select count(distinct(user_id)) as zjnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold] with(nolock) where identityid = @identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    }
                    else
                    {
                        in_str = "select count(1) as zjnumber from(select distinct(user_id) as zjnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord] with(nolock) where add_date >= @strStartDate and add_date <= @strEndDate and identityid = @identityid union select distinct(user_id) as zjnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold] with(nolock) where add_date >= @strStartDate and add_date <= @strEndDate and identityid = @identityid) as all_count ";
                    }
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(intable, dar);

                    //返點人數
                    in_str = "select count(distinct(user_id)) as rebatenumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_get_point_userrecord] with(nolock) where identityid = @identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, in_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(intable, dar);
                    tsein_e = new TimeSpan(DateTime.Now.Ticks);
                    tsein = tsein_s.Subtract(tsein_e).Duration();
                }
                );
                in_task.Start();

                System.Threading.Tasks.Task out_task = new System.Threading.Tasks.Task(() =>
                {
                    tseout_s = new TimeSpan(DateTime.Now.Ticks);
                    //查询out表的数据
                    outstr = "select isnull(sum(txmony),0) as txmony,isnull(sum(rgtcmoney),0) as rgtcmoney,isnull(sum(wctcmony),0) as wctcmony,isnull(sum(xztcmony),0) as xztcmony,isnull(sum(jjmony),0) as jjmony,isnull(sum(txcount),0) as txcount,isnull(sum(opearttime),0) as opearttime  from [srv_lnk_total_out].[dafacloud].[dbo].[dt_user_out_sum] with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, report_share, out_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(outtable, dar);

                    //打賞總額
                    outstr = "select sum(rewardmoney) as RewardMoney from [srv_lnk_total_out].[dafacloud].[dbo].[dt_reward_sum] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, report_share, out_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(outtable, dar);
                    //打賞次數
                    outstr = "select sum(rewardcount) as RewardCount from [srv_lnk_total_out].[dafacloud].[dbo].[dt_reward_userrecord]  with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate ";
                    outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, report_share, out_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(outtable, dar);
                    //打賞人數
                    outstr = "select count(distinct(user_id)) as RewardNum from [srv_lnk_total_out].[dafacloud].[dbo].[dt_reward_userrecord] with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, report_share, out_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(outtable, dar);

                    //out人數
                    out_str = "select count(distinct(case when type2 in(3,9,10)  and [state]=1 then user_id end))  as txnumber,count(distinct(case when type2=3  and [state]=1 then user_id end)) as rgtcnumber,count(distinct(case when type2=9  and [state]=1 then user_id end)) as wctcnumber,count(distinct(case when type2=10  and [state]=1 then user_id end)) as xztcnumber,count(distinct(case when type2=2 and state=3 then user_id end)) as jjnumber  from [srv_lnk_total_out].[dafacloud].[dbo].[dt_user_out_record] with(nolock) where identityid = @identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(out_str, report_share, out_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(outtable, dar);
                    tseout_e = new TimeSpan(DateTime.Now.Ticks);
                    tseout = tseout_s.Subtract(tseout_e).Duration();
                }
                );
                out_task.Start();

                System.Threading.Tasks.Task order_task = new System.Threading.Tasks.Task(() =>
                {
                    tseorder_s = new TimeSpan(DateTime.Now.Ticks);
                    //查询order表的数据
                    orderstr = "select isnull(sum(tzmony),0) as tzmony,isnull(sum(tzcount),0) as tzcount from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_sum] with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    ordertable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(orderstr, report_share, order_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(ordertable, dar);

                    //投注人数
                    order_str = "select count(distinct(user_id)) as tznumber from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record] with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                    ordertable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(order_str, report_share, order_sqlcmdpar);
                    lock (dar)
                        reporthelper.ConvertToMode(ordertable, dar);
                    tseorder_e = new TimeSpan(DateTime.Now.Ticks);
                    tseorder = tseorder_s.Subtract(tseorder_e).Duration();
                }
                );
                order_task.Start();
                in_task.Wait();
                out_task.Wait();
                order_task.Wait();
            }

            if (begindate.Substring(0, 10) == System.DateTime.Now.AddMonths(-1).ToString("yyyy-MM") + "-01" && enddate.Substring(0, 10) == System.DateTime.Now.AddMonths(-1).ToString("yyyy-MM") + "-" + System.DateTime.DaysInMonth(System.DateTime.Now.Year, System.DateTime.Now.AddMonths(-1).Month))
            {
                //將dt_month_all_record轉換為ReportSummary
                reporthelper.ConvertReportModelToModel(reportSummary, dmar);
                dar = dmar;
            }
            else if (begindate.Substring(0, 10) == System.DateTime.Now.ToString("yyyy-MM") + "-01" && Convert.ToDateTime(enddate.Substring(0, 10)) >= Convert.ToDateTime(System.DateTime.Now.ToString("yyyy-MM-dd")) && Convert.ToDateTime(enddate.Substring(0, 10)).Day != 1)
            {
                //將dt_month_all_record轉換為ReportSummary
                reporthelper.ConvertReportModelToModel(reportSummary, dmar);
                dar = dmar;
            }
            else
            {
                //將dt_month_all_record轉換為ReportSummary
                reporthelper.ConvertReportModelToModel(reportSummary, dar);
            }


            FormatHelper formHelper = new FormatHelper();
            double fInMoneyTime = 0;
            //將充值操作時間=充值時間/充值筆數
            if (!string.IsNullOrWhiteSpace(reportSummary.InMoneyTime))
            {
                decimal fTime = GetMoney(reportSummary.InMoneyTime);
                double fNum = GetNum(reportSummary.InMoneyTime);
                if (fNum != 0)
                {
                    fInMoneyTime = (double)fTime / fNum;
                }
            }
            //轉換為時分秒
            reportSummary.InMoneyTime = formHelper.FormatSecond(fInMoneyTime);
            double fOutMoneyTime = 0;
            //將提現操作時間=提現時間/提現筆數
            if (!string.IsNullOrWhiteSpace(reportSummary.OutMoneyTime))
            {
                decimal fTime = GetMoney(reportSummary.OutMoneyTime);
                double fNum = GetNum(reportSummary.OutMoneyTime);
                if (fNum != 0)
                {
                    fOutMoneyTime = (double)fTime / fNum;
                }
            }
            //轉換為時分秒
            reportSummary.OutMoneyTime = formHelper.FormatSecond(fOutMoneyTime);

            lastmonth_task.Wait();

            decimal ss = dmlar.sysymoney;
            decimal lastmonth_profitloss = (dmlar.tzmony - dmlar.zjmoney - dmlar.hdljmony - dmlar.rebatemoney - (dmlar.dlgzmony + dmlar.dlfhmony) + dmlar.xztcmony + dmlar.jjmony);
            decimal lastmonth_profitrate = 0;
            if (dmlar.tzmony > 0)
            {
                dmr.syylrate = (lastmonth_profitloss / dmlar.tzmony * 100);
                lastmonth_profitrate = (lastmonth_profitloss / dmlar.tzmony * 100);
            }
            reportSummary.LastMonthOut_BettingAccount = dmlar.tzmony.ToString("0.00") + "/" + dmr.ytznumber.ToString("0") + "人";
            //月投注额月投人數
            reportSummary.MonthOut_BettingAccount = dmr.ytzmoney.ToString("0.00") + "/" + dmr.ytznumber.ToString("0") + "人";
            //月中奖额月中奖额
            reportSummary.MonthOut_WinningAccount = dmr.yzjmoney.ToString("0.00") + "/" + dmr.yzjnumber.ToString("0") + "人";
            //本月损益
            reportSummary.MonthProfitAndLoss = dmr.bysymoney.ToString("0.00");
            //上月损益
            reportSummary.LastMonthProfitAndLoss = (dmlar.tzmony - dmlar.zjmoney).ToString("0.00");
            //本月盈利
            reportSummary.MonthProfitLoss = dmr.byylmoney.ToString("0.00");
            //本月盈率
            reportSummary.MonthProfitRate = dmr.byylrate.ToString("0.00");
            //上月盈利
            reportSummary.LastMonthProfitLoss = lastmonth_profitloss.ToString("0.00");
            //上月赢率
            // reportSummary.LastMonthProfitRate = dmr.syylrate.ToString("0.00");
            //平台抽成
            reportSummary.TakePercentage = dmr.ptccmoney.ToString("0.00");

            //当日盈利需拿当日数据计算
            //查询in表的数据

            decimal zjmoney = dar.zjmoney;//中奖金额
            decimal hdljmony = dar.hdljmony;//活动礼金
            decimal rebatemony = dar.rebatemoney;//返点
            decimal dlgzmony = dar.dlgzmony;// 代理工资
            decimal dlfhmony = dar.dlfhmony;//代理分红
            decimal xztcmony = dar.xztcmony;//行政提出
            decimal jjmony = dar.jjmony;//拒绝金额
            decimal tzmony = dar.tzmony;//投注金额

            //盈利=投注金额-中奖金额-活动礼金-返点-代理金额+行政提出+拒绝金额
            dmr.dlylmoney = tzmony - zjmoney - hdljmony - rebatemony - (dlgzmony + dlfhmony) + xztcmony + jjmony;
            if (tzmony > 0)
            {
                dmr.dlylrate = dmr.dlylmoney / tzmony * 100;
            }
            reportSummary.NowProfit = dmr.dlylmoney.ToString("0.00");
            reportSummary.NowProfitRate = dmr.dlylrate.ToString("0.00");
            reportSummary.CurrentProfitAndLoss = (dar.tzmony - dar.zjmoney).ToString("0.00");
            string sql_str = "select count(distinct user_id )  as txnumber  from [srv_lnk_total_out].[dafacloud].[dbo].[dt_user_out_record] with(nolock) where identityid = @identityid and add_date>=@strStartDate and add_date<=@strEndDate and type2 in(9,10)  and [state]=1";
            outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(sql_str, report_share, sqlcmdpar);
            //Convert.ToDecimal(dicPropertyInfo["rgtcmoney"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["rgtcnumber"].GetValue(mode_source, null) + "人";

            reportSummary.TotalArtificialOut = dar.wctcmony + dar.xztcmony + "/" + outtable.Rows[0]["txnumber"] + "人";
            //out人數

            tseother_s = new TimeSpan(DateTime.Now.Ticks);


            //线上汇总数据
            string congkustr = "";
            DataTable congkutable;
            dt_congku_sum dcs = new dt_congku_sum();

            tsereg_s = new TimeSpan(DateTime.Now.Ticks);
            //註冊人數
            congkustr = "select count(distinct(userid)) as zcnumber  from dt_users_register  where identityid=@identityid  and Tester=0 and addtime>=@strStartDate and addtime<=@strEndDate";
            congkutable = DBcon.DbHelperSQL.GetQueryFromCongku(congkustr, sqlcmdpar_online);
            dcs.zcnumber = Convert.ToInt32(congkutable.Rows[0]["zcnumber"]);
            reportSummary.NewMemberNum = dcs.zcnumber.ToString();
            tsereg_e = new TimeSpan(DateTime.Now.Ticks);
            tsereg = tsereg_s.Subtract(tsereg_e).Duration();

            tsenewpay_s = new TimeSpan(DateTime.Now.Ticks);
            //首充人數
            congkustr = "select count(distinct(userid)) as scnumber  from dt_user_newpay  where identityid=@identityid and flog=0 and addtime>=@strStartDate and addtime<=@strEndDate";
            congkutable = DBcon.DbHelperSQL.GetQueryFromCongku(congkustr, sqlcmdpar_online);
            dcs.scnumber = Convert.ToInt32(congkutable.Rows[0]["scnumber"]);
            reportSummary.NewPrepaidNum = dcs.scnumber.ToString();
            tsenewpay_e = new TimeSpan(DateTime.Now.Ticks);
            tsenewpay = tsenewpay_s.Subtract(tsenewpay_e).Duration();

            tsecapital_s = new TimeSpan(DateTime.Now.Ticks);
            //剩餘金額
            congkustr = "select isnull(sum(use_money),0) as userMoney from dt_user_capital  where identityid=@identityid and flog=0";
            congkutable = DBcon.DbHelperSQL.GetQueryFromCongku(congkustr, sqlcmdpar_online);
            dcs.userMoney = Convert.ToDecimal(congkutable.Rows[0]["userMoney"]);
            reportSummary.TotlaBalance = dcs.userMoney.ToString();
            online_task.Wait();
            if (online_task.Status == System.Threading.Tasks.TaskStatus.RanToCompletion)
            {
                online_task.Dispose();
            }
            tsecapital_e = new TimeSpan(DateTime.Now.Ticks);
            tsecapital = tsecapital_s.Subtract(tsecapital_e).Duration();
            tseother_e = new TimeSpan(DateTime.Now.Ticks);
            tseother = tseother_s.Subtract(tseother_e).Duration();
            ////在線人數
            //var redisOnlineClientConfig = GlobalVariable.onlineRedisClient;
            //Expression<Func<EasyFrame.NewCommon.Model.dt_sys_online, bool>> expressionOnLine = n => GetReportIndexOnLineCondition(n, strIdentityId);
            //var onlineList = redisOnlineClientConfig.GetAll<EasyFrame.NewCommon.Model.dt_sys_online>().Where(expressionOnLine.Compile()).ToList();
            //reportSummary.OnLinessNum = onlineList.Count.ToString();
            listReportSummary.Add(reportSummary);


            //以下将以上各类的字段赋值显示到页面上去即可
            DataTable dataTable = dataTableHelper.ConvertListToDataTable(listReportSummary);
            DataSet ds = new DataSet();
            ds.Tables.Add(dataTable);
            TimeSpan tse2 = new TimeSpan(DateTime.Now.Ticks);
            tse = tse1.Subtract(tse2).Duration();
            //Log.Info("查询综合报表结束时间：" + DateTime.Now.ToString());
            LogMsg logmsg2 = new LogMsg();
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("After_GetReportIndexData", "GetReportIndexData", st.ElapsedMilliseconds.ToString(), para);
            }
            return ds;
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("After_Error", "GetReportIndexData", st.ElapsedMilliseconds.ToString(), para);
            }
            return null;
        }
        finally
        {

        }



    }


    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetRedis(string strIdentityId, string strStartDate, string strEndDate)
    {
        int count = 0;
        DataTableHelper dataTableHelper = new DataTableHelper();
        //在線人數
        var redisOnlineClientConfig = GlobalVariable.onlineRedisClient;
        Expression<Func<EasyFrame.NewCommon.Model.dt_sys_online, bool>> expressionOnLine = n => GetReportIndexOnLineCondition(n, strIdentityId);
        var onlineList = redisOnlineClientConfig.GetAll<EasyFrame.NewCommon.Model.dt_sys_online>().Where(expressionOnLine.Compile()).ToList();
        count = onlineList.Count();
        List<EasyFrame.NewCommon.Model.dt_sys_online> list = new List<EasyFrame.NewCommon.Model.dt_sys_online>();
        for (int i = 0; i < count; i++)
        {
            EasyFrame.NewCommon.Model.dt_sys_online ss2 = (EasyFrame.NewCommon.Model.dt_sys_online)onlineList[i];
            list.Add(ss2);
        }

        DataTable dataTable = dataTableHelper.ConvertListToDataTable(list);
        DataSet ds = new DataSet();
        ds.Tables.Add(dataTable);

        return ds;
    }

    /// <summary>
    /// 获取代理报表查询条件语句
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strUserName">代理名称</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">结束日期</param>
    /// <returns></returns>
    private string GetReportAgentWhereSql(string strIdentityId, string strUserName, string strStartDate, string strEndDate, ref List<SqlParameter> sqlcmdlist)
    {
        StringBuilder strTemp = new StringBuilder();
        strUserName = strUserName.Replace("'", "");
        strStartDate = strStartDate.Replace("'", "");
        strEndDate = strEndDate.Replace("'", "");
        strTemp.Append(" identityid=@identityid and is_agent=1 and flog=0");
        sqlcmdlist.Add(new SqlParameter("@identityid", strIdentityId));

        if (!string.IsNullOrEmpty(strUserName))
        {
            strTemp.Append(" and id in (select id from [dt_users]  where user_name=@username) and flog=0");
            sqlcmdlist.Add(new SqlParameter("@username", strUserName));
        }
        else
        {
            strTemp.Append(" and agent_id=-1 and flog=0");
        }
        return strTemp.ToString();
    }



    /// <summary>
    /// 获取代理报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strUserName">代理ID</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportAgentData(string strIdentityId, string strUserName, string strStartDate, string strEndDate, int pageIndex, int pageSize)
    {
        DataSet dataSet = new DataSet();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "IdentityId=" + strIdentityId + " UserName=" + strUserName + " StartTime=" + strStartDate + " EndTime=" + strEndDate + " pageIndex=" + pageIndex + " pageSize=" + pageSize;
        try
        {
            st.Reset();
            st.Start();
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return dataSet;
            //}            

            //获取代理统计表參數
            DateTime dtStartTime = DateTime.Parse(strStartDate);
            DateTime dtEndTime = DateTime.Parse(strEndDate);
            ReportHelper reporthelper = new ReportHelper();

            //par

            //connectstring
            string report_share = ReportHelper.Report_Share;
            string agent_in_conn = DBcon.DBcontring.Agent_In_Conn;
            string agent_out_conn = DBcon.DBcontring.Agent_Out_Conn;
            string agent_order_conn = DBcon.DBcontring.Agent_Order_Conn;
            string report_order_conn = DBcon.DBcontring.Report_Order_Conn;

            //報表
            List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
            sqlcmdlist.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist.Add(new SqlParameter("@strStartDate", strStartDate));
            sqlcmdlist.Add(new SqlParameter("@strEndDate", strEndDate));
            SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();

            //站
            List<SqlParameter> sqlcmdlist_identityid = new List<SqlParameter>();
            sqlcmdlist_identityid.Add(new SqlParameter("@identityid", strIdentityId));
            SqlParameter[] sqlcmdpar_identityid = sqlcmdlist_identityid.ToArray();

            //線上
            List<SqlParameter> sqlcmdlist_online = new List<SqlParameter>();
            sqlcmdlist_online.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist_online.Add(new SqlParameter("@strStartDate", strStartDate));
            sqlcmdlist_online.Add(new SqlParameter("@strEndDate", Convert.ToDateTime(strEndDate).AddDays(1).ToString("yyyy-MM-dd")));
            SqlParameter[] sqlcmdpar_online = sqlcmdlist_online.ToArray();

            //datatable
            DataTable agentUser = new DataTable();
            DataTable listUsers = new DataTable();
            DataTable usersDataTable = new DataTable();
            DataTable dt_agentin = new DataTable();
            DataTable dt_agentwin = new DataTable();
            DataTable dt_agentorder = new DataTable();
            DataTable dt_agentout = new DataTable();
            DataTable regDataTable = new DataTable();
            DataTable congkutable = new DataTable();
            DataTable report_betnumtable = new DataTable();
            DataTable totalDataTable = new DataTable();
            DataTable dataTable = new DataTable();


            //list
            List<ReportAgent> listAgentReport = new List<ReportAgent>();
            List<dt_report_newprepaid> newPrepaidList = new List<dt_report_newprepaid>();
            List<dt_report_num> reportNumList = new List<dt_report_num>();

            //字典
            Dictionary<string, string> dicAgent = new Dictionary<string, string>();
            Dictionary<string, string> dicUsers = new Dictionary<string, string>();
            Dictionary<string, int> dicRegNum = new Dictionary<string, int>();
            Dictionary<string, ReportAgent> dicReportAgent = new Dictionary<string, ReportAgent>();
            //首充人数
            Dictionary<string, int> dicNewPrepaid = new Dictionary<string, int>();
            //投注人数
            Dictionary<string, int> dicBetNum = new Dictionary<string, int>();


            //1、获取代理档案
            //站長
            List<SqlParameter> sqlcmdlist_ali = new List<SqlParameter>();
            string strWhereSql = GetReportAgentWhereSql(strIdentityId, strUserName, strStartDate, strEndDate, ref sqlcmdlist_ali);
            SqlParameter[] sqlcmdpar_ali = sqlcmdlist_ali.ToArray();
            listUsers = DBcon.DbHelperSQL.Query("Select id as UserId,user_name as UserName from [dt_users] where " + strWhereSql, sqlcmdpar_ali).Tables[0];

            //2、获取站长添加所关心的代理的数据
            //站長
            List<SqlParameter> sqlcmdlist_alicare = new List<SqlParameter>();
            sqlcmdlist_alicare.Add(new SqlParameter("@identityid", strIdentityId));
            string strSql = "select UserId,UserName from [dt_agent_show] where identityid=@identityid ";
            if (!string.IsNullOrWhiteSpace(strUserName))
            {
                sqlcmdlist_alicare.Add(new SqlParameter("@username", strUserName));
                strSql += " and UserId in (select id from [dt_users]  where user_name=@username)";
            }
            SqlParameter[] sqlcmdpar_care = sqlcmdlist_alicare.ToArray();
            agentUser = DBcon.DbHelperSQL.Query(strSql, sqlcmdpar_care).Tables[0];

            //將站長加上關心代理
            if (listUsers.Rows.Count > 0)
            {
                agentUser.Merge(listUsers.Copy());
            }

            string userIds = string.Empty;

            //取得站長關注的玩家字典
            for (int i = 0; i < agentUser.Rows.Count; i++)
            {
                string strUserId = agentUser.Rows[i]["UserId"].ToString();
                userIds += strUserId + ",";
                if (dicAgent.ContainsKey(strUserId))
                    continue;

                dicAgent.Add(strUserId, agentUser.Rows[i]["UserName"].ToString());
                ReportAgent agentReport = new ReportAgent();
                agentReport.UserId = strUserId;
                if (!dicReportAgent.ContainsKey(strUserId))
                {
                    dicReportAgent.Add(strUserId, agentReport);
                    listAgentReport.Add(agentReport);
                }
            }
            //in
            string agent_in_str = "select identityid,user_id as UserId,isnull(sum(hdljmoney),0) as TotalDiscountAccount,isnull(sum(tdfdmoney),0) as TotalRebateAccount,isnull(sum(czmoney),0) as TotalInMoney,isnull(sum(dlfdmoney),0) as RebateAccount,isnull(sum(dlgzmoney),0) as TotalWages,isnull(sum(dlfhmoney),0) as TotalBonus    from [srv_lnk_agent_in].[dafacloud].[dbo].[dt_user_in_proxy_report] with(nolock)   where identityid = @identityid and add_date >= @strStartDate and add_date<= @strEndDate group by identityid,user_id ";
            dt_agentin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_in_str, report_share, sqlcmdpar);
            //win
            string agent_win_str = "select identityid,user_id as UserId,isnull(sum(zjmoney),0) as TotalWinningAccount from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_lottery_winning_proxy_report] with(nolock)  where identityid = @identityid and add_date >= @strStartDate and add_date<= @strEndDate group by identityid,user_id ";
            dt_agentwin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_win_str, report_share, sqlcmdpar);
            //order
            string agent_order_str = "select identityid,user_id as UserId,isnull(sum(tzmoney),0) as TotalBettingAccount from [srv_lnk_agent_order].[dafacloud].[dbo].[dt_lottery_proxy_report] with(nolock)  where identityid= @identityid and add_date >=@strStartDate and add_date <=@strEndDate  group by identityid,user_id  ";
            dt_agentorder = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_order_str, report_share, sqlcmdpar);
            //out
            string agent_out_str = "select identityid,user_id as UserId,isnull(sum(txmoney),0) as TotalOutMoney from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_user_out_proxy_report] with(nolock)  where identityid= @identityid and add_date >=@strStartDate and add_date <=@strEndDate  group by identityid,user_id  ";
            dt_agentout = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_out_str, report_share, sqlcmdpar);


            for (int i = 0; i < listAgentReport.Count; i++)
            {
                string listAgentId_str = listAgentReport[i].UserId;
                if (dt_agentin.Select("UserId = '" + listAgentId_str + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(dt_agentin.Select("UserId = '" + listAgentId_str + "'")[0], listAgentReport[i]);
                }
                if (dt_agentwin.Select("UserId = '" + listAgentId_str + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(dt_agentwin.Select("UserId = '" + listAgentId_str + "'")[0], listAgentReport[i]);
                }
                if (dt_agentorder.Select("UserId = '" + listAgentId_str + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(dt_agentorder.Select("UserId = '" + listAgentId_str + "'")[0], listAgentReport[i]);
                }
                if (dt_agentout.Select("UserId = '" + listAgentId_str + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(dt_agentout.Select("UserId = '" + listAgentId_str + "'")[0], listAgentReport[i]);
                }
            }

            //去除最後的,
            if (userIds.Length > 0)
            {
                userIds = userIds.Substring(0, userIds.Length - 1);
            }
            else
            {
                userIds = "0";
            }

            //获取注册人数            
            string reg_str = "select  v2.father_id,count(distinct(v2.user_id))count  from (select identityid,userid,addtime from [dbo].[dt_users_register] where identityid =@identityid and addtime>=@strStartDate and addtime<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from [dbo].[dt_users_class] where identityid =@identityid  ) as v2 on v1.userid = v2.user_id    where  v1.identityid =@identityid  and v1.addtime>=@strStartDate and v1.addtime<@strEndDate and father_id is not null      group by v2.father_id ";
            regDataTable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(reg_str, report_share, sqlcmdpar_online);
            //將字典dicRegNum填入代理級此代理下註冊人數
            for (int i = 0; i < regDataTable.Rows.Count; i++)
            {
                string strUserId = regDataTable.Rows[i]["father_id"].ToString();
                int strUsercount = Convert.ToInt32(regDataTable.Rows[i]["count"]);
                //ComputeNum(dicRegNum, strUserId, dicAgentData);
                dicRegNum.Add(strUserId, strUsercount);
            }

            //首充人数
            string congkustr = "select  v2.father_id,count(distinct(v2.user_id))count  from (select identityid,userid,addtime from [dbo].[dt_user_newpay] where identityid =@identityid and addtime>=@strStartDate and addtime<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from [dbo].[dt_users_class] where identityid =@identityid  ) as v2 on v1.userid = v2.user_id    where  v1.identityid =@identityid  and v1.addtime>=@strStartDate and v1.addtime<@strEndDate and father_id is not null      group by v2.father_id  ";

            congkutable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(congkustr, report_share, sqlcmdpar_online);
            for (int i = 0; i < congkutable.Rows.Count; i++)
            {
                //dt_report_newprepaid report_newprepaid = new dt_report_newprepaid();
                //reporthelper.ConvertDataRowToModel_sql(congkutable.Rows[i], report_newprepaid);
                //newPrepaidList.Add(report_newprepaid);
                string strUserId = congkutable.Rows[i]["father_id"].ToString();
                int strUsercount = Convert.ToInt32(congkutable.Rows[i]["count"]);
                // ComputeNum(dicNewPrepaid, strUserId, dicAgentData);
                dicNewPrepaid.Add(strUserId, strUsercount);

            }

            string query_str2 = "  select father_id ,sum(count) as count  from ( select  v2.father_id,count(distinct(v2.user_id))count  from ( select identityid,user_id,add_date from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record] where identityid =@identityid and add_date>=@strStartDate and add_date<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from [dbo].[dt_users_class] where identityid =@identityid   ) as v2  on v1.user_id = v2.user_id    where  v1.identityid =@identityid  and v1.add_date>=@strStartDate and v1.add_date<@strEndDate and father_id is not null      group by v2.father_id "
                      //  + " union all  select user_id as father_id,count(distinct(user_id)) count from [dbo].[dt_lottery_record]  left outer join (  select father_id from [dt_users_class]  where identityid='"+identityid+"' ) as v2 on [dt_lottery_record].User_Id = v2.father_id where [dt_lottery_record].identityid ='"+identityid+"' and [dt_lottery_record].add_date>=@strStartDate and [dt_lottery_record].add_date<@strEndDate   and v2.father_id is null  group by identityid,user_id "
                      + " union all  select v2.user_id,count(distinct(v2.user_id))count from(select identityid, user_id, add_date from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record] where identityid = @identityid and add_date>= @strStartDate and add_date<@strEndDate  ) as v1 left outer join(select identityid, father_id, user_id, addtime from [dbo].[dt_users_class] where identityid = @identityid   ) as v2  on v1.user_id = v2.user_id    where v1.identityid =@identityid  and v1.add_date>=@strStartDate and v1.add_date<@strEndDate     group by v2.user_id  "
                      + " ) as view2  group by father_id OPTION (RECOMPILE)";
            report_betnumtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(query_str2, report_share, sqlcmdpar_online);
            // 投注人数
            for (int i = 0; i < report_betnumtable.Rows.Count; i++)
            {
                //dt_report_num report_num = new dt_report_num();
                //reporthelper.ConvertDataRowToModel_sql(report_betnumtable.Rows[i], report_num);
                //reportNumList.Add(report_num);
                //string strUserId = report_betnumtable.Rows[i]["father_id"].ToString();
                string strUserId = report_betnumtable.Rows[i]["father_id"].ToString();
                int strUsercount = Convert.ToInt32(report_betnumtable.Rows[i]["count"]);
                // ComputeNum(dicBetNum, strUserId, dicAgentData);
                string ss = "";
                try
                {
                    ss = strUserId;
                    dicBetNum.Add(strUserId, strUsercount);
                }
                catch
                {
                    string err = ss;
                }
            }

            //默认排序投注递减
            listAgentReport = listAgentReport.OrderByDescending(m => m.TotalBettingAccount).ToList();
            int totalCount = listAgentReport.Count;

            //保存记录总数量的表
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);
            listAgentReport = listAgentReport.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            for (int i = 0; i < listAgentReport.Count; i++)
            {
                string strAgentId = listAgentReport[i].UserId;
                //总代名称
                if (dicAgent.ContainsKey(strAgentId))
                {
                    listAgentReport[i].UserName = dicAgent[strAgentId];
                }
                //投注人数
                if (dicBetNum.ContainsKey(strAgentId))
                {
                    listAgentReport[i].TotalBettingNum = dicBetNum[strAgentId];
                }
                //首充人数
                if (dicNewPrepaid.ContainsKey(strAgentId))
                {
                    listAgentReport[i].NewPrepaidNum = dicNewPrepaid[strAgentId];
                }
                //注册人数
                if (dicRegNum.ContainsKey(strAgentId))
                {
                    listAgentReport[i].RegNum = dicRegNum[strAgentId];
                }
                //盈利=中奖-投注+团队返点+团队活动优惠
                listAgentReport[i].ProfitLoss = listAgentReport[i].TotalWinningAccount - listAgentReport[i].TotalBettingAccount + listAgentReport[i].TotalDiscountAccount + listAgentReport[i].TotalRebateAccount;
            }
            DataTableHelper dataTableHelper = new DataTableHelper();
            dataTable = dataTableHelper.ConvertListToDataTable(listAgentReport);
            dataSet.Tables.Add(dataTable);
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("After_GetReportAgentData", "GetReportAgentData", st.ElapsedMilliseconds.ToString(), para);
            }
            return dataSet;
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("After_Error", "GetReportAgentData", st.ElapsedMilliseconds.ToString(), para);
            }
            return null;
        }
    }
    /// <summary>
    /// 获取等级报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strUserName">代理ID</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportGroupData(string strIdentityId, string strStartDate, string strEndDate, int pageIndex, int pageSize)
    {
        DataSet dataSet = new DataSet();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "IdentityId=" + strIdentityId + " StartTime=" + strStartDate + " EndTime=" + strEndDate + " pageIndex=" + pageIndex + " pageSize=" + pageSize;
        try
        {
            st.Reset();
            st.Start();
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("strMsg_check", "GetReportGroupData", st.ElapsedMilliseconds.ToString(), strMsg);
            }
            if (!string.IsNullOrWhiteSpace(strMsg))
            {
                logmsg.CreateErrorLogTxt("strMsg_check", "xxx", st.ElapsedMilliseconds.ToString(), string.IsNullOrWhiteSpace(strMsg).ToString());
                //return dataSet;
            }
            logmsg.CreateErrorLogTxt("strMsg_check", "zzz", st.ElapsedMilliseconds.ToString(), string.IsNullOrWhiteSpace(strMsg).ToString());
            //lock (logmsg)
            //{
            //    logmsg.CreateErrorLogTxt("strMsg_check", "GetReportGroupData", st.ElapsedMilliseconds.ToString(), "true");
            //}
            DataTable table_lotterycode = new DataTable();
            DataTable table_lottery = new DataTable();
            DataTable table_lotteryname = new DataTable();
            DataTable table_bet = new DataTable();
            DataTable table_win = new DataTable();
            DataTable table_rebate = new DataTable();
            DataTable table_out = new DataTable();
            DataTable table_action = new DataTable();
            DataTable table_in = new DataTable();
            DataTable table_agent = new DataTable();
            DataTable table_misadmin = new DataTable();
            DataTable table_reg = new DataTable();
            DataTable table_newpay = new DataTable();
            DataTable table_user = new DataTable();
            int searchType = 0;
            if (Convert.ToDateTime(strStartDate).Date >= DateTime.Now.AddDays(-1).Date && Convert.ToDateTime(strEndDate).Date >= DateTime.Now.AddDays(-1).Date)
            {
                searchType = 1;
            }
            else if (Convert.ToDateTime(strStartDate).Date < DateTime.Now.AddDays(-1).Date && Convert.ToDateTime(strEndDate).Date < DateTime.Now.AddDays(-1).Date)
            {
                searchType = 2;
            }
            else
            {
                searchType = 3;
            }


            //connectstring
            string report_share = ReportHelper.Report_Share;
            string report_order_conn = DBcon.DBcontring.Report_Order_Conn;
            string report_in_conn = DBcon.DBcontring.Report_In_Conn;
            string report_out_conn = DBcon.DBcontring.Report_Out_Conn;
            string agent_order_conn = DBcon.DBcontring.Agent_Order_Conn;

            //報表
            List<SqlParameter> sqlcmdlist_identityid = new List<SqlParameter>();
            sqlcmdlist_identityid.Add(new SqlParameter("@identityid", strIdentityId));
            SqlParameter[] sqlcmdpar_identityid = sqlcmdlist_identityid.ToArray();

            //報表
            List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
            sqlcmdlist.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist.Add(new SqlParameter("@strStartDate", strStartDate));
            sqlcmdlist.Add(new SqlParameter("@strEndDate", strEndDate));
            SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();

            //線上
            List<SqlParameter> sqlcmdlist_online = new List<SqlParameter>();
            sqlcmdlist_online.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist_online.Add(new SqlParameter("@strStartDate", strStartDate));
            sqlcmdlist_online.Add(new SqlParameter("@strEndDate", Convert.ToDateTime(strEndDate).AddDays(1).ToString("yyyy-MM-dd")));
            SqlParameter[] sqlcmdpar_online = sqlcmdlist_online.ToArray();

            //字典
            Dictionary<string, string> dic_user = new Dictionary<string, string>();
            Dictionary<string, string> dic_group = new Dictionary<string, string>();

            //group 
            string group_str = "select Grade,grouptitle as group_name from [srv_lnk_agent_order].[dafacloud].[dbo].[dt_activity_grade] ";
            DataTable table_group = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(group_str, report_share, sqlcmdpar_identityid);


            //投注
            //string order_str = " select  identityid,group_id,sum(normal_money) as BetMoney  ,sum(betcount) as BetOrderNum,count(distinct(user_id)) as BetNum  from  [dbo].[dt_lottery_record]   left join (select dt_users.id,user_name,group_id from dt_users where identityid=@identityid and flog=0)dt_users on [dt_lottery_record].user_id = dt_users.id  where identityid=@identityid and add_date >=@strStartDate and add_date <=@strEndDate  group by identityid,group_id";
            string order_str = " select  identityid,group_id,sum(normal_money) as BetMoney  ,sum(betcount) as BetOrderNum,count(distinct(user_id)) as BetNum  from  [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record]   left join (select dt_users.id,user_name,group_id from [dbo].[dt_users] where identityid=@identityid and flog=0) as dt_users on [dt_lottery_record].user_id = dt_users.id  where identityid=@identityid and add_date >=@strStartDate and add_date <=@strEndDate  group by identityid,group_id";
            table_bet = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(order_str, report_share, sqlcmdpar);

            //提現
            //string cancel_str = "select identityid,group_id,  sum(money) as OutMoney from[dbo].[dt_user_out_record] left join (select dt_users.id,user_name,group_id from dt_users where identityid=@identityid and flog=0)dt_users on [dt_user_out_record].user_id = dt_users.id   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and type2 in(3,9,10) group by identityid,group_id";
            string cancel_str = "select identityid,group_id,  sum(money) as OutMoney from [srv_lnk_total_out].[dafacloud].[dbo].[dt_user_out_record] left join (select dt_users.id,user_name,group_id from [dbo].[dt_users] where identityid=@identityid and flog=0)dt_users on [dt_user_out_record].user_id = dt_users.id   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and type2 in(3,9,10) group by identityid,group_id";
            table_out = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(cancel_str, report_share, sqlcmdpar);

            //中獎
            string win_str;
            if (searchType == 1)
            {
                win_str = "select identityid,group_id, sum(money) as WinMoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord]  left join (select dt_users.id,user_name,group_id from [dbo].[dt_users] where identityid=@identityid and flog=0)dt_users on [dt_lottery_winning_userrecord].user_id = dt_users.id   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  group by identityid,group_id";
            }
            else if (searchType == 2)
            {
                win_str = "select identityid,group_id, sum(money) as WinMoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold]  left join (select dt_users.id,user_name,group_id from [dbo].[dt_users] where identityid=@identityid and flog=0)dt_users on [dt_lottery_winning_userrecord_cold].user_id = dt_users.id   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  group by identityid,group_id";
            }
            else
            {
                win_str = "select identityid,group_id, sum(WinMoney) as WinMoney from( select user_id,identityid, [money] as WinMoney,add_date,lotterycode,sourcename from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord]  with(nolock) where add_date>=@strStartDate and add_date<=@strEndDate and identityid = @identityid union select user_id,identityid, [money] as WinMoney,add_date,lotterycode,sourcename from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold] with(nolock) where add_date>=@strStartDate and add_date<=@strEndDate and identityid = @identityid) as all_userrecord left join (select dt_users.id,user_name,group_id from [dbo].[dt_users] where identityid=@identityid and flog=0)as dt_users on all_userrecord.user_id = dt_users.id where identityid = @identityid group by identityid,group_id";
            }
            //string win_str = "select identityid,group_id, sum(money) as WinMoney from [dbo].[dt_lottery_winning_userrecord]  left join (select dt_users.id,user_name,group_id from dt_users where identityid=@identityid and flog=0)dt_users on [dt_lottery_winning_userrecord].user_id = dt_users.id   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  group by identityid,group_id";
            table_win = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(win_str, report_share, sqlcmdpar);

            //返點
            string rebate_str = "select identityid,group_id,  sum(money) as Out_RebateAccount from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_get_point_userrecord]  left join (select dt_users.id,user_name,group_id from [dbo].[dt_users] where identityid=@identityid and flog=0)dt_users on [dt_user_get_point_userrecord].user_id = dt_users.id  where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  group by identityid,group_id";
            table_rebate = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(rebate_str, report_share, sqlcmdpar);

            //活動
            string action_str = "select identityid,group_id,  sum(money) as ActivityDiscountAccountMoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record]  left join (select dt_users.id,user_name,group_id from [dbo].[dt_users] where identityid=@identityid and flog=0)dt_users on [dt_user_in_record].user_id = dt_users.id  where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and type2 in (7,12)  group by identityid,group_id";
            table_action = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(action_str, report_share, sqlcmdpar);

            //充值
            string in_str = "select identityid,group_id, count(distinct(user_id)) as PrepaidNum , sum(money) as PrepaidMoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] left join (select dt_users.id,user_name,group_id from [dbo].[dt_users] where identityid=@identityid and flog=0)dt_users on [dt_user_in_record].user_id = dt_users.id   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and type =1 and state =1 group by identityid,group_id";
            table_in = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, sqlcmdpar);

            //代理
            string agent_str = "select identityid,group_id,  sum(money) as agentmoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record]  left join (select dt_users.id,user_name,group_id from [dbo].[dt_users] where identityid=@identityid and flog=0)dt_users on [dt_user_in_record].user_id = dt_users.id  where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and type2 in (13,14) and state =1 group by identityid,group_id";
            table_agent = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_str, report_share, sqlcmdpar);

            //拒絕+行政
            //string misadmin_str = "select identityid,group_id, sum(money) as out_money from[dbo].[dt_user_out_record] left join (select dt_users.id,user_name,group_id from dt_users where identityid=@identityid and flog=0)dt_users on [dt_user_out_record].user_id = dt_users.id   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and ((type2 =10 and state=1) or (type2=3 and state=3)) group by identityid,group_id";
            string misadmin_str = "select identityid,group_id, sum(money) as out_money from [srv_lnk_total_out].[dafacloud].[dbo].[dt_user_out_record] left join (	select dt_users.id,user_name,group_id from [dbo].[dt_users] where identityid=@identityid and flog=0) as dt_users on [dt_user_out_record].user_id = dt_users.id where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and ((type2 =10 and state=1) or (type2=3 and state=3)) group by identityid,group_id";
            table_misadmin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(misadmin_str, report_share, sqlcmdpar);

            //註冊人數
            string reg_str = "select identityid,group_id,count(distinct(userid)) as RegNum  from [dbo].[dt_users_register] left join (select dt_users.id,user_name,group_id from [dbo].[dt_users] where identityid=@identityid and flog=0)dt_users on [dt_users_register].userid = dt_users.id  where identityid = @identityid and addtime >= @strStartDate and addtime<= @strEndDate group by identityid,group_id";
            //table_reg = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(reg_str, agent_order_conn, sqlcmdpar_online);
            table_reg = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(reg_str, report_share, sqlcmdpar_online);

            //首充人數
            string newpay_str = "select identityid,dt_users.group_id,count(distinct(userid)) as NewPrepaidNum  from [dbo].[dt_user_newpay] left join (select dt_users.id,user_name,group_id from [dbo].[dt_users] where identityid=@identityid and flog=0)dt_users on [dt_user_newpay].userid = dt_users.id  where identityid = @identityid and addtime >= @strStartDate and addtime<= @strEndDate group by identityid,dt_users.group_id";
            //table_newpay = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(newpay_str, agent_order_conn, sqlcmdpar_online);
            table_newpay = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(newpay_str, report_share, sqlcmdpar_online);
            List<ReportGroup> listReportGroup = new List<ReportGroup>();
            ReportHelper reporthelper = new ReportHelper();
            for (int i = 0; i < table_group.Rows.Count; i++)
            {
                ReportGroup reportGroup = new ReportGroup();
                DataRow dr_group = table_group.Rows[i];
                reporthelper.ConvertDataRowToModel_sql(dr_group, reportGroup);
                string group = dr_group["Grade"].ToString().TrimEnd();

                decimal BetMoney = 0;
                decimal WinMoney = 0;
                decimal RebateMoney = 0;
                decimal ActivityDiscountAccountMoney = 0;
                decimal agentmoney = 0;
                decimal out_money = 0;
                //投注
                if (table_bet.Select("group_id = '" + group + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_bet.Select("group_id = '" + group + "'")[0], reportGroup);
                    BetMoney = reportGroup.BetMoney;
                }
                //中獎
                if (table_win.Select("group_id = '" + group + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_win.Select("group_id = '" + group + "'")[0], reportGroup);
                    WinMoney = reportGroup.WinMoney;
                }
                //返點
                if (table_rebate.Select("group_id = '" + group + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_rebate.Select("group_id = '" + group + "'")[0], reportGroup);
                    RebateMoney = reportGroup.Out_RebateAccount;
                }
                //提現
                if (table_out.Select("group_id = '" + group + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_out.Select("group_id = '" + group + "'")[0], reportGroup);
                }
                //充值
                if (table_in.Select("group_id = '" + group + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_in.Select("group_id = '" + group + "'")[0], reportGroup);
                }
                //活動
                if (table_action.Select("group_id = '" + group + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_action.Select("group_id = '" + group + "'")[0], reportGroup);
                    ActivityDiscountAccountMoney = reportGroup.ActivityDiscountAccountMoney;
                }
                //拒絕+行政
                if (table_misadmin.Select("group_id = '" + group + "'").Count() > 0)
                {
                    out_money = Convert.ToDecimal(table_misadmin.Select("group_id = '" + group + "'")[0]["out_money"]);

                }
                //代理
                if (table_agent.Select("group_id = '" + group + "'").Count() > 0)
                {
                    agentmoney = Convert.ToDecimal(table_misadmin.Select("group_id = '" + group + "'")[0]["agentmoney"]);
                }
                //註冊
                if (table_reg.Select("group_id = '" + group + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_reg.Select("group_id = '" + group + "'")[0], reportGroup);
                }
                //首充
                if (table_newpay.Select("group_id = '" + group + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_newpay.Select("group_id = '" + group + "'")[0], reportGroup);
                }

                //盈利＝投注-中奖-返点-活动-其他优惠-代理工资-代理分红+拒绝总额+行政提出
                decimal ProfitMoney = BetMoney - WinMoney - RebateMoney - ActivityDiscountAccountMoney - agentmoney + out_money;
                decimal ProfitRate = 0;
                if (BetMoney > 0)
                {
                    ProfitRate = ProfitMoney / BetMoney * 100;
                }

                reportGroup.ProfitLoss = ProfitMoney;
                reportGroup.WinRate = ProfitRate;
                listReportGroup.Add(reportGroup);
            }

            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            int totalCount = listReportGroup.Count;
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);


            Dictionary<string, ReportGroup> dicReportGroup = new Dictionary<string, ReportGroup>();

            DataTableHelper dataTableHelper = new DataTableHelper();
            DataTable dataTable = dataTableHelper.ConvertListToDataTable(listReportGroup);
            dataSet.Tables.Add(dataTable);
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("After_GetReportGroupData", "GetReportGroupData", st.ElapsedMilliseconds.ToString(), para);
            }
            return dataSet;
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("After_Error", "GetReportGroupData", st.ElapsedMilliseconds.ToString(), para);
            }
            return null;
        }
    }

    /// <summary>
    /// 获取彩种报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strLotteryCode">彩种编码</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="pageIndex">当前页号</param>
    /// <param name="pageSize">每一页多少条记录</param>
    /// <param name="strOrderBy">排序</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportLotteryData(string strIdentityId, string strLotteryCode, string strStartDate, string strEndDate, int pageIndex, int pageSize, string strOrderBy)
    {
        DataSet dataSet = new DataSet();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "IdentityId=" + strIdentityId + " LotteryCode=" + strLotteryCode + " StartTime=" + strStartDate + " EndTime=" + strEndDate + " pageIndex=" + pageIndex + " pageSize=" + pageSize + " strOrderBy=" + strOrderBy;
        try
        {
            st.Reset();
            st.Start();
            //验证是否有权访问  
            //string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return dataSet;
            //}
            DateTime dtStartTime = DateTime.Parse(strStartDate);
            DateTime dtEndTime = DateTime.Parse(strEndDate);
            string wherelottery = "";

            //DataTable table_lotterycode = new DataTable();
            //DataTable table_lottery = new DataTable();
            DataTable table_lotteryname = new DataTable();
            DataTable table_bet = new DataTable();
            DataTable table_win = new DataTable();
            DataTable table_rebate = new DataTable();
            DataTable table_cen = new DataTable();
            //DataTable table_betnum = new DataTable();
            int searchType = 0;//二日內表=1、二日外表=2、跨表=3
            if (Convert.ToDateTime(strStartDate).Date >= DateTime.Now.AddDays(-1).Date && Convert.ToDateTime(strEndDate).Date >= DateTime.Now.AddDays(-1).Date)
            {
                searchType = 1;
            }
            else if (Convert.ToDateTime(strStartDate).Date < DateTime.Now.AddDays(-1).Date && Convert.ToDateTime(strEndDate).Date < DateTime.Now.AddDays(-1).Date)
            {
                searchType = 2;
            }
            else
            {
                searchType = 3;
            }

            //connectstringn            
            string report_share = ReportHelper.Report_Share;
            string report_order_conn = DBcon.DBcontring.Report_Order_Conn;
            string report_in_conn = DBcon.DBcontring.Report_In_Conn;
            string report_out_conn = DBcon.DBcontring.Report_Out_Conn;


            //報表
            List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
            if (!String.IsNullOrEmpty(strIdentityId))
            {
                sqlcmdlist.Add(new SqlParameter("@identityid", strIdentityId));
            }
            sqlcmdlist.Add(new SqlParameter("@strStartDate", strStartDate));
            sqlcmdlist.Add(new SqlParameter("@strEndDate", strEndDate));
            if (!string.IsNullOrEmpty(strLotteryCode) && strLotteryCode != "0000")
            {
                sqlcmdlist.Add(new SqlParameter("@LotteryCode", strLotteryCode));
                wherelottery = " and lotterycode = @LotteryCode ";
            }
            SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();

            //字典
            Dictionary<string, string> dic_lottery = new Dictionary<string, string>();

            ReportHelper reporthelper = new ReportHelper();
            List<ReportLottery> listLottery = new List<ReportLottery>();

            string lotteryname_str = "select lottery_code ,lottery_name from [srv_lnk_total_out].[dafacloud].[dbo].[dt_lottery_explain]";
            table_lotteryname = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(lotteryname_str, report_share, sqlcmdpar);
            for (int i = 0; i < table_lotteryname.Rows.Count; i++)
            {
                string lotterycode = table_lotteryname.Rows[i]["lottery_code"].ToString();
                string lotteryname = table_lotteryname.Rows[i]["lottery_name"].ToString();
                dic_lottery.Add(lotterycode, lotteryname);
            }



            //投注金額
            string identityidColumn = null;
            string allIdentityidConstraint = null;
            string order_str = null;
            if (String.IsNullOrEmpty(strIdentityId) || strIdentityId == "")
            {
                identityidColumn = " ";
                allIdentityidConstraint = " identityid<>'test' and ";
            }
            else
            {
                identityidColumn = " identityid, ";
                allIdentityidConstraint = " identityid=@identityid and ";
            }
            order_str = "select " + identityidColumn + " lotterycode,sum(normal_money) as BetMoney,convert(decimal,count(distinct(user_id))) as BetNum  from  [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record]   where " + allIdentityidConstraint + " add_date >=@strStartDate and add_date <=@strEndDate  " + wherelottery + " group by " + identityidColumn + " lotterycode";
            table_bet = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(order_str, report_share, sqlcmdpar);
            // 中獎人數
            string win_str;
            if (searchType == 1)
            {
                win_str = "select " + identityidColumn + " lotterycode,sum(money) as WinMoney  from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord] where " + allIdentityidConstraint + " add_date >=@strStartDate and add_date <=@strEndDate " + wherelottery + "  group by  " + identityidColumn + " lotterycode";
            }
            else if (searchType == 2)
            {
                win_str = "select " + identityidColumn + " lotterycode,sum(money) as WinMoney  from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold] where  " + allIdentityidConstraint + "  add_date >=@strStartDate and add_date <=@strEndDate " + wherelottery + "  group by  " + identityidColumn + " lotterycode";
            }
            else
            {
                win_str = "select " + identityidColumn + " lotterycode,sum(WinMoney) as WinMoney from( select  user_id, " + identityidColumn + " lotterycode,[money] as WinMoney,add_date,sourcename  from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord] where  " + allIdentityidConstraint + " add_date >=@strStartDate and add_date <=@strEndDate " + wherelottery + " union select  user_id, " + identityidColumn + " lotterycode,[money] as WinMoney,add_date,sourcename  from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold] where  " + allIdentityidConstraint + " add_date >=@strStartDate and add_date <=@strEndDate " + wherelottery + " ) as all_userrecord group by  " + identityidColumn + " lotterycode";
            }

            table_win = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(win_str, report_share, sqlcmdpar);

            //返點人數
            string rebate_str = "select " + identityidColumn + " lotterycode,sum(money) as RebateMoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_get_point_userrecord] where  " + allIdentityidConstraint + " add_date >=@strStartDate and add_date <=@strEndDate " + wherelottery + "  group by  " + identityidColumn + " lotterycode";
            table_rebate = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(rebate_str, report_share, sqlcmdpar);

            //撤單
            string cancel_str = "select " + identityidColumn + " lotterycode,sum(money) as CancelMoney from [srv_lnk_total_out].[dafacloud].[dbo].[dt_lottery_cancellationsflow_record] where  " + allIdentityidConstraint + " add_date >=@strStartDate and add_date <=@strEndDate " + wherelottery + "  group by  " + identityidColumn + " lotterycode";
            table_cen = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(cancel_str, report_share, sqlcmdpar);

            for (int i = 0; i < table_bet.Rows.Count; i++)
            {
                ReportLottery reportlottery = new ReportLottery();
                DataRow dr_lotterycode = table_bet.Rows[i];
                string lotterycode_str = dr_lotterycode["lotterycode"].ToString().TrimEnd();
                reporthelper.ConvertDataRowToModel(dr_lotterycode, reportlottery);
                decimal BetMoney = Convert.ToDecimal(dr_lotterycode["BetMoney"]);
                decimal WinMoney = 0;
                decimal RebateMoney = 0;
                decimal CancelMoney = 0;
                //decimal BetNum = 0;
                if (table_win.Select("lotterycode = '" + lotterycode_str + "'").Count() > 0)
                {
                    WinMoney = Convert.ToDecimal(table_win.Select("lotterycode = '" + lotterycode_str + "'")[0]["WinMoney"]);
                    // BetNum = Convert.ToDecimal(table_betnum.Select("lotterycode = '" + lotterycode_str + "'")[0]["BetNum"]);
                }
                if (table_rebate.Select("lotterycode = '" + lotterycode_str + "'").Count() > 0)
                {
                    RebateMoney = Convert.ToDecimal(table_rebate.Select("lotterycode = '" + lotterycode_str + "'")[0]["RebateMoney"]);
                }
                if (table_cen.Select("lotterycode = '" + lotterycode_str + "'").Count() > 0)
                {
                    CancelMoney = Convert.ToDecimal(table_cen.Select("lotterycode = '" + lotterycode_str + "'")[0]["CancelMoney"]);
                }

                decimal ProfitMoney = BetMoney - WinMoney - RebateMoney;
                decimal ProfitRate = ProfitMoney / BetMoney * 100;
                reportlottery.LotteryName = dic_lottery[lotterycode_str].ToString();
                reportlottery.WinMoney = WinMoney;
                reportlottery.RebateMoney = RebateMoney;
                reportlottery.CancelMoney = CancelMoney;
                reportlottery.ProfitMoney = ProfitMoney;
                reportlottery.ProfitRate = ProfitRate;
                listLottery.Add(reportlottery);
            }


            // }

            ////盈利总额＝投注-中奖－返点

            ////盈率=盈利总额/投注总额*100

            int totalCount = listLottery.Count;
            DataTableHelper dataTableHelper = new DataTableHelper();
            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);
            if (string.IsNullOrWhiteSpace(strOrderBy))
            {
                strOrderBy = "betting_money desc";
            }
            //投注递减
            if (strOrderBy == "betting_money desc")
            {
                listLottery = listLottery.OrderByDescending(m => m.BetMoney).ToList();
            }
            //盈利递减
            if (strOrderBy == "profits_money desc")
            {
                listLottery = listLottery.OrderByDescending(m => m.ProfitMoney).ToList();
            }
            //盈利递增
            if (strOrderBy == "profits_money asc")
            {
                listLottery = listLottery.OrderBy(m => m.ProfitMoney).ToList();
            }
            //盈率递减
            if (strOrderBy == "profits_rates desc")
            {
                listLottery = listLottery.OrderByDescending(m => m.ProfitRate).ToList();
            }
            //盈率递增
            if (strOrderBy == "profits_rates asc")
            {
                listLottery = listLottery.OrderBy(m => m.ProfitRate).ToList();
            }

            // listLottery = listLottery.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            DataTable dataTable = dataTableHelper.ConvertListToDataTable(listLottery);
            dataSet.Tables.Add(dataTable);
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("After_GetReportLotteryData", "GetReportLotteryData", st.ElapsedMilliseconds.ToString(), para);
            }
            return dataSet;
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("After_Error", "GetReportLotteryData", st.ElapsedMilliseconds.ToString(), para);
            }
            return null;
        }
    }

    /// <summary>
    /// 获取会员报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strUserName">会员名称</param>
    /// <param name="strAgentName">代理名称</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="pageIndex">当前页号</param>
    /// <param name="pageSize">每一页多少条记录</param>
    /// <param name="strOrderBy">排序</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportMemberData(string strIdentityId, string strUserName, string strAgentName, string strStartDate, string strEndDate, int pageIndex, int pageSize, string strOrderBy)
    {
        DataSet dataSet = new DataSet();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "IdentityId=" + strIdentityId + " UserName=" + strUserName + " AgentName=" + strAgentName + " StartTime=" + strStartDate + " EndTime=" + strEndDate + " pageIndex=" + pageIndex + " pageSize=" + pageSize + " strOrderBy=" + strOrderBy;
        try
        {
            st.Reset();
            st.Start();
            //验证是否有权访问  
            //string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return dataSet;
            //}
            DataTable table_lotterycode = new DataTable();
            DataTable table_lottery = new DataTable();
            DataTable table_lotteryname = new DataTable();
            DataTable table_bet = new DataTable();
            DataTable table_win = new DataTable();
            DataTable table_rebate = new DataTable();
            DataTable table_reward = new DataTable();
            DataTable table_out = new DataTable();
            DataTable table_action = new DataTable();
            DataTable table_in = new DataTable();
            DataTable table_agent = new DataTable();
            DataTable table_misadmin = new DataTable();
            DataTable table_reg = new DataTable();
            DataTable table_newpay = new DataTable();
            DataTable table_user = new DataTable();
            int searchType = 0;
            if (Convert.ToDateTime(strStartDate).Date >= DateTime.Now.AddDays(-1).Date && Convert.ToDateTime(strEndDate).Date >= DateTime.Now.AddDays(-1).Date)
            {
                searchType = 1;
            }
            else if (Convert.ToDateTime(strStartDate).Date < DateTime.Now.AddDays(-1).Date && Convert.ToDateTime(strEndDate).Date < DateTime.Now.AddDays(-1).Date)
            {
                searchType = 2;
            }
            else
            {
                searchType = 3;
            }


            //connectstring
            string report_share = ReportHelper.Report_Share;
            string report_order_conn = DBcon.DBcontring.Report_Order_Conn;
            string report_in_conn = DBcon.DBcontring.Report_In_Conn;
            string report_out_conn = DBcon.DBcontring.Report_Out_Conn;
            string agent_order_conn = DBcon.DBcontring.Agent_Order_Conn;

            //報表
            List<SqlParameter> sqlcmdlist_identityid = new List<SqlParameter>();
            sqlcmdlist_identityid.Add(new SqlParameter("@identityid", strIdentityId));
            SqlParameter[] sqlcmdpar_identityid = sqlcmdlist_identityid.ToArray();

            //使用者
            List<SqlParameter> sqlcmdlist_user = new List<SqlParameter>();
            sqlcmdlist_user.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist_user.Add(new SqlParameter("@user_name", strUserName));
            SqlParameter[] sqlcmdpar_user = sqlcmdlist_user.ToArray();

            //報表
            List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
            sqlcmdlist.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist.Add(new SqlParameter("@strStartDate", strStartDate));
            sqlcmdlist.Add(new SqlParameter("@strEndDate", strEndDate));
            SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();


            if (strOrderBy == "BetMoney desc" || string.IsNullOrWhiteSpace(strOrderBy))
            {
                strOrderBy = "BetMoney desc";
            }

            string where_str = "";
            string strUserId = string.Empty;
            if (!string.IsNullOrWhiteSpace(strUserName))
            {
                string sql_str = "select id from [dbo].[dt_users] where identityid=@identityid and user_name=@user_name";
                DataTable userData = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(sql_str, report_share, sqlcmdpar_user);
                if (userData != null && userData.Rows.Count > 0)
                {
                    strUserId = userData.Rows[0]["id"].ToString();
                }
                else
                {
                    strUserId = "-1";
                }
                where_str += " and user_id = " + strUserId;
            }
            //上级代理Id
            string strAgentId = string.Empty;
            DataTable agentDataTable = null;
            if (!string.IsNullOrWhiteSpace(strAgentName))
            {
                string sql_str = "select father_id,user_id from dt_users_class where identityid='" + strIdentityId + "' and father_id in (select id from dt_users where user_name='" + strAgentName + "')";
                agentDataTable = DBcon.DbHelperSQL.GetQueryFromCongku(sql_str, null);

                string agent_user_in = "";
                for (int r = 0; r < agentDataTable.Rows.Count; r++)
                {
                    agent_user_in += agentDataTable.Rows[r]["user_id"] + ",";
                }
                if (agent_user_in.Length > 0)
                {
                    agent_user_in = agent_user_in.Substring(0, agent_user_in.Length - 1);

                }
                else
                {
                    agent_user_in = "0";
                }
                where_str += " and user_id in (" + agent_user_in + ")";
            }

            //user_id
            //string user_str = "  select view_user.id,view_user.user_name,grouptitle,view_agent.user_name from (select id,identityid,user_name,group_id,agent_id from dt_users where identityid = @identityid) as view_user    left join dt_activity_grade on view_user.group_id = dt_activity_grade.Grade    left join(select id, user_name from dt_users where identityid = @identityid) as view_agent on view_user.agent_id = view_agent.id    where view_user.identityid = @identityid";
            // table_user = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(user_str, agent_order_conn, sqlcmdpar_identityid);

            //投注
            string order_str = "select identityid, user_id as UserId, sum(normal_money) as BetMoney from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record]    where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate   " + where_str + "  group by identityid,user_id";
            table_bet = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(order_str, report_share, sqlcmdpar);

            //提現
            string cancel_str = "select identityid, user_id as UserId, sum(money) as OutMoney from [srv_lnk_total_out].[dafacloud].[dbo].[dt_user_out_record]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and type = 3  and state=1 " + where_str + " group by identityid,user_id";
            table_out = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(cancel_str, report_share, sqlcmdpar);

            //中獎
            string win_str;
            if (searchType == 1)
            {
                win_str = "select identityid, user_id as UserId, sum(money) as WinMoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord]    where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate   " + where_str + " group by identityid,user_id";
            }
            else if (searchType == 2)
            {
                win_str = "select identityid, user_id as UserId, sum(money) as WinMoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold]    where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate   " + where_str + " group by identityid,user_id";
            }
            else
            {
                win_str = "select identityid, UserId,sum(WinMoney) as WinMoney from( select user_id as UserId,identityid,lotterycode,[money] as WinMoney,add_date,sourcename from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord] where identityid = @identityid  and add_date >=@strStartDate and add_date <=@strEndDate " + where_str + " union     select  user_id as UserId,identityid,lotterycode,[money] as WinMoney,add_date,sourcename from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold] where identityid = @identityid  and add_date >=@strStartDate and add_date <=@strEndDate  " + where_str + " ) as all_userrecord group by identityid, UserId ";
            }
            //win_str = "select identityid, user_id as UserId, sum(money) as WinMoney from [dbo].[dt_lottery_winning_userrecord]    where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate   " + where_str + " group by identityid,user_id";
            table_win = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(win_str, report_share, sqlcmdpar);

            //返點
            string rebate_str = "select identityid, user_id as UserId, sum(money) as RebateMoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_get_point_userrecord]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate   " + where_str + " group by identityid,user_id";
            table_rebate = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(rebate_str, report_share, sqlcmdpar);

            //打賞
            string reward_str = "select identityid,  cast(user_id as int) as UserId, sum(rewardmoney) as RewardMoney from [srv_lnk_total_out].[dafacloud].[dbo].[dt_reward_userrecord]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate   " + where_str + " group by identityid,user_id";
            table_reward = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(reward_str, report_share, sqlcmdpar);

            //活動
            string action_str = "select identityid, user_id as UserId, sum(money) as DiscountMoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and type2 in (7,12)   " + where_str + " group by identityid,user_id";
            table_action = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(action_str, report_share, sqlcmdpar);

            //充值
            string in_str = "select identityid, user_id as UserId, sum(money) as InMoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and type =1 and state =1  " + where_str + "  group by identityid,user_id";
            table_in = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, sqlcmdpar);

            ////代理
            //string agent_str = "select identityid,user_id as UserId, sum(money) as agentmoney from[dbo].[dt_user_in_record]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and type2 in (13,14) and state =1  " + where_str + "  group by identityid,user_id";
            //table_agent = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_str, report_in_conn, sqlcmdpar);

            ////拒絕+行政
            //string misadmin_str = "select identityid,user_id as UserId, sum(money) as out_money from[dbo].[dt_user_out_record]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and ((type2 =10 and state=1) or (type2=3 and state=3)) " + where_str + "  group by identityid,user_id";
            //table_misadmin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(misadmin_str, report_out_conn, sqlcmdpar);



            DataTable table_member = table_bet.DefaultView.ToTable(true, "UserId");
            table_member.Merge(table_out.DefaultView.ToTable(true, "UserId"));
            table_member.Merge(table_win.DefaultView.ToTable(true, "UserId"));
            table_member.Merge(table_rebate.DefaultView.ToTable(true, "UserId"));
            table_member.Merge(table_reward.DefaultView.ToTable(true, "UserId"));
            table_member.Merge(table_action.DefaultView.ToTable(true, "UserId"));
            table_member.Merge(table_in.DefaultView.ToTable(true, "UserId"));
            //table_member.Merge(table_agent.DefaultView.ToTable(true, "UserId"));
            //table_member.Merge(table_misadmin.DefaultView.ToTable(true, "UserId"));
            table_member = table_member.DefaultView.ToTable(true, "UserId");

            string user_in = "";
            string capital_in = "";
            for (int i = 0; i < table_member.Rows.Count; i++)
            {
                user_in += table_member.Rows[i]["UserId"].ToString() + ",";
            }
            if (user_in.Length > 0)
            {
                user_in = user_in.Substring(0, user_in.Length - 1);
                capital_in = " and user_id in (" + user_in + ")  ";
                user_in = " and id in (" + user_in + ")  ";

            }
            else
            {
                capital_in = " and user_id in (0)  ";
                user_in = " and id in (0)  ";
            }
            string user_str = "select view_user.id as UserId,view_user.user_name as UserName,grouptitle as GroupName,view_agent.user_name as AgentName from (select id,identityid,user_name,group_id,agent_id from [dbo].[dt_users] where identityid = @identityid " + user_in + " ) as view_user    left join [srv_lnk_total_in].[dafacloud].[dbo].[dt_activity_grade] on view_user.group_id = dt_activity_grade.Grade    left join(select id, user_name from [dbo].[dt_users] where identityid = @identityid) as view_agent on view_user.agent_id = view_agent.id    where view_user.identityid = @identityid";
            table_user = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(user_str, report_share, sqlcmdpar_identityid);

            //會員餘額
            string capital_str = "select user_id as UserId,use_money from dt_user_capital where   flog=0 and identityid = @identityid  " + capital_in + "";
            DataTable table_capital = DBcon.DbHelperSQL.GetQueryFromCongku(capital_str, sqlcmdpar_identityid);


            List<ReportMember> listReportMember = new List<ReportMember>();
            ReportHelper reporthelper = new ReportHelper();
            for (int i = 0; i < table_member.Rows.Count; i++)
            {
                ReportMember reportMember = new ReportMember();
                DataRow dr_member = table_member.Rows[i];
                reporthelper.ConvertDataRowToModel_sql(dr_member, reportMember);
                string user = dr_member["UserId"].ToString().TrimEnd();

                decimal BetMoney = 0;
                decimal WinMoney = 0;
                decimal RebateMoney = 0;
                decimal RewardMoney = 0;
                decimal ActivityDiscountAccountMoney = 0;
                decimal agentmoney = 0;
                decimal out_money = 0;
                decimal Balance = 0;

                //投注
                if (table_bet.Select("UserId = '" + user + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_bet.Select("UserId = '" + user + "'")[0], reportMember);
                    BetMoney = reportMember.BetMoney;
                }
                //中獎
                if (table_win.Select("UserId = '" + user + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_win.Select("UserId = '" + user + "'")[0], reportMember);
                    WinMoney = reportMember.WinMoney;
                }
                //返點
                if (table_rebate.Select("UserId = '" + user + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_rebate.Select("UserId = '" + user + "'")[0], reportMember);
                    RebateMoney = reportMember.RebateMoney;
                }
                //打賞
                if (table_reward.Select("UserId = '" + user + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_reward.Select("UserId = '" + user + "'")[0], reportMember);
                    RewardMoney = reportMember.RewardMoney;
                }
                //提現
                if (table_out.Select("UserId = '" + user + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_out.Select("UserId = '" + user + "'")[0], reportMember);
                }
                //充值
                if (table_in.Select("UserId = '" + user + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_in.Select("UserId = '" + user + "'")[0], reportMember);
                }
                //活動
                if (table_action.Select("UserId = '" + user + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_action.Select("UserId = '" + user + "'")[0], reportMember);
                    ActivityDiscountAccountMoney = reportMember.DiscountMoney;
                }
                ////拒絕+行政
                //if (table_misadmin.Select("UserId = '" + user + "'").Count() > 0)
                //{
                //    out_money = Convert.ToDecimal(table_misadmin.Select("UserId = '" + user + "'")[0]["out_money"]);

                //}
                ////代理
                //if (table_agent.Select("UserId = '" + user + "'").Count() > 0)
                //{
                //    agentmoney = Convert.ToDecimal(table_misadmin.Select("UserId = '" + user + "'")[0]["agentmoney"]);
                //}
                //會員餘額
                if (table_capital.Select("UserId = '" + user + "'").Count() > 0)
                {
                    Balance = Convert.ToDecimal(table_capital.Select("UserId = '" + user + "'")[0]["use_money"]);
                }
                //會員資訊
                if (table_user.Select("UserId = '" + user + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_user.Select("UserId = '" + user + "'")[0], reportMember);
                }

                //盈利＝中奖-投注+返点+活动+其他优惠+代理工资+代理分红-拒绝总额-行政提出
                decimal ProfitMoney = WinMoney - BetMoney + RebateMoney + ActivityDiscountAccountMoney;//+ agentmoney - out_money;
                decimal ProfitRate = 0;
                if (BetMoney > 0)
                {
                    ProfitRate = ProfitMoney / BetMoney * 100;
                }
                reportMember.Balance = Balance;
                reportMember.ProfitLoss = ProfitMoney;
                reportMember.ProfitRate = ProfitRate;
                listReportMember.Add(reportMember);
            }

            //排序
            //投注递减
            if (strOrderBy == "BetMoney desc" || string.IsNullOrWhiteSpace(strOrderBy))
            {
                listReportMember = listReportMember.OrderByDescending(m => m.BetMoney).ToList();
            }
            //盈利递减
            if (strOrderBy == "ProfitLoss desc")
            {
                listReportMember = listReportMember.OrderByDescending(m => m.ProfitLoss).ToList();
            }
            //盈利递增
            if (strOrderBy == "ProfitLoss asc")
            {
                listReportMember = listReportMember.OrderBy(m => m.ProfitLoss).ToList();
            }
            //盈率递减
            if (strOrderBy == "ProfitRate desc")
            {
                listReportMember = listReportMember.OrderByDescending(m => m.ProfitRate).ToList();
            }
            //盈率递增
            if (strOrderBy == "ProfitRate asc")
            {
                listReportMember = listReportMember.OrderBy(m => m.ProfitRate).ToList();
            }
            //入款递减
            if (strOrderBy == "InMoney desc")
            {
                listReportMember = listReportMember.OrderByDescending(m => m.InMoney).ToList();
            }
            //出款递减
            if (strOrderBy == "OutMoney desc")
            {
                listReportMember = listReportMember.OrderByDescending(m => m.OutMoney).ToList();
            }
            //返点递减
            if (strOrderBy == "RebateMoney desc")
            {
                listReportMember = listReportMember.OrderByDescending(m => m.RebateMoney).ToList();
            }
            //活动礼金递减
            if (strOrderBy == "DiscountMoney desc")
            {
                listReportMember = listReportMember.OrderByDescending(m => m.DiscountMoney).ToList();
            }


            int totalCount = listReportMember.Count;
            DataTableHelper dataTableHelper = new DataTableHelper();
            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);
            listReportMember = listReportMember.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            DataTable dataTable = dataTableHelper.ConvertListToDataTable(listReportMember);
            dataSet.Tables.Add(dataTable);
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("After_GetReportMemberData", "GetReportMemberData", st.ElapsedMilliseconds.ToString(), para);
            }
            return dataSet;
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("After_Error", "GetReportMemberData", st.ElapsedMilliseconds.ToString(), para);
            }
            return null;
        }
    }
    /// <summary>
    /// 获取新充报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strUserName">会员名称</param>
    /// <param name="strAgentName">代理名称</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="pageIndex">当前页号</param>
    /// <param name="pageSize">每一页多少条记录</param>
    /// <param name="strOrderBy">排序</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportNewPrepaidData(string strIdentityId, string strUserName, string strAgentName, string strStartDate, string strEndDate, int pageIndex, int pageSize, string strOrderBy)
    {
        DataSet dataSet = new DataSet();
        try
        {
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return dataSet;
            //}
            //会员账号

            DateTime dtStartTime = DateTime.Parse(strStartDate);
            DateTime dtEndTime = DateTime.Parse(strEndDate);
            // string wherelottery = "";

            DataTable table_newpay = new DataTable();
            DataTable table_userid = new DataTable();

            string where_str = "";
            string order_str = "";

            //connectstring
            string report_share = ReportHelper.Report_Share;
            string agent_order_conn = DBcon.DBcontring.Agent_Order_Conn;
            string report_in_conn = DBcon.DBcontring.Report_In_Conn;
            string report_out_conn = DBcon.DBcontring.Report_Out_Conn;


            ReportHelper reporthelper = new ReportHelper();

            //報表
            List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
            sqlcmdlist.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist.Add(new SqlParameter("@strStartDate", strStartDate));
            sqlcmdlist.Add(new SqlParameter("@strEndDate", Convert.ToDateTime(strEndDate).AddDays(1).ToString("yyyy-MM-dd")));



            //判斷是否有代理或玩家
            if (!string.IsNullOrWhiteSpace(strUserName))
            {
                where_str += " and a.username = @username ";
                sqlcmdlist.Add(new SqlParameter("@username", strUserName));
            }
            //代理名称
            string strAgentId = string.Empty;
            if (!string.IsNullOrWhiteSpace(strAgentName))
            {
                where_str += " and c.user_name  = @agentname ";
                sqlcmdlist.Add(new SqlParameter("@agentname", strAgentName));
            }
            SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();

            //判斷是否有排序
            if (string.IsNullOrWhiteSpace(strOrderBy))
            {
                strOrderBy = " a.addtime desc";
                order_str = " order by " + strOrderBy;
            }
            else if (strOrderBy == "add_time desc")
            {
                strOrderBy = " a.addtime desc";
                order_str = " order by " + strOrderBy;
            }
            else //if (strOrderBy == "money desc")
            {
                order_str = " order by " + strOrderBy;
            }

            string newpay_str = " select  a.userid,a.username as user_name,GroupTitle as title,c.user_name as agent_name,a.money, a.commt as commt,a.SourceName,convert(varchar(19),a.addtime,121) as cztime,convert(varchar(19),regtime,121) as RegTime from(select  identityid, userid, username, money, commt, sourcename, addtime, agent_id, group_id, regtime  from [dbo].[dt_user_newpay]  where identityid = @identityid and addtime >= @strStartDate and addtime <= @strEndDate) a  left join [srv_lnk_total_in].[dafacloud].[dbo].[dt_activity_grade] b on a.group_id = b.Grade  left join(select id, user_name from [dbo].[dt_users] where identityid= @identityid) c on a.agent_id = c.id  where a.identityid = @identityid  and a.addtime >= @strStartDate and a.addtime <= @strEndDate  " + where_str + "  " + order_str;
            table_newpay = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(newpay_str, report_share, sqlcmdpar);
            int dt_count = table_newpay.Rows.Count;
            string capital_user = "";
            for (int i = 0; i < dt_count; i++)
            {
                capital_user += table_newpay.Rows[i]["userid"] + ",";
            }
            if (capital_user.Length > 0)
            {
                capital_user = capital_user.Substring(0, capital_user.Length - 1);
                capital_user = " and user_id in (" + capital_user + ") ";
            }
            else
            {
                capital_user = " and user_id in (0) ";
            }
            string capital_str = "select user_id,use_money from dt_user_capital where identityid = @identityid " + capital_user + " ";
            table_userid = DBcon.DbHelperSQL.GetQueryFromCongku(capital_str, sqlcmdpar);
            DataColumn workCol = table_newpay.Columns.Add("use_money", typeof(Decimal));
            for (int r = 0; r < dt_count; r++)
            {
                string user_id = table_newpay.Rows[r]["userid"].ToString();
                decimal use_money = 0;
                if (table_userid.Select("user_id ='" + user_id + "'").Count() > 0)
                {
                    use_money = Convert.ToDecimal(table_userid.Select("user_id ='" + user_id + "'")[0]["use_money"]);
                }
                table_newpay.Rows[r]["use_money"] = use_money;
            }

            int totalCount = dt_count;
            table_newpay = reporthelper.GetPagedTable(table_newpay, pageIndex, pageSize);


            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);



            dataSet.Tables.Add(table_newpay.Copy());
            return dataSet;
        }
        catch (Exception ex)
        {
            Log.Error(ex);
            return null;
        }
    }
    /// <summary>
    /// 获取终端报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportSourceData(string strIdentityId, string strStartDate, string strEndDate, int pageIndex, int pageSize)
    {
        DataSet dataSet = new DataSet();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "IdentityId=" + strIdentityId + " StartTime=" + strStartDate + " EndTime=" + strEndDate + " pageIndex=" + pageIndex + " pageSize=" + pageSize;
        try
        {
            st.Reset();
            st.Start();
            //验证是否有权访问  
            //string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return dataSet;
            //}
            strStartDate = strStartDate.Substring(0, 10);
            strEndDate = strEndDate.Substring(0, 10);

            DataTable table_lotterycode = new DataTable();
            DataTable table_lottery = new DataTable();
            DataTable table_lotteryname = new DataTable();
            DataTable table_bet = new DataTable();
            DataTable table_win = new DataTable();
            DataTable table_rebate = new DataTable();
            DataTable table_out = new DataTable();
            DataTable table_action = new DataTable();
            DataTable table_in = new DataTable();
            DataTable table_agent = new DataTable();
            DataTable table_misadmin = new DataTable();
            DataTable table_reg = new DataTable();
            DataTable table_newpay = new DataTable();
            int searchType = 0;
            if (Convert.ToDateTime(strStartDate).Date >= DateTime.Now.AddDays(-1).Date && Convert.ToDateTime(strEndDate).Date >= DateTime.Now.AddDays(-1).Date)
            {
                searchType = 1;
            }
            else if (Convert.ToDateTime(strStartDate).Date < DateTime.Now.AddDays(-1).Date && Convert.ToDateTime(strEndDate).Date < DateTime.Now.AddDays(-1).Date)
            {
                searchType = 2;
            }
            else
            {
                searchType = 3;
            }


            //connectstring
            string report_share = ReportHelper.Report_Share;
            string report_order_conn = DBcon.DBcontring.Report_Order_Conn;
            string report_in_conn = DBcon.DBcontring.Report_In_Conn;
            string report_out_conn = DBcon.DBcontring.Report_Out_Conn;
            string agent_order_conn = DBcon.DBcontring.Agent_Order_Conn;

            //報表
            List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
            sqlcmdlist.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist.Add(new SqlParameter("@strStartDate", strStartDate));
            sqlcmdlist.Add(new SqlParameter("@strEndDate", strEndDate));
            SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();

            //線上
            List<SqlParameter> sqlcmdlist_online = new List<SqlParameter>();
            sqlcmdlist_online.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist_online.Add(new SqlParameter("@strStartDate", strStartDate));
            sqlcmdlist_online.Add(new SqlParameter("@strEndDate", Convert.ToDateTime(strEndDate).AddDays(1).ToString("yyyy-MM-dd")));
            SqlParameter[] sqlcmdpar_online = sqlcmdlist_online.ToArray();

            //字典
            Dictionary<string, string> dic_lottery = new Dictionary<string, string>();

            ReportHelper reporthelper = new ReportHelper();
            List<ReportSource> listReportSource = new List<ReportSource>();



            //投注
            string order_str = "select identityid, sourcename as group_name,count(distinct(user_id)) as BetNum, sum(normal_money) as BetMoney, sum(betcount) as BetOrderNum from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record]    where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  group by identityid,sourcename";
            table_bet = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(order_str, report_share, sqlcmdpar);

            //提現
            string cancel_str = "select identityid, sourcename as group_name, sum(money) as OutMoney from [srv_lnk_total_out].[dafacloud].[dbo].[dt_user_out_record]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and type2 in(3,9,10) group by identityid,sourcename";
            table_out = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(cancel_str, report_share, sqlcmdpar);

            //中獎
            string win_str;
            if (searchType == 1)
            {
                win_str = "select identityid, sourcename as group_name, sum(money) as WinMoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord]    where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  group by identityid,sourcename";
            }
            else if (searchType == 2)
            {
                win_str = "select identityid, sourcename as group_name, sum(money) as WinMoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold]    where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  group by identityid,sourcename";
            }
            else
            {
                win_str = "select identityid,sourcename as group_name,sum([money]) as WinMoney from( select identityid,USER_ID, sourcename,lotterycode,add_date, [money] from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord] where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate union select identityid,USER_ID, sourcename,lotterycode,add_date, [money] from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold] where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate ) as all_userrecord group by identityid,sourcename ";
            }
            //string win_str = "select identityid, sourcename as group_name, sum(money) as WinMoney from [dbo].[dt_lottery_winning_userrecord]    where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  group by identityid,sourcename";
            table_win = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(win_str, report_share, sqlcmdpar);

            //返點
            string rebate_str = "select identityid, sourcename as group_name, sum(money) as Out_RebateAccount from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_get_point_userrecord]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  group by identityid,sourcename";
            table_rebate = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(rebate_str, report_share, sqlcmdpar);

            //活動
            string action_str = "select identityid, sourcename as group_name, sum(money) as ActivityDiscountAccountMoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and type2 in (7,12)  group by identityid,sourcename";
            table_action = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(action_str, report_share, sqlcmdpar);

            //充值
            string in_str = "select identityid, sourcename as group_name,count(distinct(user_id)) as PrepaidNum , sum(money) as PrepaidMoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and type =1 and state =1 group by identityid,sourcename";
            table_in = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, sqlcmdpar);

            //代理
            string agent_str = "select identityid, sourcename as group_name, sum(money) as agentmoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and type2 in (13,14) and state =1 group by identityid,sourcename";
            table_agent = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_str, report_share, sqlcmdpar);

            //拒絕+行政
            string misadmin_str = "select identityid, sourcename as group_name, sum(money) as out_money from [srv_lnk_total_out].[dafacloud].[dbo].[dt_user_out_record]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and ((type2 =10 and state=1) or (type2=3 and state=3)) group by identityid,sourcename";
            table_misadmin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(misadmin_str, report_share, sqlcmdpar);

            //註冊人數
            string reg_str = "select identityid,sourcename as group_name,count(distinct(userid)) as RegNum  from [dbo].[dt_users_register] where identityid = @identityid and addtime >= @strStartDate and addtime<= @strEndDate and Tester =0 group by identityid,sourcename";
            table_reg = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(reg_str, report_share, sqlcmdpar_online);

            //首充人數
            string newpay_str = "select identityid,sourcename as group_name,count(distinct(userid)) as NewPrepaidNum  from [dbo].[dt_user_newpay] where identityid = @identityid and addtime >= @strStartDate and addtime<= @strEndDate  group by identityid,sourcename";
            table_newpay = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(newpay_str, report_share, sqlcmdpar_online);

            DataTable table_sourcename = table_bet.DefaultView.ToTable(true, "group_name");
            table_sourcename.Merge(table_in.DefaultView.ToTable(true, "group_name"));
            table_sourcename.Merge(table_out.DefaultView.ToTable(true, "group_name"));
            table_sourcename.Merge(table_action.DefaultView.ToTable(true, "group_name"));
            table_sourcename.Merge(table_misadmin.DefaultView.ToTable(true, "group_name"));
            table_sourcename = table_sourcename.DefaultView.ToTable(true, "group_name");

            for (int i = 0; i < table_sourcename.Rows.Count; i++)
            {
                ReportSource reportSource = new ReportSource();
                DataRow dr_source = table_sourcename.Rows[i];
                reporthelper.ConvertDataRowToModel_sql(dr_source, reportSource);
                string sourcename_str = dr_source["group_name"].ToString().TrimEnd();
                reporthelper.ConvertDataRowToModel(dr_source, reportSource);
                decimal BetMoney = 0;
                decimal WinMoney = 0;
                decimal RebateMoney = 0;
                decimal ActivityDiscountAccountMoney = 0;
                decimal agentmoney = 0;
                decimal out_money = 0;
                //投注
                if (table_bet.Select("group_name = '" + sourcename_str + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_bet.Select("group_name = '" + sourcename_str + "'")[0], reportSource);
                    BetMoney = reportSource.BetMoney;
                }
                //中獎
                if (table_win.Select("group_name = '" + sourcename_str + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_win.Select("group_name = '" + sourcename_str + "'")[0], reportSource);
                    WinMoney = reportSource.WinMoney;
                }
                //返點
                if (table_rebate.Select("group_name = '" + sourcename_str + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_rebate.Select("group_name = '" + sourcename_str + "'")[0], reportSource);
                    RebateMoney = reportSource.Out_RebateAccount;
                }
                //提現
                if (table_out.Select("group_name = '" + sourcename_str + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_out.Select("group_name = '" + sourcename_str + "'")[0], reportSource);
                }
                //充值
                if (table_in.Select("group_name = '" + sourcename_str + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_in.Select("group_name = '" + sourcename_str + "'")[0], reportSource);
                }
                //活動
                if (table_action.Select("group_name = '" + sourcename_str + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_action.Select("group_name = '" + sourcename_str + "'")[0], reportSource);
                    ActivityDiscountAccountMoney = reportSource.ActivityDiscountAccountMoney;
                }
                //拒絕+行政
                if (table_misadmin.Select("group_name = '" + sourcename_str + "'").Count() > 0)
                {
                    out_money = Convert.ToDecimal(table_misadmin.Select("group_name = '" + sourcename_str + "'")[0]["out_money"]);

                }
                //代理
                if (table_agent.Select("group_name = '" + sourcename_str + "'").Count() > 0)
                {
                    agentmoney = Convert.ToDecimal(table_misadmin.Select("group_name = '" + sourcename_str + "'")[0]["agentmoney"]);
                }
                //註冊
                if (table_reg.Select("group_name = '" + sourcename_str + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_reg.Select("group_name = '" + sourcename_str + "'")[0], reportSource);
                }
                //首充
                if (table_newpay.Select("group_name = '" + sourcename_str + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_newpay.Select("group_name = '" + sourcename_str + "'")[0], reportSource);
                }

                //盈利＝投注-中奖-返点-活动-其他优惠-代理工资-代理分红+拒绝总额+行政提出
                decimal ProfitMoney = BetMoney - WinMoney - RebateMoney - ActivityDiscountAccountMoney - agentmoney + out_money;
                decimal ProfitRate = 0;
                if (BetMoney > 0)
                {
                    ProfitRate = ProfitMoney / BetMoney * 100;
                }

                reportSource.ProfitLoss = ProfitMoney;
                reportSource.WinRate = ProfitRate;
                listReportSource.Add(reportSource);
            }

            int totalCount = listReportSource.Count;
            DataTableHelper dataTableHelper = new DataTableHelper();
            DataTable dataTable = null;


            //保存记录总数量的表
            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);
            if (listReportSource == null || listReportSource.Count < 1)
            {
                dataTable = dataTableHelper.ConvertListToDataTable(listReportSource);
                dataSet.Tables.Add(dataTable);
                st.Stop();
                lock (logmsg)
                {
                    logmsg.CreateErrorLogTxt("After_Error", "GetReportSourceData", st.ElapsedMilliseconds.ToString(), para);
                }
                return dataSet;
            }


            totalCount = listReportSource.Count;
            dataSet.Tables[0].Rows[0]["totalCount"] = totalCount;
            listReportSource = listReportSource.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            dataTable = dataTableHelper.ConvertListToDataTable(listReportSource);
            dataSet.Tables.Add(dataTable);
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("After_GetReportSourceData", "GetReportSourceData", st.ElapsedMilliseconds.ToString(), para);
            }
            return dataSet;
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("After_Error", "GetReportSourceData", st.ElapsedMilliseconds.ToString(), para);
            }
            return null;
        }
    }
    /// <summary>
    /// 获取等級报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetGroupLayerData(string strIdentityId, string strIsFirst, string strUserName, int pageIndex, int pageSize)
    {
        DataSet dataSet = new DataSet();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "strIdentityId=" + strIdentityId + " strIsFirst=" + strIsFirst + " strUserName=" + strUserName + " pageIndex=" + pageIndex + " pageSize=" + pageSize;
        try
        {
            //验证是否有权访问  
            //string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return dataSet;
            //}
            //通过历史查询默认列表为空
            int totalCount = 0;
            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);
            DataTableHelper dataTableHelper = new DataTableHelper();
            List<GroupLayer> listGroupLayer = new List<GroupLayer>();
            DataTable dataTable = null;
            if (strIsFirst == "1")
            {
                dataTable = dataTableHelper.ConvertListToDataTable(listGroupLayer);
                dataSet.Tables.Add(dataTable);
                return dataSet;
            }
            //1、获取会员档案数据
            string[] strUserNames = strUserName.Split(",");
            string strWhereSql = string.Empty;
            Dictionary<string, GroupLayer> dicInOutAccount = new Dictionary<string, GroupLayer>();
            Dictionary<string, string> dicUsers = new Dictionary<string, string>();
            for (int i = 0; i < strUserNames.Length; i++)
            {
                strWhereSql += "'" + strUserNames[i] + "',";
            }
            if (string.IsNullOrWhiteSpace(strWhereSql))
            {
                dataTable = dataTableHelper.ConvertListToDataTable(listGroupLayer);
                dataSet.Tables.Add(dataTable);
                return dataSet;
            }
            if (strWhereSql.Length > 0)
            {
                strWhereSql = strWhereSql.Substring(0, strWhereSql.Length - 1);
            }
            else
            {
                strWhereSql = "0";
            }
            DataTable userDataTable = DBcon.DbHelperSQL.Query("select id,user_name,group_id,reg_time from dt_users where IdentityId='" + strIdentityId + "' and  user_name in (" + strWhereSql + ")  ").Tables[0];
            if (userDataTable != null && userDataTable.Rows.Count < 1)
            {
                dataTable = dataTableHelper.ConvertListToDataTable(listGroupLayer);
                dataSet.Tables.Add(dataTable);
                return dataSet;
            }
            for (int i = 0; i < strUserNames.Length; i++)
            {
                DataRow[] dataRows = userDataTable.Select("user_name='" + strUserNames[i] + "'");
                if (dataRows != null && dataRows.Length < 1)
                    continue;

                GroupLayer groupLayer = new GroupLayer();
                string strUserId = dataRows[0]["id"].ToString();
                if (dicInOutAccount.ContainsKey(strUserId))
                    continue;

                dicInOutAccount.Add(strUserId, groupLayer);
                listGroupLayer.Add(groupLayer);
                //会员Id
                groupLayer.User_id = int.Parse(strUserId);
                //会员账号称
                object bjUserName = dataRows[0]["user_name"];
                if (bjUserName != null)
                {
                    groupLayer.User_name = bjUserName.ToString();
                }
                //会员注册时间
                object bjRegTime = dataRows[0]["reg_time"];
                if (bjRegTime != null)
                {
                    groupLayer.zctime = DateTime.Parse(bjRegTime.ToString());
                }
                //会员等级
                object bjGroup = dataRows[0]["group_id"];
                if (bjGroup != null && !string.IsNullOrWhiteSpace(bjGroup.ToString()))
                {
                    groupLayer.groups = bjGroup.ToString();
                }
            }
            totalCount = listGroupLayer.Count;
            listGroupLayer = listGroupLayer.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            string user_in = "";
            for (int i = 0; i < listGroupLayer.Count; i++)
            {
                string strUserId = listGroupLayer[i].User_id.ToString();
                if (!dicUsers.ContainsKey(strUserId))
                {
                    dicUsers.Add(strUserId, strUserId);
                }
                user_in += strUserId + ",";
            }
            if (user_in.Length > 0)
            {
                user_in = user_in.Substring(0, user_in.Length - 1);
                user_in = " and user_id in (" + user_in + ") ";
            }
            else
            {
                user_in = " and user_id in (0) ";
            }
            DataTable table_lotterycode = new DataTable();
            DataTable table_lottery = new DataTable();
            DataTable table_lotteryname = new DataTable();
            DataTable table_bet = new DataTable();
            DataTable table_win = new DataTable();
            DataTable table_rebate = new DataTable();
            DataTable table_reward = new DataTable();
            DataTable table_out = new DataTable();
            DataTable table_action = new DataTable();
            DataTable table_in = new DataTable();
            DataTable table_agent = new DataTable();
            DataTable table_misadmin = new DataTable();
            DataTable table_reg = new DataTable();
            DataTable table_newpay = new DataTable();


            //connectstring
            string report_share = ReportHelper.Report_Share;
            string report_order_conn = DBcon.DBcontring.Report_Order_Conn;
            string report_in_conn = DBcon.DBcontring.Report_In_Conn;
            string report_out_conn = DBcon.DBcontring.Report_Out_Conn;
            string agent_order_conn = DBcon.DBcontring.Agent_Order_Conn;

            //報表
            List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
            sqlcmdlist.Add(new SqlParameter("@identityid", strIdentityId));
            SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();
            string bet_str = "select identityid,user_id,sum(out_bettingaccount) as Out_BettingAccount  from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_historyRecord] where identityid=@identityid  " + user_in + "  group by identityid,user_id";
            table_bet = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(bet_str, report_share, sqlcmdpar);


            string in_str = "select identityid,user_id,sum(In_alipayprepaid) as In_AlipayPrepaid,sum(in_alipayprepaid_num) as in_alipayprepaid_num,sum(in_artificialdeposit) as In_ArtificialDeposit,sum(in_artificialdeposit_num) as in_artificialdeposit_num,sum(in_bankprepaid) as In_BankPrepaid,sum(in_bankprepaid_num) as in_bankprepaid_num,sum(in_fastprepaid) as In_FastPrepaid,sum(in_fastprepaid_num) as in_fastprepaid_num,sum(in_wechatprepaid) as In_WeChatPrepaid,sum(in_wechatprepaid_num) as in_wechatprepaid_num,sum(In_QQ) as In_QQ ,sum(In_QQ_num) as In_QQ_num,sum(In_fourthprepaid) as In_FourthPrepaid ,sum(In_fourthPrepaid_num) as In_FourthPrepaid_num,sum(In_unionpayprepaid) as In_UnionpayPrepaid ,sum(In_unionpayPrepaid_num) as In_UnionpayPrepaid_num , sum(out_otherdiscountaccount) as Out_OtherDiscountAccount,sum(out_otherdiscountaccount_num) as out_otherdiscountaccount_num,sum(out_activitydiscountaccount) as Out_ActivityDiscountAccount,sum(out_activitydiscountaccount_num) as out_activitydiscountaccount_num,sum(out_cancelaccount) as Out_CancelAccount,sum(out_cancelaccount_num) as out_cancelaccount_num,sum(bonus) as Bonus,sum(wages) as Wages   from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_historyRecord] where identityid = @identityid " + user_in + "  group by identityid,user_id";
            table_in = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, sqlcmdpar);


            string win_str = " select identityid,user_id,sum(out_winningaccount) as Out_WinningAccount from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_historyRecord]   where identityid=@identityid " + user_in + "  group by identityid,user_id ";
            table_win = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(win_str, report_share, sqlcmdpar);

            string rebate_str = " select identityid,user_id,sum(out_rebateaccount) as Out_RebateAccount from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_get_point_historyRecord] where identityid=@identityid " + user_in + "  group by identityid,user_id";
            table_rebate = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(rebate_str, report_share, sqlcmdpar);

            string reward_str = " select identityid,user_id,sum(rewardmoney) as RewardMoney from [srv_lnk_total_out].[dafacloud].[dbo].[dt_reward_historyRecord] where identityid=@identityid " + user_in + "  group by identityid,user_id";
            table_reward = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(reward_str, report_share, sqlcmdpar);

            string out_str = "select identityid,user_id,sum(out_administrationaccount) as Out_AdministrationAccount,sum(out_administrationaccount_num) as out_administrationaccount_num,sum(out_account) as Out_Account,sum(out_account_num) as out_account_num,sum(out_refuseaccount) as Out_RefuseAccount,sum(out_refuseaccount_num) as out_refuseaccount_num  from [srv_lnk_total_out].[dafacloud].[dbo].[dt_user_out_historyRecord] where identityid = @identityid " + user_in + " group by identityid,user_id";
            table_out = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(out_str, report_share, sqlcmdpar);

            List<dt_report_inoutaccount_new> tempInOutAccountList = new List<dt_report_inoutaccount_new>();
            ReportHelper reporthelper = new ReportHelper();
            int user_count = listGroupLayer.Count;
            for (int i = 0; i < user_count; i++)
            {
                dt_report_inoutaccount_new report_inoutaccount = new dt_report_inoutaccount_new();
                string user = listGroupLayer[i].User_id.ToString();
                decimal totalyh = 0;//活動
                decimal Deficit = 0;//盈利
                decimal totalruk = 0; //充值金額
                decimal totalchuk = 0;//提現金額
                decimal totalplay = 0;//投注
                decimal totalwin = 0;//中獎
                decimal totalfd = 0;//返點
                decimal totalreward = 0;//打賞
                decimal Out_AdministrationAccount = 0;//行政提出
                decimal Out_RefuseAccount = 0;//拒絕提出
                decimal agent = 0;//代理
                int ruknum = 0;//充值次數
                int chuknum = 0;//提現次數

                //投注
                if (table_bet.Select("user_id = '" + user + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_bet.Select("user_id = '" + user + "'")[0], report_inoutaccount);
                    totalplay = Convert.ToDecimal(table_bet.Select("user_id = '" + user + "'")[0]["Out_BettingAccount"]);
                }
                //in
                if (table_in.Select("user_id = '" + user + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_in.Select("user_id = '" + user + "'")[0], report_inoutaccount);
                    totalyh = report_inoutaccount.Out_OtherDiscountAccount + report_inoutaccount.Out_ActivityDiscountAccount;
                    totalruk = report_inoutaccount.In_AlipayPrepaid + report_inoutaccount.In_ArtificialDeposit + report_inoutaccount.In_BankPrepaid + report_inoutaccount.In_FastPrepaid + report_inoutaccount.In_WeChatPrepaid + report_inoutaccount.In_QQ + report_inoutaccount.In_FourthPrepaid + report_inoutaccount.In_UnionpayPrepaid;
                    agent = report_inoutaccount.Bonus + report_inoutaccount.Wages;
                    ruknum = Convert.ToInt32(table_in.Select("user_id = '" + user + "'")[0]["in_alipayprepaid_num"]) + Convert.ToInt32(table_in.Select("user_id = '" + user + "'")[0]["in_artificialdeposit_num"]) + Convert.ToInt32(table_in.Select("user_id = '" + user + "'")[0]["in_bankprepaid_num"]) + Convert.ToInt32(table_in.Select("user_id = '" + user + "'")[0]["in_fastprepaid_num"]) + Convert.ToInt32(table_in.Select("user_id = '" + user + "'")[0]["in_wechatprepaid_num"]) + Convert.ToInt32(table_in.Select("user_id = '" + user + "'")[0]["In_QQ_num"]) + Convert.ToInt32(table_in.Select("user_id = '" + user + "'")[0]["In_FourthPrepaid_num"]) + Convert.ToInt32(table_in.Select("user_id = '" + user + "'")[0]["In_UnionpayPrepaid_num"]);
                }
                //out
                if (table_out.Select("user_id = '" + user + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_out.Select("user_id = '" + user + "'")[0], report_inoutaccount);
                    Out_AdministrationAccount = report_inoutaccount.Out_AdministrationAccount;
                    Out_RefuseAccount = report_inoutaccount.Out_RefuseAccount;
                    totalchuk = Out_AdministrationAccount + Out_RefuseAccount + report_inoutaccount.Out_Account;
                    chuknum = Convert.ToInt32(table_out.Select("user_id = '" + user + "'")[0]["out_administrationaccount_num"]) + Convert.ToInt32(table_out.Select("user_id = '" + user + "'")[0]["out_account_num"]);
                }
                //win
                if (table_win.Select("user_id = '" + user + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_win.Select("user_id = '" + user + "'")[0], report_inoutaccount);
                    totalwin = report_inoutaccount.Out_WinningAccount;

                }
                //rebate
                if (table_rebate.Select("user_id = '" + user + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_rebate.Select("user_id = '" + user + "'")[0], report_inoutaccount);
                    totalfd = report_inoutaccount.Out_RebateAccount;
                }
                //reward
                if (table_reward.Select("user_id = '" + user + "'").Count() > 0)
                {
                    reporthelper.ConvertDataRowToModel_sql(table_reward.Select("user_id = '" + user + "'")[0], report_inoutaccount);
                    totalreward = report_inoutaccount.RewardMoney;
                }
                //盈利总额=中奖-投注+返点+活动+其他优惠+代理工资+代理分红-拒绝总额-行政提出
                Deficit = totalwin - totalplay + totalfd + totalyh + agent - Out_RefuseAccount - Out_AdministrationAccount;
                listGroupLayer[i].chuknum = chuknum;
                listGroupLayer[i].Deficit = Deficit;
                listGroupLayer[i].ruknum = ruknum;
                listGroupLayer[i].totalchuk = totalchuk;
                listGroupLayer[i].totalfd = totalfd;
                listGroupLayer[i].TotalReward = totalreward;
                listGroupLayer[i].totalplay = totalplay;
                listGroupLayer[i].totalruk = totalruk;
                listGroupLayer[i].totalwin = totalwin;
                listGroupLayer[i].totalyh = totalyh;
            }
            int inOutAccountListCount = tempInOutAccountList.Count;

            dataSet.Tables[0].Rows[0]["totalCount"] = totalCount;
            dataTable = dataTableHelper.ConvertListToDataTable(listGroupLayer);
            dataSet.Tables.Add(dataTable);

            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("After_GetGroupLayerData", "GetGroupLayerData", st.ElapsedMilliseconds.ToString(), para);
            }
            return dataSet;
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("After_Error", "GetGroupLayerData", st.ElapsedMilliseconds.ToString(), para);
            }
            return null;
        }
    }
    /// <summary>
    /// 获取会员取款盈利
    /// </summary>
    /// <param name="UserId"></param>
    /// <param name="IdentityId"></param>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetUserProfit(int UserId, string strIdentityId, string strOrderTime)
    {
        decimal ProfitLoss = 0m;
        string ProfitRebate = "0.00%";
        string strResult = string.Empty;
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "UserId=" + UserId + " IdentityId=" + strIdentityId + " strOrderTime=" + strOrderTime;
        int searchType = 0;
        if (Convert.ToDateTime(strOrderTime).Date >= DateTime.Now.AddDays(-1).Date)
        {
            searchType = 1;
        }
        else if (Convert.ToDateTime(strOrderTime).Date < DateTime.Now.AddDays(-1).Date)
        {
            searchType = 2;
        }
        else
        {
            searchType = 3;
        }
        try
        {
            st.Reset();
            st.Start();

            DateTime dtStartTime = Convert.ToDateTime(strOrderTime + " 00:00:00");
            DateTime dtEndTime = Convert.ToDateTime(strOrderTime + " 23:59:59");
            string report_share = ReportHelper.Report_Share;
            string report_order_conn = DBcon.DBcontring.Report_Order_Conn;
            string report_in_conn = DBcon.DBcontring.Report_In_Conn;
            string report_out_conn = DBcon.DBcontring.Report_Out_Conn;
            string agent_order_conn = DBcon.DBcontring.Agent_Order_Conn;



            List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
            sqlcmdlist.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist.Add(new SqlParameter("@user_id", UserId));
            sqlcmdlist.Add(new SqlParameter("@strStartDate", dtStartTime));
            sqlcmdlist.Add(new SqlParameter("@strEndDate", dtEndTime));
            SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();

            ReportHelper reporthelper = new ReportHelper();
            dt_month_all_record dar = new dt_month_all_record();

            //投注
            string order_str = "select identityid, user_id as UserId, sum(normal_money) as tzmony from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record]    where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate   and user_id = @user_id  group by identityid,user_id";
            DataTable table_bet = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(order_str, report_share, sqlcmdpar);
            reporthelper.ConvertToMode(table_bet, dar);

            //中獎
            string win_str;
            if (searchType == 1)
            {
                win_str = "select identityid, user_id as UserId, sum(money) as zjmoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord]  where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate   and user_id = @user_id group by identityid,user_id";
            }
            else if (searchType == 2)
            {
                win_str = "select identityid, user_id as UserId, sum(money) as zjmoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold]  where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate   and user_id = @user_id group by identityid,user_id";
            }
            else
            {
                win_str = "select identityid,UserId,sum([money]) as zjmoney from( select identityid, user_id as UserId,[money],sourcename,lotterycode,add_date from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord] where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and user_id = @user_id union select identityid, user_id as UserId,[money],sourcename,lotterycode,add_date from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold] where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and user_id = @user_id ) as all_userrecord group by identityid,UserId ";
            }
            //string win_str = "select identityid, user_id as UserId, sum(money) as zjmoney from [dbo].[dt_lottery_winning_userrecord]    where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate   and user_id = @user_id group by identityid,user_id";
            DataTable table_win = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(win_str, report_share, sqlcmdpar);
            reporthelper.ConvertToMode(table_win, dar);

            //返點
            string rebate_str = "select identityid, user_id as UserId, sum(money) as rebatemoney from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_get_point_userrecord]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate   and user_id = @user_id group by identityid,user_id";
            DataTable table_rebate = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(rebate_str, report_share, sqlcmdpar);
            reporthelper.ConvertToMode(table_rebate, dar);

            //活動
            string action_str = "select identityid, user_id as UserId, sum(money) as hdljmony from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and type2 in (7,12)   and user_id = @user_id group by identityid,user_id";
            DataTable table_action = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(action_str, report_share, sqlcmdpar);
            reporthelper.ConvertToMode(table_action, dar);

            //代理
            string agent_str = "select identityid,user_id as UserId, sum(money) as dlgzmony from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and type2 in (13,14) and state =1  and user_id = @user_id  group by identityid,user_id";
            DataTable table_agent = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_str, report_share, sqlcmdpar);

            //拒絕+行政
            string misadmin_str = "select identityid,user_id as UserId, sum(money) as xztcmony from [srv_lnk_total_out].[dafacloud].[dbo].[dt_user_out_record]   where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  and ((type2 =10 and state=1) or (type2=3 and state=3)) and user_id = @user_id group by identityid,user_id";
            DataTable table_misadmin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(misadmin_str, report_share, sqlcmdpar);


            decimal zjmoney = dar.zjmoney;//中奖金额
            decimal hdljmony = dar.hdljmony;//活动礼金
            decimal rebatemony = dar.rebatemoney;//返点
            decimal agent = dar.dlgzmony;// 代理工资 +代理分红  偷懶合併用字段dlgzmony取代
            decimal out_money = dar.xztcmony;//行政提出+拒绝金额  偷懶合併用字段xztcmony取代
            decimal tzmony = dar.tzmony;//投注金额
                                        //盈利总额=中奖-投注+返点+活动+其他优惠+代理工资+代理分红-拒绝总额-行政提出

            ProfitLoss = zjmoney - tzmony + rebatemony + hdljmony + agent - out_money;

            //盈率
            decimal fProfitRate = 0;
            if (tzmony != 0)
            {
                fProfitRate = ProfitLoss / tzmony * 100;
            }
            ProfitRebate = fProfitRate.ToString("0.00") + "%";
            //redisClient.Quit();
            //redisClient.Dispose();
            //redisClientKey.Quit();
            //redisClientKey.Dispose();
            // GlobalVariable.PooledRedisClientManager.DisposeClient(redisClient);
            //  GlobalVariable.PooledRedisClientManager.DisposeClient(redisClientKey);
            strResult = ProfitLoss.ToString("#0.00") + "_" + ProfitRebate;
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("After_GetUserProfit", "GetUserProfit", st.ElapsedMilliseconds.ToString(), para);
            }
            return strResult;
        }
        catch (Exception ex)
        {
            strResult = "0.00_0.00%";
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("After_Error", "GetUserProfit", st.ElapsedMilliseconds.ToString(), para);
            }
        }
        return strResult;
    }
    /// <summary>
    /// 获取后台首页报表预览数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportIndexPreviewData(string strIdentityId, string strStartDate, string strEndDate)
    {
        DataSet dataSet = new DataSet();
        List<ReportSummary> listReportSummary = new List<ReportSummary>();
        ReportSummary reportSummary = new ReportSummary();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "IdentityId=" + strIdentityId + " StartTime=" + strStartDate + " EndTime=" + strEndDate;
        try
        {
            st.Reset();
            st.Start();
            //验证是否有权访问  
            //string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return dataSet;
            //}
            // var redisClient = GlobalVariable.redisClient;
            //var redisClientKey = GlobalVariable.redisClientKey;

            string report_share = ReportHelper.Report_Share;
            string report_order_conn = DBcon.DBcontring.Report_Order_Conn;
            string report_in_conn = DBcon.DBcontring.Report_In_Conn;
            string agent_order_conn = DBcon.DBcontring.Agent_Order_Conn;

            DateTime dtStartTime = DateTime.Parse(strStartDate);
            DateTime dtEndTime = DateTime.Parse(strEndDate);

            //報表
            List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
            sqlcmdlist.Add(new SqlParameter("@identityid", strIdentityId));
            sqlcmdlist.Add(new SqlParameter("@strStartDate", strStartDate));
            sqlcmdlist.Add(new SqlParameter("@strEndDate", strEndDate));
            SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();

            ReportHelper reporthelper = new ReportHelper();
            //偷懶用In_AlipayPrepaid字段表是type=1 and state=1
            string in_str = "select distinct user_id,identityid,add_date as AddTime ,sum(money) as In_AlipayPrepaid  from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record]  where identityid=@identityid and add_date>=@strStartDate and add_date <=@strEndDate and type=1 and state=1 group by identityid, user_id, add_date";
            DataTable table_in = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, report_share, sqlcmdpar);

            //投注
            string order_str = "select identityid, user_id, add_date as AddTime,sum(normal_money) as Out_BettingAccount from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record]    where identityid = @identityid and add_date >= @strStartDate and add_date <=@strEndDate  group by identityid, user_id, add_date";
            DataTable table_bet = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(order_str, report_share, sqlcmdpar);

            DataTable dt_dis = table_bet.DefaultView.ToTable(true, "user_id");
            dt_dis.Merge(table_in.DefaultView.ToTable(true, "user_id"));
            dt_dis = dt_dis.DefaultView.ToTable(true, "user_id");

            List<dt_report_inoutaccount> inOutAccountList = new List<dt_report_inoutaccount>();
            for (int i = 0; i < table_bet.Rows.Count; i++)
            {
                DataRow dr_bet = table_bet.Rows[i];
                dt_report_inoutaccount inoutaccount = new dt_report_inoutaccount();
                reporthelper.ConvertDataRowToModel_sql(dr_bet, inoutaccount);
                inOutAccountList.Add(inoutaccount);
            }
            for (int i = 0; i < table_in.Rows.Count; i++)
            {
                DataRow dr_in = table_in.Rows[i];
                dt_report_inoutaccount inoutaccount = new dt_report_inoutaccount();
                reporthelper.ConvertDataRowToModel_sql(dr_in, inoutaccount);
                inOutAccountList.Add(inoutaccount);
            }



            //      List < dt_report_inoutaccount > inOutAccountList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, dtStartTime, dtEndTime, strIdentityId).ToList();
            // List<dt_report_num> reportNumList = redisHelper.Search<dt_report_num>(redisClient, redisClientKey, dtStartTime, dtEndTime, strIdentityId).ToList();
            int inOutAccountListCount = inOutAccountList.Count;
            Dictionary<string, Dictionary<string, string>> dicData = new Dictionary<string, Dictionary<string, string>>();
            for (var i = 0; i < inOutAccountListCount; i++)
            {
                //会员Id
                string strUserId = inOutAccountList[i].user_id.ToString();
                string strAddTime = inOutAccountList[i].AddTime.ToString("yyyy-MM-dd");
                //充值人數,偷懶只要抓type=1and state=1  in_money 先用In_AlipayPrepaid字段
                if (inOutAccountList[i].In_AlipayPrepaid > 0 || inOutAccountList[i].In_BankPrepaid > 0 ||
                    inOutAccountList[i].In_FastPrepaid > 0 || inOutAccountList[i].In_WeChatPrepaid > 0 || inOutAccountList[i].In_ArtificialDeposit > 0)
                {
                    string strKey = "Recharge_" + strAddTime;
                    if (!dicData.ContainsKey(strKey))
                    {
                        Dictionary<string, string> dicUsers = new Dictionary<string, string>();
                        dicUsers.Add(strUserId, strUserId);
                        dicData.Add(strKey, dicUsers);
                    }
                    else
                    {
                        Dictionary<string, string> dicUsers = dicData[strKey];
                        if (!dicUsers.ContainsKey(strUserId))
                        {
                            dicUsers.Add(strUserId, strUserId);
                        }
                    }
                }
                //投注人数
                if (inOutAccountList[i].Out_BettingAccount > 0)
                {
                    string strKey2 = "BetRecord_" + strAddTime;
                    if (!dicData.ContainsKey(strKey2))
                    {
                        Dictionary<string, string> dicUsers = new Dictionary<string, string>();
                        dicUsers.Add(strUserId, strUserId);
                        dicData.Add(strKey2, dicUsers);
                    }
                    else
                    {
                        Dictionary<string, string> dicUsers = dicData[strKey2];
                        if (!dicUsers.ContainsKey(strUserId))
                        {
                            dicUsers.Add(strUserId, strUserId);
                        }
                    }
                }
            }
            //注册人数
            DataTable registerDataTable = DBcon.DbHelperSQL.Query(@"select UserId,AddTime from dt_users_register  where identityid='" + strIdentityId +
                                                                    @"' and DATEDIFF(day,AddTime,'" + strStartDate + "')<=0 and DATEDIFF(day,AddTime,'" + strEndDate + "')>=0 and Tester=0").Tables[0];
            for (int i = 0; i < registerDataTable.Rows.Count; i++)
            {
                string strUserId = registerDataTable.Rows[i]["UserId"].ToStringValue();
                string strAddTime = registerDataTable.Rows[i]["AddTime"].ToDateTime().ToString("yyyy-MM-dd");
                string strKey = "Signup_" + strAddTime;
                if (!dicData.ContainsKey(strKey))
                {
                    Dictionary<string, string> dicUsers = new Dictionary<string, string>();
                    dicUsers.Add(strUserId, strUserId);
                    dicData.Add(strKey, dicUsers);
                }
                else
                {
                    Dictionary<string, string> dicUsers = dicData[strKey];
                    if (!dicUsers.ContainsKey(strUserId))
                    {
                        dicUsers.Add(strUserId, strUserId);
                    }
                }
            }
            //首充人员
            string newpaid_str = "select distinct userid as user_id,identityid,addtime as AddTime from  dt_user_newpay where identityid=@identityid and addtime >= @strStartDate and addtime <=@strEndDate and flog=0";
            DataTable dt_newpaid = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(newpaid_str, agent_order_conn, sqlcmdpar);
            List<dt_report_newprepaid> reportNewPreapdiList = new List<dt_report_newprepaid>();
            for (int i = 0; i < dt_newpaid.Rows.Count; i++)
            {
                dt_report_newprepaid newpaid = new dt_report_newprepaid();
                DataRow dr_newpaid = dt_newpaid.Rows[i];
                reporthelper.ConvertDataRowToModel_sql(dr_newpaid, newpaid);
                reportNewPreapdiList.Add(newpaid);
            }



            for (int i = 0; i < reportNewPreapdiList.Count; i++)
            {
                string strUserId = reportNewPreapdiList[i].user_id.ToString();
                string strAddTime = reportNewPreapdiList[i].AddTime.ToString("yyyy-MM-dd");
                string strKey = "FirstDeal_" + strAddTime;
                if (!dicData.ContainsKey(strKey))
                {
                    Dictionary<string, string> dicUsers = new Dictionary<string, string>();
                    dicUsers.Add(strUserId, strUserId);
                    dicData.Add(strKey, dicUsers);
                }
                else
                {
                    Dictionary<string, string> dicUsers = dicData[strKey];
                    if (!dicUsers.ContainsKey(strUserId))
                    {
                        dicUsers.Add(strUserId, strUserId);
                    }
                }
            }
            Dictionary<string, Type> dicColumns = new Dictionary<string, Type>();
            dicColumns.Add("FirstDeal", typeof(int));
            dicColumns.Add("Signup", typeof(int));
            dicColumns.Add("BetRecord", typeof(int));
            dicColumns.Add("Recharge", typeof(int));
            EasyFrame.NewCommon.Common.DataTableHelper dataTableHelper = new EasyFrame.NewCommon.Common.DataTableHelper();
            DataTable dataTable = dataTableHelper.ConvertDictionaryToDataTable(dicColumns);
            TimeSpan timeSpan = DateTime.Parse(strEndDate) - DateTime.Parse(strStartDate);
            for (int i = 0; i <= timeSpan.Days; i++)
            {
                string strDate = DateTime.Parse(strStartDate).AddDays(i).ToString("yyyy-MM-dd");
                DataRow dataRow = dataTable.NewRow();
                foreach (KeyValuePair<string, Type> keyValuePair in dicColumns)
                {
                    int iData = 0;
                    string strKey = keyValuePair.Key + "_" + strDate;
                    if (dicData.ContainsKey(strKey))
                    {
                        iData = dicData[strKey].Count;
                    }
                    dataRow[keyValuePair.Key] = iData;
                }
                dataTable.Rows.Add(dataRow);
            }
            dataSet.Tables.Add(dataTable);
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("After_GetReportIndexPreviewData", "GetReportIndexPreviewData", st.ElapsedMilliseconds.ToString(), para);
            }
            return dataSet;
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("After_Error", "GetReportIndexPreviewData", st.ElapsedMilliseconds.ToString(), para);
            }
            return null;
        }
    }
    /// <summary>
    /// 获取今日排名报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportRankingData(string strIdentityId, string strStartDate, string strEndDate)
    {
        DataSet dataSet = new DataSet();
        try
        {
            //验证是否有权访问  
            //string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return dataSet;
            //}
            //获取汇总数据
            var redisClient = GlobalVariable.redisClient;
            var redisClientKey = GlobalVariable.redisClientKey;
            DateTime dtStartTime = DateTime.Parse(strStartDate);
            DateTime dtEndTime = DateTime.Parse(strEndDate);
            DataTableHelper dataTableHelper = new DataTableHelper();
            ReportRanking reportRanking = new ReportRanking();
            DataTable dataTable = dataTableHelper.ConvertModelToDataTable(reportRanking);
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "Type";
            dataTable.Columns.Add(dataColumn);
            dataSet.Tables.Add(dataTable);
            List<dt_report_inoutaccount> inOutAccountList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, dtStartTime, dtEndTime, strIdentityId).ToList();
            GlobalVariable.PooledRedisClientManager.DisposeClient(redisClient);
            GlobalVariable.PooledRedisClientManager.DisposeClient(redisClientKey);
            if (inOutAccountList == null || inOutAccountList.Count < 1)
            {
                return dataSet;
            }
            int inOutAccountCount = inOutAccountList.Count;
            Dictionary<string, ReportMember> dicReportMember = new Dictionary<string, ReportMember>();
            Dictionary<string, string> dicUsers = new Dictionary<string, string>();
            List<ReportMember> listReportMember = new List<ReportMember>();
            for (int i = 0; i < inOutAccountCount; i++)
            {
                string strUserId = inOutAccountList[i].user_id.ToString();
                string strAgentId = inOutAccountList[i].agent_id.ToString();
                ReportMember reportMember = new ReportMember();
                if (!dicReportMember.ContainsKey(strUserId))
                {
                    dicReportMember.Add(strUserId, reportMember);
                    listReportMember.Add(reportMember);
                    dicUsers.Add(strUserId, strAgentId);
                }
                reportMember = dicReportMember[strUserId];
                //会员Id
                reportMember.UserId = strUserId;
                //入款总额
                reportMember.InMoney += inOutAccountList[i].In_FastPrepaid;
                reportMember.InMoney += inOutAccountList[i].In_AlipayPrepaid;
                reportMember.InMoney += inOutAccountList[i].In_ArtificialDeposit;
                reportMember.InMoney += inOutAccountList[i].In_BankPrepaid;
                reportMember.InMoney += inOutAccountList[i].In_WeChatPrepaid;
                //投注总额
                reportMember.BetMoney += inOutAccountList[i].Out_BettingAccount;
                //盈利总额=中奖-投注+返点+活动+其他优惠+代理工资+代理分红-拒绝总额-行政提出
                reportMember.ProfitLoss += inOutAccountList[i].Out_WinningAccount;
                reportMember.ProfitLoss -= inOutAccountList[i].Out_BettingAccount;
                reportMember.ProfitLoss += inOutAccountList[i].Out_RebateAccount;
                reportMember.ProfitLoss += inOutAccountList[i].Out_ActivityDiscountAccount;
                reportMember.ProfitLoss += inOutAccountList[i].Out_OtherDiscountAccount;
                reportMember.ProfitLoss += inOutAccountList[i].Wages;
                reportMember.ProfitLoss += inOutAccountList[i].Bonus;
                reportMember.ProfitLoss -= inOutAccountList[i].Out_RefuseAccount;
                reportMember.ProfitLoss -= inOutAccountList[i].Out_AdministrationAccount;
            }
            //获取会员信息表
            string strUserIdWhereSql = string.Empty;
            string strUserCapitalWhereSql = string.Empty;
            Dictionary<string, string> dicTempUsers = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> user in dicUsers)
            {
                if (!dicTempUsers.ContainsKey(user.Key))
                {
                    dicTempUsers.Add(user.Key, user.Key);
                    strUserIdWhereSql += user.Key + ",";
                }
                if (!dicTempUsers.ContainsKey(user.Value))
                {
                    dicTempUsers.Add(user.Value, user.Value);
                    strUserIdWhereSql += user.Value + ",";
                }
            }
            if (!string.IsNullOrWhiteSpace(strUserIdWhereSql))
            {
                if (strUserIdWhereSql.Length > 0)
                {
                    strUserIdWhereSql = strUserIdWhereSql.Substring(0, strUserIdWhereSql.Length - 1);
                }
                strUserCapitalWhereSql = " user_id in(" + strUserIdWhereSql + ")";
                strUserIdWhereSql = " id in(" + strUserIdWhereSql + ")";
            }
            else
            {
                strUserIdWhereSql = "1=2";
                strUserCapitalWhereSql = "1=2";
            }
            DataTable userDataTable = DBcon.DbHelperSQL.Query("select id,user_name,avatar from dt_users where identityid='" + strIdentityId + "' and " + strUserIdWhereSql).Tables[0];
            //充值排名
            List<ReportMember> RechargeTopList = listReportMember.OrderByDescending(m => m.InMoney).Take(10).ToList();
            for (int i = 0; i < RechargeTopList.Count; i++)
            {
                DataRow dataRow = dataTable.NewRow();
                DataRow[] userDataRows = userDataTable.Select("id=" + RechargeTopList[i].UserId);
                if (userDataRows == null || userDataRows.Length < 1)
                    continue;

                dataRow["NickName"] = userDataRows[0]["user_name"].ToStringValue();
                if (i <= 2)
                {
                    dataRow["UserPhoto"] = userDataRows[0]["avatar"].ToStringValue();
                }
                dataRow["RechargeMoney"] = RechargeTopList[i].InMoney;
                dataRow["Type"] = "RechargeTop10";
                dataTable.Rows.Add(dataRow);
            }
            //盈利排名
            List<ReportMember> ProfitTopList = listReportMember.Where(m => m.ProfitLoss >= 0).OrderByDescending(m => m.ProfitLoss).Take(10).ToList();
            for (int i = 0; i < ProfitTopList.Count; i++)
            {
                DataRow dataRow = dataTable.NewRow();
                DataRow[] userDataRows = userDataTable.Select("id=" + RechargeTopList[i].UserId);
                if (userDataRows == null || userDataRows.Length < 1)
                    continue;

                dataRow["NickName"] = userDataRows[0]["user_name"].ToStringValue();
                if (i <= 2)
                {
                    dataRow["UserPhoto"] = userDataRows[0]["avatar"].ToStringValue();
                }
                dataRow["RechargeMoney"] = ProfitTopList[i].ProfitLoss;
                dataRow["Type"] = "ProfitTop10";
                dataTable.Rows.Add(dataRow);
            }
            //负盈利排名
            List<ReportMember> NoProfitTopList = listReportMember.Where(m => m.ProfitLoss < 0).OrderByDescending(m => m.ProfitLoss).Take(10).ToList();
            for (int i = 0; i < NoProfitTopList.Count; i++)
            {
                DataRow dataRow = dataTable.NewRow();
                DataRow[] userDataRows = userDataTable.Select("id=" + RechargeTopList[i].UserId);
                if (userDataRows == null || userDataRows.Length < 1)
                    continue;

                dataRow["NickName"] = userDataRows[0]["user_name"].ToStringValue();
                if (i <= 2)
                {
                    dataRow["UserPhoto"] = userDataRows[0]["avatar"].ToStringValue();
                }
                dataRow["RechargeMoney"] = NoProfitTopList[i].ProfitLoss;
                dataRow["Type"] = "NoProfitTop10";
                dataTable.Rows.Add(dataRow);
            }
            //投注排名
            List<ReportMember> BetTopList = listReportMember.OrderByDescending(m => m.BetMoney).Take(10).ToList();
            for (int i = 0; i < BetTopList.Count; i++)
            {
                DataRow dataRow = dataTable.NewRow();
                DataRow[] userDataRows = userDataTable.Select("id=" + RechargeTopList[i].UserId);
                if (userDataRows == null || userDataRows.Length < 1)
                    continue;

                dataRow["NickName"] = userDataRows[0]["user_name"].ToStringValue();
                if (i <= 2)
                {
                    dataRow["UserPhoto"] = userDataRows[0]["avatar"].ToStringValue();
                }
                dataRow["RechargeMoney"] = BetTopList[i].BetMoney;
                dataRow["Type"] = "BetTop10";
                dataTable.Rows.Add(dataRow);
            }
            return dataSet;
        }
        catch (Exception ex)
        {
            Log.Error(ex);
            return null;
        }
    }
    /// <summary>
    /// 获取各站訊息
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetIdentityidReportData(string strIdentityId, string strStartDate, string strEndDate, int pageIndex, int pageSize, string strOrderBy)
    {
        if (strIdentityId == null)
            strIdentityId = "";
        if (strOrderBy == null || strOrderBy == "")
            strOrderBy = "Out_BettingAccountNum";

        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "strIdentityId=" + strIdentityId + " strStartDate=" + strStartDate + " strEndDate=" + strEndDate + " pageIndex=" + pageIndex + " pageSize=" + pageSize + " strOrderBy=" + strOrderBy;
        try
        {
            st.Reset();
            st.Start();
            string strStartDate_s = Convert.ToDateTime(strStartDate).ToString("yyyy-MM-dd HH:mm:ss");
            string strEndDate_s = Convert.ToDateTime(strEndDate).ToString("yyyy-MM-dd HH:mm:ss");

            //从报表库拿数据
            string identityid = strIdentityId; //条件从查询条件获取
            string begindate = strStartDate_s;//条件从查询条件获取
            string enddate = strEndDate_s;//条件从查询条件获取

            //線上
            List<SqlParameter> sqlcmdlist_online = new List<SqlParameter>();
            sqlcmdlist_online.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 10) { Value = strIdentityId });
            sqlcmdlist_online.Add(new SqlParameter("@strStartDate", SqlDbType.DateTime) { Value = strStartDate });
            sqlcmdlist_online.Add(new SqlParameter("@strEndDate", SqlDbType.DateTime) { Value = Convert.ToDateTime(strEndDate).AddDays(1).ToString("yyyy-MM-dd") });
            SqlParameter[] sqlcmdpar_online = sqlcmdlist_online.ToArray();

            //報表
            List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
            sqlcmdlist.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 10) { Value = strIdentityId });
            sqlcmdlist.Add(new SqlParameter("@strStartDate", SqlDbType.DateTime) { Value = strStartDate });
            sqlcmdlist.Add(new SqlParameter("@strEndDate", SqlDbType.DateTime) { Value = strEndDate });
            SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();
            List<SqlParameter> sqlcmdlist1 = new List<SqlParameter>();
            sqlcmdlist1.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 10) { Value = strIdentityId });
            sqlcmdlist1.Add(new SqlParameter("@strStartDate", SqlDbType.DateTime) { Value = strStartDate });
            sqlcmdlist1.Add(new SqlParameter("@strEndDate", SqlDbType.DateTime) { Value = strEndDate });
            SqlParameter[] in_sqlcmdpar = sqlcmdlist1.ToArray();
            List<SqlParameter> sqlcmdlist2 = new List<SqlParameter>();
            sqlcmdlist2.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 10) { Value = strIdentityId });
            sqlcmdlist2.Add(new SqlParameter("@strStartDate", SqlDbType.DateTime) { Value = strStartDate });
            sqlcmdlist2.Add(new SqlParameter("@strEndDate", SqlDbType.DateTime) { Value = strEndDate });
            SqlParameter[] out_sqlcmdpar = sqlcmdlist2.ToArray();
            List<SqlParameter> sqlcmdlist3 = new List<SqlParameter>();
            sqlcmdlist3.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 10) { Value = strIdentityId });
            sqlcmdlist3.Add(new SqlParameter("@strStartDate", SqlDbType.DateTime) { Value = strStartDate });
            sqlcmdlist3.Add(new SqlParameter("@strEndDate", SqlDbType.DateTime) { Value = strEndDate });
            SqlParameter[] order_sqlcmdpar = sqlcmdlist3.ToArray();

            //報表物件
            dt_identityidReport dar = new dt_identityidReport();
            List<dt_identityidReport> listdar = new List<dt_identityidReport>();

            //connectstring
            string report_share = ReportHelper.Report_Share;
            string report_in_conn = ReportHelper.Report_In_Conn;
            string report_out_conn = ReportHelper.Report_Out_Conn;
            string report_order_conn = ReportHelper.Report_Order_Conn;
            string instr = "";
            string where_str = "";

            if (identityid != "")
                where_str = " and identityid = @identityid";

            //datatable
            DataTable intable = null;
            string outstr = "";
            DataTable outtable = null;
            string orderstr = "";
            DataTable ordertable = null;
            string congkustr = "";
            DataTable congkutable = null;


            ReportHelper reporthelper = new ReportHelper();
            DataTableHelper dataTableHelper = new DataTableHelper();

            //查询in表的数据
            //instr = "select convert(varchar(10),add_date,121) as AddDate,identityid,cznumber  as In_AccountNum,czmony as In_Account,czcount as In_AccountCount,xthdmony as Out_ActivityDiscountAccount,qthdmony as Out_OtherDiscountAccount from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_sum]  with(nolock) where   add_date>=@strStartDate and add_date<=@strEndDate " + where_str;

            instr = "select convert(varchar(10),add_date,121) as AddDate,identityid,max(cznumber)  as In_AccountNum,sum(czmony) as In_Account,sum(czcount) as In_AccountCount,sum(xthdmony) as Out_ActivityDiscountAccount,sum(qthdmony) as Out_OtherDiscountAccount from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_sum]  with(nolock) where   add_date>=@strStartDate and add_date<=@strEndDate " + where_str + " group by identityid,add_date ";

            intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, report_share, in_sqlcmdpar);
            lock (dar)
                reporthelper.ConvertToMode<dt_identityidReport>(intable, dar, listdar);

            //查询winning表的数据
            instr = "select convert(varchar(10),add_date,121) as AddDate,identityid,zjmoney as In_WinningAccount from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_sum]  with(nolock) where  add_date>=@strStartDate and add_date<=@strEndDate " + where_str;
            intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, report_share, in_sqlcmdpar);
            lock (dar)
            {
                for (int i = 0; i < intable.Rows.Count; i++)
                {
                    string ntime = intable.Rows[i]["AddDate"].ToString();
                    string sidentityid = intable.Rows[i]["identityid"].ToString().TrimEnd();
                    var mm = listdar.Where(o => o.identityid == sidentityid && o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(intable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(intable.Rows[i], dar, listdar);
                }
            }

            //查询get_point表的数据
            instr = "select convert(varchar(10),add_date,121) as AddDate,identityid,rebatemoney as Out_RebateAccount from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_get_point_sum]  with(nolock) where  add_date>=@strStartDate and add_date<=@strEndDate " + where_str;
            intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, report_share, in_sqlcmdpar);
            lock (dar)
            {
                for (int i = 0; i < intable.Rows.Count; i++)
                {
                    string ntime = intable.Rows[i]["AddDate"].ToString();
                    string sidentityid = intable.Rows[i]["identityid"].ToString().TrimEnd();
                    var mm = listdar.Where(o => o.identityid == sidentityid && o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(intable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(intable.Rows[i], dar, listdar);
                }
            }

            //查询out表的数据
            outstr = "select convert(varchar(10),add_date,121) as AddDate,identityid,txmony as Out_Account,txcount as Out_AccountCount,xztcmony as Out_AdministrationAccount,jjmony as Out_RefuseAccount  from [srv_lnk_total_out].[dafacloud].[dbo].[dt_user_out_sum]  with(nolock) where  add_date>=@strStartDate and add_date<=@strEndDate " + where_str;
            outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, report_share, out_sqlcmdpar);
            lock (dar)
            {
                for (int i = 0; i < outtable.Rows.Count; i++)
                {
                    string ntime = outtable.Rows[i]["AddDate"].ToString();
                    string sidentityid = outtable.Rows[i]["identityid"].ToString().TrimEnd();
                    var mm = listdar.Where(o => o.identityid == sidentityid && o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(outtable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(outtable.Rows[i], dar, listdar);
                }
            }

            //查询order表的数据
            orderstr = "select convert(varchar(10),add_date,121) as AddDate,identityid,tzmony as Out_BettingAccount, tzcount as Out_BettingAccountCount from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_sum]  with(nolock) where  add_date>=@strStartDate and add_date<=@strEndDate " + where_str;
            ordertable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(orderstr, report_share, order_sqlcmdpar);
            lock (dar)
            {
                for (int i = 0; i < ordertable.Rows.Count; i++)
                {
                    string ntime = ordertable.Rows[i]["AddDate"].ToString();
                    string sidentityid = ordertable.Rows[i]["identityid"].ToString().TrimEnd();
                    var mm = listdar.Where(o => o.identityid == sidentityid && o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(ordertable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(ordertable.Rows[i], dar, listdar);
                }
            }

            //查询打賞表的数据
            outstr = "select convert(varchar(10),add_date,121) as AddDate,identityid,rewardmoney as RewardMoney from [srv_lnk_total_out].[dafacloud].[dbo].[dt_reward_sum]  with(nolock) where  add_date>=@strStartDate and add_date<=@strEndDate " + where_str;
            outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, report_share, out_sqlcmdpar);
            lock (dar)
            {
                for (int i = 0; i < outtable.Rows.Count; i++)
                {
                    string ntime = outtable.Rows[i]["AddDate"].ToString();
                    string sidentityid = outtable.Rows[i]["identityid"].ToString().TrimEnd();
                    var mm = listdar.Where(o => o.identityid == sidentityid && o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(outtable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(outtable.Rows[i], dar, listdar);
                }
            }

            //查询打賞表的人數数据
            outstr = "select convert(varchar(10),add_date,121) as AddDate,identityid,count(distinct(user_id)) as RewardNum from [srv_lnk_total_out].[dafacloud].[dbo].[dt_reward_userrecord]  with(nolock) where  add_date>=@strStartDate and add_date<=@strEndDate " + where_str + " group by identityid,add_date";
            outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, report_share, out_sqlcmdpar);
            lock (dar)
            {
                for (int i = 0; i < outtable.Rows.Count; i++)
                {
                    string ntime = outtable.Rows[i]["AddDate"].ToString();
                    string sidentityid = outtable.Rows[i]["identityid"].ToString().TrimEnd();
                    var mm = listdar.Where(o => o.identityid == sidentityid && o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(outtable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(outtable.Rows[i], dar, listdar);
                }
            }

            //查询order表的人數数据
            orderstr = "select convert(varchar(10),add_date,121) as AddDate,identityid,count(distinct(user_id)) as Out_BettingAccountNum from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record]  with(nolock) where  add_date>=@strStartDate and add_date<=@strEndDate " + where_str + " group by identityid,add_date";
            ordertable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(orderstr, report_share, order_sqlcmdpar);
            lock (dar)
            {
                for (int i = 0; i < ordertable.Rows.Count; i++)
                {
                    string ntime = ordertable.Rows[i]["AddDate"].ToString();
                    string sidentityid = ordertable.Rows[i]["identityid"].ToString().TrimEnd();
                    var mm = listdar.Where(o => o.identityid == sidentityid && o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(ordertable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(ordertable.Rows[i], dar, listdar);
                }
            }

            //註冊人數
            congkustr = "select convert(varchar(10),addtime,121) as AddDate,identityid,count(distinct(userid)) as NewMemberNum   from dt_users_register  where  Tester=0 and addtime>=@strStartDate and addtime<=@strEndDate " + where_str + " group by identityid,convert(varchar(10),addtime,121) ";
            congkutable = DBcon.DbHelperSQL.GetQueryFromCongku(congkustr, sqlcmdpar_online);
            lock (dar)
            {
                for (int i = 0; i < congkutable.Rows.Count; i++)
                {
                    string ntime = congkutable.Rows[i]["AddDate"].ToString();
                    string sidentityid = congkutable.Rows[i]["identityid"].ToString().TrimEnd();
                    var mm = listdar.Where(o => o.identityid == sidentityid && o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(congkutable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(congkutable.Rows[i], dar, listdar);
                }
            }

            //首充人數
            congkustr = "select convert(varchar(10),addtime,121) as AddDate,identityid,count(distinct(userid)) as NewPrepaidNum  from dt_user_newpay  where  flog=0 and addtime>=@strStartDate and addtime<=@strEndDate " + where_str + " group by identityid,convert(varchar(10),addtime,121) ";
            congkutable = DBcon.DbHelperSQL.GetQueryFromCongku(congkustr, sqlcmdpar_online);
            lock (dar)
            {
                for (int i = 0; i < congkutable.Rows.Count; i++)
                {
                    string ntime = congkutable.Rows[i]["AddDate"].ToString();
                    string sidentityid = congkutable.Rows[i]["identityid"].ToString().TrimEnd();
                    var mm = listdar.Where(o => o.identityid == sidentityid && o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(congkutable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(congkutable.Rows[i], dar, listdar);
                }
            }

            //当日盈利需拿当日数据计算          
            foreach (dt_identityidReport report in listdar)
            {
                //盈利=投注金额-中奖金额-活动礼金-返点+行政提出+拒绝金额
                report.ProfitLoss = Math.Round(report.Out_BettingAccount - report.In_WinningAccount, 2);
                //盈率 = 盈利/投注*100
                if (report.Out_BettingAccount > 0)
                    report.ProfitRate = Math.Round(report.ProfitLoss / report.Out_BettingAccount * 100, 2);
            }
            int totalCount = listdar.Count;
            listdar = listdar.OrderByDescending(o => o.Out_BettingAccountNum).ToList();
            switch (strOrderBy)
            {
                case "Out_BettingAccountNum":
                    listdar = listdar.OrderByDescending(o => o.Out_BettingAccountNum).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    break;
                case "NewPrepaidNum":
                    listdar = listdar.OrderByDescending(o => o.NewPrepaidNum).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    break;
                case "Out_BettingAccount":
                    listdar = listdar.OrderByDescending(o => o.Out_BettingAccount).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    break;
                case "ProfitLoss":
                    listdar = listdar.OrderByDescending(o => o.ProfitLoss).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    break;
                case "ProfitRate":
                    listdar = listdar.OrderByDescending(o => o.ProfitRate).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    break;
            }


            DataSet ds = new DataSet();
            DataTable dataTable = dataTableHelper.ConvertListToDataTable(listdar);

            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            ds.Tables.Add(totalDataTable);
            ds.Tables.Add(dataTable);
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("After_GetIdentityidReportData", "GetIdentityidReportData", st.ElapsedMilliseconds.ToString(), para);
            }
            return ds;
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("After_Error", "GetIdentityidReportData", st.ElapsedMilliseconds.ToString(), para);
            }
            return null;
        }
        finally
        {

        }
    }
    /// <summary>
    /// 获取平台訊息
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportData(string strStartDate, string strEndDate, int pageIndex, int pageSize)
    {
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "StartTime=" + strStartDate + " EndTime=" + strEndDate + " pageIndex=" + pageIndex + " pageSize=" + pageSize;
        try
        {
            st.Reset();
            st.Start();
            string strStartDate_s = Convert.ToDateTime(strStartDate).ToString("yyyy-MM-dd HH:mm:ss");
            string strEndDate_s = Convert.ToDateTime(strEndDate).ToString("yyyy-MM-dd HH:mm:ss");

            //从报表库拿数据
            string begindate = strStartDate_s;//条件从查询条件获取
            string enddate = strEndDate_s;//条件从查询条件获取

            //線上
            List<SqlParameter> sqlcmdlist_online = new List<SqlParameter>();
            sqlcmdlist_online.Add(new SqlParameter("@strStartDate", SqlDbType.DateTime) { Value = strStartDate });
            sqlcmdlist_online.Add(new SqlParameter("@strEndDate", SqlDbType.DateTime) { Value = Convert.ToDateTime(strEndDate).AddDays(1).ToString("yyyy-MM-dd") });
            SqlParameter[] sqlcmdpar_online = sqlcmdlist_online.ToArray();

            //報表
            List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
            sqlcmdlist.Add(new SqlParameter("@strStartDate", SqlDbType.DateTime) { Value = strStartDate });
            sqlcmdlist.Add(new SqlParameter("@strEndDate", SqlDbType.DateTime) { Value = strEndDate });
            SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();
            List<SqlParameter> sqlcmdlist1 = new List<SqlParameter>();
            sqlcmdlist1.Add(new SqlParameter("@strStartDate", SqlDbType.DateTime) { Value = strStartDate });
            sqlcmdlist1.Add(new SqlParameter("@strEndDate", SqlDbType.DateTime) { Value = strEndDate });
            SqlParameter[] in_sqlcmdpar = sqlcmdlist1.ToArray();
            List<SqlParameter> sqlcmdlist2 = new List<SqlParameter>();
            sqlcmdlist2.Add(new SqlParameter("@strStartDate", SqlDbType.DateTime) { Value = strStartDate });
            sqlcmdlist2.Add(new SqlParameter("@strEndDate", SqlDbType.DateTime) { Value = strEndDate });
            SqlParameter[] out_sqlcmdpar = sqlcmdlist2.ToArray();
            List<SqlParameter> sqlcmdlist3 = new List<SqlParameter>();
            sqlcmdlist3.Add(new SqlParameter("@strStartDate", SqlDbType.DateTime) { Value = strStartDate });
            sqlcmdlist3.Add(new SqlParameter("@strEndDate", SqlDbType.DateTime) { Value = strEndDate });
            SqlParameter[] order_sqlcmdpar = sqlcmdlist3.ToArray();

            //報表物件
            dt_identityidReport dar = new dt_identityidReport();
            List<dt_identityidReport> listdar = new List<dt_identityidReport>();

            //connectstring
            string report_share = ReportHelper.Report_Share;
            string report_in_conn = ReportHelper.Report_In_Conn;
            string report_out_conn = ReportHelper.Report_Out_Conn;
            string report_order_conn = ReportHelper.Report_Order_Conn;

            //datatable
            string instr = "";
            DataTable intable = null;
            string outstr = "";
            DataTable outtable = null;
            string orderstr = "";
            DataTable ordertable = null;
            string congkustr = "";
            DataTable congkutable = null;

            ReportHelper reporthelper = new ReportHelper();
            DataTableHelper dataTableHelper = new DataTableHelper();

            //查询in表的数据

            instr = "select convert(varchar(10),add_date,121) as AddDate,sum(czmony) as In_Account,sum(czcount) as In_AccountCount,sum(xthdmony) as Out_ActivityDiscountAccount,sum(qthdmony) as Out_OtherDiscountAccount from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_sum]  with(nolock) where identityid <> 'test' and add_date>=@strStartDate and add_date<=@strEndDate group by convert(varchar(10),add_date,121)";

            intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, report_share, in_sqlcmdpar);
            lock (dar)
                reporthelper.ConvertToMode<dt_identityidReport>(intable, dar, listdar);

            instr = "select convert(varchar(10),add_date,121) as AddDate,sum(In_AccountNum) as In_AccountNum from (select max(cznumber) as In_AccountNum, identityid, add_date from[srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_sum]  with(nolock) where identityid <> 'test' and add_date >= @strStartDate and add_date <= @strEndDate group by IDENTITYid, add_date) as v2 group by add_date";

            intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, report_share, in_sqlcmdpar);

            lock (dar)
            {
                for (int i = 0; i < intable.Rows.Count; i++)
                {
                    string ntime = intable.Rows[i]["AddDate"].ToString();
                    var mm = listdar.Where(o => o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(intable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(intable.Rows[i], dar, listdar);
                }
            }


            //查询winning表的数据
            instr = "select convert(varchar(10),add_date,121) as AddDate,sum(zjmoney) as In_WinningAccount from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_sum]  with(nolock) where identityid <> 'test' and add_date>=@strStartDate and add_date<=@strEndDate group by convert(varchar(10),add_date,121)";
            intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, report_share, in_sqlcmdpar);
            lock (dar)
            {
                for (int i = 0; i < intable.Rows.Count; i++)
                {
                    string ntime = intable.Rows[i]["AddDate"].ToString();
                    var mm = listdar.Where(o => o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(intable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(intable.Rows[i], dar, listdar);
                }
            }

            //查询get_point表的数据
            instr = "select convert(varchar(10),add_date,121) as AddDate,sum(rebatemoney) as Out_RebateAccount from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_get_point_sum]  with(nolock) where identityid <> 'test' and add_date>=@strStartDate and add_date<=@strEndDate group by convert(varchar(10),add_date,121)";
            intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, report_share, in_sqlcmdpar);
            lock (dar)
            {
                for (int i = 0; i < intable.Rows.Count; i++)
                {
                    string ntime = intable.Rows[i]["AddDate"].ToString();
                    var mm = listdar.Where(o => o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(intable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(intable.Rows[i], dar, listdar);
                }
            }

            //查询out表的数据
            outstr = "select convert(varchar(10),add_date,121) as AddDate,sum(txmony) as Out_Account,sum(txcount) as Out_AccountCount,sum(xztcmony) as Out_AdministrationAccount,sum(jjmony) as Out_RefuseAccount  from [srv_lnk_total_out].[dafacloud].[dbo].[dt_user_out_sum]  with(nolock) where identityid <> 'test' and add_date>=@strStartDate and add_date<=@strEndDate group by convert(varchar(10),add_date,121)";
            outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, report_share, out_sqlcmdpar);
            lock (dar)
            {
                for (int i = 0; i < outtable.Rows.Count; i++)
                {
                    string ntime = outtable.Rows[i]["AddDate"].ToString();
                    var mm = listdar.Where(o => o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(outtable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(outtable.Rows[i], dar, listdar);
                }
            }

            //查询order表的数据
            orderstr = "select convert(varchar(10),add_date,121) as AddDate,sum(tzmony) as Out_BettingAccount , sum(tzcount) as Out_BettingAccountCount from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_sum]  with(nolock) where identityid <> 'test' and add_date>=@strStartDate and add_date<=@strEndDate group by convert(varchar(10),add_date,121)";
            ordertable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(orderstr, report_share, order_sqlcmdpar);
            lock (dar)
            {
                for (int i = 0; i < ordertable.Rows.Count; i++)
                {
                    string ntime = ordertable.Rows[i]["AddDate"].ToString();
                    var mm = listdar.Where(o => o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(ordertable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(ordertable.Rows[i], dar, listdar);
                }
            }

            //查询打賞表的数据
            outstr = "select convert(varchar(10),add_date,121) as AddDate,sum(rewardmoney) as RewardMoney from [srv_lnk_total_out].[dafacloud].[dbo].[dt_reward_sum]  with(nolock) where identityid <> 'test' and  add_date>=@strStartDate and add_date<=@strEndDate group by convert(varchar(10),add_date,121)";
            outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, report_share, out_sqlcmdpar);
            lock (dar)
            {
                for (int i = 0; i < outtable.Rows.Count; i++)
                {
                    string ntime = outtable.Rows[i]["AddDate"].ToString();
                    var mm = listdar.Where(o => o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(outtable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(outtable.Rows[i], dar, listdar);
                }
            }

            //查询打賞表的人數数据
            outstr = "select convert(varchar(10),add_date,121) as AddDate,count(distinct(user_id)) as RewardNum from [srv_lnk_total_out].[dafacloud].[dbo].[dt_reward_userrecord]  with(nolock) where  identityid <> 'test' and  add_date>=@strStartDate and add_date<=@strEndDate group by convert(varchar(10),add_date,121)";
            outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, report_share, out_sqlcmdpar);
            lock (dar)
            {
                for (int i = 0; i < outtable.Rows.Count; i++)
                {
                    string ntime = outtable.Rows[i]["AddDate"].ToString();
                    var mm = listdar.Where(o => o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(outtable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(outtable.Rows[i], dar, listdar);
                }
            }

            //查询order表的人數数据
            orderstr = "select convert(varchar(10),add_date,121) as AddDate,count(distinct(user_id)) as Out_BettingAccountNum  from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record]  with(nolock) where identityid <> 'test' and add_date>=@strStartDate and add_date<=@strEndDate  group by convert(varchar(10),add_date,121)";
            ordertable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(orderstr, report_share, order_sqlcmdpar);
            lock (dar)
            {
                for (int i = 0; i < ordertable.Rows.Count; i++)
                {
                    string ntime = ordertable.Rows[i]["AddDate"].ToString();
                    var mm = listdar.Where(o => o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(ordertable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(ordertable.Rows[i], dar, listdar);
                }
            }

            //註冊人數
            congkustr = "select convert(varchar(10),addtime,121) as AddDate,count(distinct(userid)) as NewMemberNum   from dt_users_register  where identityid <> 'test' and Tester=0 and addtime>=@strStartDate and addtime<=@strEndDate  group by convert(varchar(10),addtime,121) ";
            congkutable = DBcon.DbHelperSQL.GetQueryFromCongku(congkustr, sqlcmdpar_online);
            lock (dar)
            {
                for (int i = 0; i < congkutable.Rows.Count; i++)
                {
                    string ntime = congkutable.Rows[i]["AddDate"].ToString();
                    var mm = listdar.Where(o => o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(congkutable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(congkutable.Rows[i], dar, listdar);
                }
            }

            //首充人數
            congkustr = "select convert(varchar(10),addtime,121) as AddDate,count(distinct(userid)) as NewPrepaidNum  from dt_user_newpay  where identityid <> 'test' and flog=0 and addtime>=@strStartDate and addtime<=@strEndDate  group by convert(varchar(10),addtime,121) ";
            congkutable = DBcon.DbHelperSQL.GetQueryFromCongku(congkustr, sqlcmdpar_online);
            lock (dar)
            {
                for (int i = 0; i < congkutable.Rows.Count; i++)
                {
                    string ntime = congkutable.Rows[i]["AddDate"].ToString();
                    var mm = listdar.Where(o => o.AddDate == ntime).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(congkutable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(congkutable.Rows[i], dar, listdar);
                }
            }

            //当日盈利需拿当日数据计算          
            foreach (dt_identityidReport report in listdar)
            {
                report.identityid = "ALL";
                //盈利=投注金额-中奖金额-活动礼金-返点+行政提出+拒绝金额
                report.ProfitLoss = Math.Round(report.Out_BettingAccount - report.In_WinningAccount, 2);
                //盈率 = 盈利/投注*100
                if (report.Out_BettingAccount > 0)
                    report.ProfitRate = Math.Round(report.ProfitLoss / report.Out_BettingAccount * 100, 2);
            }
            int totalCount = listdar.Count;
            listdar = listdar.OrderByDescending(o => o.AddDate).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            DataTable dataTable = dataTableHelper.ConvertListToDataTable(listdar);
            DataSet ds = new DataSet();
            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            ds.Tables.Add(totalDataTable);
            ds.Tables.Add(dataTable);
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("After_GetReportData", "GetReportData", st.ElapsedMilliseconds.ToString(), para);
            }
            return ds;
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("After_Error", "GetReportData", st.ElapsedMilliseconds.ToString(), para);
            }
            return null;
        }
        finally
        {

        }
    }
    /// <summary>
    /// 获取上月各站及平台訊息
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetLastMonthReportData(string strStartDate, string strEndDate, int pageIndex, int pageSize)
    {
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "strStartDate=" + strStartDate + " strEndDate=" + strEndDate + " pageIndex=" + pageIndex + " pageSize=" + pageSize;
        try
        {
            st.Reset();
            st.Start();
            string strStartDate_s = Convert.ToDateTime(strStartDate).ToString("yyyy-MM-dd HH:mm:ss");
            string strEndDate_s = Convert.ToDateTime(strEndDate).ToString("yyyy-MM-dd HH:mm:ss");
            string strMonth = Convert.ToDateTime(strStartDate).ToString("yyyy-MM") + "-01";
            //从报表库拿数据
            string begindate = strStartDate_s;//条件从查询条件获取
            string enddate = strEndDate_s;//条件从查询条件获取

            //線上
            List<SqlParameter> sqlcmdlist_online = new List<SqlParameter>();
            sqlcmdlist_online.Add(new SqlParameter("@strStartDate", SqlDbType.DateTime) { Value = strStartDate });
            sqlcmdlist_online.Add(new SqlParameter("@strEndDate", SqlDbType.DateTime) { Value = Convert.ToDateTime(strEndDate).AddDays(1).ToString("yyyy-MM-dd") });
            SqlParameter[] sqlcmdpar_online = sqlcmdlist_online.ToArray();

            //報表
            List<SqlParameter> sqlcmdlist3 = new List<SqlParameter>();
            sqlcmdlist3.Add(new SqlParameter("@add_month", SqlDbType.DateTime) { Value = strMonth });
            SqlParameter[] order_sqlcmdpar = sqlcmdlist3.ToArray();

            //報表物件
            dt_identityidReport dar = new dt_identityidReport();
            List<dt_identityidReport> listdar = new List<dt_identityidReport>();

            //connectstring
            string report_share = ReportHelper.Report_Share;
            string report_in_conn = ReportHelper.Report_In_Conn;
            string report_out_conn = ReportHelper.Report_Out_Conn;
            string report_order_conn = ReportHelper.Report_Order_Conn;

            //datatable
            string monthstr = "";
            DataTable monthtable = null;
            string congkustr = "";
            DataTable congkutable = null;

            ReportHelper reporthelper = new ReportHelper();
            DataTableHelper dataTableHelper = new DataTableHelper();

            //查询order表的数据
            monthstr = " select identityid, czmony as In_Account ,cznumber as In_AccountNum,czcount as In_AccountCount,xthdmony as Out_ActivityDiscountAccount,qthdmony as Out_OtherDiscountAccount,zjmoney as In_WinningAccount,rebatemoney as Out_RebateAccount,txmony as Out_Account,txcount as Out_AccountCount,xztcmony as Out_AdministrationAccount,jjmony as Out_RefuseAccount,tzmony as Out_BettingAccount,tznumber as Out_BettingAccountNum,rewardmoney as RewardMoney,rewardnumber as RewardNum " +
                       " from [srv_lnk_total_order].[dafacloud].[dbo].[dt_month_all_record] where identityid <> 'test' and add_date = @add_month Union  " +
                       " select ' ALL' as identityid,sum(czmony) as In_Account ,sum(cznumber) as In_AccountNum,sum(czcount) as In_AccountCount,sum(xthdmony) as Out_ActivityDiscountAccount,sum(qthdmony) as Out_OtherDiscountAccount,sum(zjmoney) as In_WinningAccount,sum(rebatemoney) as Out_RebateAccount,sum(txmony) as Out_Account,sum(txcount) as Out_AccountCount,sum(xztcmony) as Out_AdministrationAccount,sum(jjmony) as Out_RefuseAccount,sum(tzmony) as Out_BettingAccount,sum(tznumber) as Out_BettingAccountNum,sum(rewardmoney) as RewardMoney,sum(rewardnumber) as RewardNum" +
                       " from [srv_lnk_total_order].[dafacloud].[dbo].[dt_month_all_record] where identityid <> 'test' and add_date = @add_month  " +
                       " order by identityid";


            monthtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(monthstr, report_share, order_sqlcmdpar);
            lock (dar)
                reporthelper.ConvertToMode<dt_identityidReport>(monthtable, dar, listdar);

            //註冊人數
            congkustr = " select identityid,count(distinct(userid)) as NewMemberNum   from dt_users_register  where identityid<>'test' and Tester=0 and addtime>=@strStartDate and addtime<=@strEndDate  group by identityid Union" +
                        " select ' ALL' as identityid,count(distinct(userid)) as NewMemberNum   from dt_users_register  where identityid <> 'test' and Tester=0 and addtime>=@strStartDate and addtime<=@strEndDate";
            congkutable = DBcon.DbHelperSQL.GetQueryFromCongku(congkustr, sqlcmdpar_online);
            lock (dar)
            {
                for (int i = 0; i < congkutable.Rows.Count; i++)
                {
                    string sidentityid = congkutable.Rows[i]["identityid"].ToString();
                    var mm = listdar.Where(o => o.identityid == sidentityid).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(congkutable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(congkutable.Rows[i], dar, listdar);
                }
            }

            //首充人數
            congkustr = " select identityid,count(distinct(userid)) as NewPrepaidNum  from dt_user_newpay  where identityid<>'test' and flog=0 and addtime>=@strStartDate and addtime<=@strEndDate  group by identityid Union" +
                        " select ' ALL' as identityid,count(distinct(userid)) as NewPrepaidNum  from dt_user_newpay  where identityid<>'test' and flog=0 and addtime>=@strStartDate and addtime<=@strEndDate";
            congkutable = DBcon.DbHelperSQL.GetQueryFromCongku(congkustr, sqlcmdpar_online);
            lock (dar)
            {
                for (int i = 0; i < congkutable.Rows.Count; i++)
                {
                    string sidentityid = congkutable.Rows[i]["identityid"].ToString();
                    var mm = listdar.Where(o => o.identityid == sidentityid).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(congkutable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_identityidReport>(congkutable.Rows[i], dar, listdar);
                }
            }

            //当日盈利需拿当日数据计算          
            foreach (dt_identityidReport report in listdar)
            {
                report.AddDate = strMonth;
                //盈利=投注金额-中奖金额-活动礼金-返点+行政提出+拒绝金额
                report.ProfitLoss = Math.Round(report.Out_BettingAccount - report.In_WinningAccount, 2);
                //盈率 = 盈利/投注*100
                if (report.Out_BettingAccount > 0)
                    report.ProfitRate = Math.Round(report.ProfitLoss / report.Out_BettingAccount * 100, 2);

            }
            int totalCount = listdar.Count;
            listdar = listdar.OrderBy(o => o.identityid).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            DataTable dataTable = dataTableHelper.ConvertListToDataTable(listdar);
            //
            dataTable.Columns.Add("identityName", typeof(string));
            string identityname = "select identityid,sitename from dt_tenant";
            DataTable dt_identity = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(identityname, report_in_conn, null);
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                if (dt_identity.Select("identityid = '" + dataTable.Rows[i]["identityid"] + "'").Count() > 0)
                    dataTable.Rows[i]["identityName"] = dt_identity.Select("identityid = '" + dataTable.Rows[i]["identityid"] + "'")[0][1].ToString();
                else if (dataTable.Rows[i]["identityid"].ToString().TrimEnd() == " ALL")
                    dataTable.Rows[i]["identityName"] = "平台";
            }

            DataSet ds = new DataSet();
            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            ds.Tables.Add(totalDataTable);
            ds.Tables.Add(dataTable);
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("After_GetLastMonthReportData", "GetLastMonthReportData", st.ElapsedMilliseconds.ToString(), para);
            }
            return ds;
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("After_Error", "GetLastMonthReportData", st.ElapsedMilliseconds.ToString(), para);
            }
            return null;
        }
        finally
        {

        }
    }

    /*
    /// <summary>
    /// 获取彩種報表趨勢圖
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportChartData(string strIdentityId, string lottery_code, string strStartDate, string strEndDate)
    {
        DataSet dataSet = new DataSet();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "IdentityId=" + strIdentityId + " lottery_code=" + lottery_code + " StartTime=" + strStartDate + " EndTime=" + strEndDate;
        try
        {
            List<SqlParameter> ListParameter = new List<SqlParameter>();
            SqlParameter identityidPar = new SqlParameter("@strIdentityId", SqlDbType.VarChar, 50);
            SqlParameter lotterycodePar = new SqlParameter("@lotterycode", SqlDbType.VarChar, 20);
            SqlParameter stardatePar = new SqlParameter("@add_time_s", SqlDbType.Date);
            SqlParameter enddatePar = new SqlParameter("@add_time_e", SqlDbType.Date);
            identityidPar.Value = strIdentityId;
            lotterycodePar.Value = lottery_code;
            stardatePar.Value = strStartDate;
            enddatePar.Value = strEndDate;
            ListParameter.Add(identityidPar);
            ListParameter.Add(lotterycodePar);
            ListParameter.Add(stardatePar);
            ListParameter.Add(enddatePar);

            string field_str = "";
            string where_str = " and identityid <>'test'";
            if (strIdentityId != "all")
            {
                field_str += ",'" + strIdentityId + "' as identityid ";
                where_str += " and identityid=@strIdentityId";
            }
            else
            {
                field_str += ",'平台' as identityid";
            }
            if (lottery_code != "all")
            {
                field_str += ",'" + lottery_code + "' as lotterycode";
                where_str += " and lotterycode=@lotterycode";
            }
            else
            {
                field_str += ",'全彩種' as lotterycode";
            }
            string sql_str = " select convert(char(10),v_order.add_date,121) as add_date " + field_str + ",v_order.lottery_in,v_win.lottery_out,(v_order.lottery_in-v_win.lottery_out) as diff,((v_order.lottery_in-v_win.lottery_out)/v_order.lottery_in*100) as 'per' from "
                           + " (select add_date, sum(normal_money) lottery_in from[dbo].[dt_lottery_record] where add_date>= @add_time_s and add_date<@add_time_e and identityid <>'test' " + where_str + "  group by add_date ) as v_order "
                           + " left join "
                           + " (select add_date, sum(money) lottery_out from srv_lnk_total_in.dafacloud.dbo.dt_lottery_winning_userrecord with(nolock) where add_date>= @add_time_s and add_date<@add_time_e and identityid <>'test' " + where_str + " group by add_date   ) as v_win"
                           + " on v_order.add_date = v_win.add_date order by convert(char(10),v_order.add_date,121)";

            string report_order_conn = ReportHelper.Report_Order_Conn;
            DataTable dt = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(sql_str, report_order_conn, ListParameter.ToArray());
            dataSet.Tables.Add(dt);
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("After_GetReportChartData", "GetReportChartData", st.ElapsedMilliseconds.ToString(), para);
            }
            return dataSet;
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("After_Error", "GetReportChartData", st.ElapsedMilliseconds.ToString(), para);
            }
            return null;
        }
    }
    */





}
